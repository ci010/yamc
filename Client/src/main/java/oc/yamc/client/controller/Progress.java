package oc.yamc.client.controller;

public enum Progress {
    BEGIN, DOING, STOP
}
