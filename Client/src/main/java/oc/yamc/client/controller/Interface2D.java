package oc.yamc.client.controller;

import oc.yamc.client.UserInterface;
import oc.yamc.client.keybinding.KeyCode;
import oc.yamc.client.keybinding.KeyModifier;
import oc.yamc.client.ui.UIParent;
import org.lwjgl.glfw.GLFW;

public class Interface2D implements UserInterface.State {
    private UIParent uiRoot;
    private double x, y;

    public Interface2D(UIParent uiRoot) {
        this.uiRoot = uiRoot;
    }

    @Override
    public boolean handleCursorMove(double x, double y) {
        this.x = x;
        this.y = y;
        uiRoot.onMouseMove(x, y);
        return false;
    }

    @Override
    public boolean handleScroll(double xoffset, double yoffset) {
        uiRoot.handleScroll(x, y, xoffset, yoffset);
        return false;
    }

    @Override
    public boolean handleMousePress(int button, int action, int modifiers) {
        switch (action) {
            case GLFW.GLFW_PRESS:
                return uiRoot.handleMousePress(x, y, KeyCode.valueOf(button), KeyModifier.valueOf(modifiers));
            case GLFW.GLFW_RELEASE:
                return uiRoot.handleMouseRelease(x, y, KeyCode.valueOf(button), KeyModifier.valueOf(modifiers));
            case GLFW.GLFW_REPEAT:
                return uiRoot.handleMousePressing(x, y, KeyCode.valueOf(button), KeyModifier.valueOf(modifiers));
        }
        return false;
    }

    @Override
    public boolean handleKeyPress(int key, int scancode, int action, int modifiers) {
        switch (action) {
            case GLFW.GLFW_PRESS:
                return uiRoot.handleKeyPress(KeyCode.valueOf(key), KeyModifier.valueOf(modifiers));
            case GLFW.GLFW_RELEASE:
                return uiRoot.handleKeyRelease(KeyCode.valueOf(key), KeyModifier.valueOf(modifiers));
            case GLFW.GLFW_REPEAT:
                return uiRoot.handleKeyPressing(KeyCode.valueOf(key), KeyModifier.valueOf(modifiers));
        }
        return false;
    }
}
