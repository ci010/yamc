package oc.yamc.client.controller;

import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Multimap;
import lombok.NonNull;
import lombok.Setter;
import lombok.Value;
import oc.yamc.GameContext;
import oc.yamc.client.UserInterface;
import oc.yamc.client.keybinding.KeyCode;
import oc.yamc.client.keybinding.KeyEventSource;
import oc.yamc.event.EventBus;
import oc.yamc.event.EventSource;
import org.lwjgl.glfw.GLFW;

import java.util.Collection;
import java.util.EnumSet;

/**
 * In game 3d mode
 */
public class InterfaceInGame implements UserInterface.State {
    private final Multimap<Integer, SourceAndExecutor> codeToBinding = LinkedListMultimap.create();
    private final EnumSet<KeyCode> pressedKey = EnumSet.allOf(KeyCode.class);
    @Setter
    @NonNull
    private CameraController cameraController;
    private GameContext context;

    public InterfaceInGame(GameContext context) {
        this.context = context;
        this.context.getRegistry().getRegistry(EventSource.class).getValues().stream()
                .filter(v -> v instanceof KeyEventSource)
                .forEach(source -> codeToBinding.put(((KeyEventSource) source).getCode().code, new SourceAndExecutor(((KeyEventSource) source), context.getEventExecutor(((KeyEventSource) source).getEventType()))));
    }

    private boolean handlePress(int button, int action, int modifiers) {
        KeyCode keyCode;
        Collection<SourceAndExecutor> keys;
        switch (action) {
            case GLFW.GLFW_PRESS:
                keyCode = KeyCode.valueOf(button);
                pressedKey.add(keyCode);
                keys = codeToBinding.get(keyCode.code);
                for (var emitter : keys) {
                    KeyEventSource source = emitter.source;
                    source.setStatus(Progress.BEGIN);
                    emitter.executor.execute(source.construct(context));
                }
                break;
            case GLFW.GLFW_RELEASE:
                keyCode = KeyCode.valueOf(button);
                pressedKey.remove(keyCode);
                keys = codeToBinding.get(keyCode.code);
                for (var emitter : keys) {
                    KeyEventSource source = emitter.source;
                    source.setStatus(Progress.STOP);
                    emitter.executor.execute(source.construct(context));
                }
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    public boolean handleCursorMove(double x, double y) {
        cameraController.handleCursorMove(x, y);
        return false;
    }

    @Override
    public boolean handleScroll(double xoffset, double yoffset) {
        cameraController.handleScroll(xoffset, yoffset);
        return false;
    }

    @Value
    class SourceAndExecutor {
        KeyEventSource source;
        EventBus.Executor executor;
    }

    @Override
    public boolean handleMousePress(int button, int action, int modifiers) {
        return handlePress(button + 400, action, modifiers);
    }

    @Override
    public boolean handleKeyPress(int key, int scancode, int action, int modifiers) {
        return handlePress(key, action, modifiers);
    }
}
