package oc.yamc.client.controller;

import oc.yamc.client.scene.Camera;
import org.joml.Vector3f;

public class FirstPersonController extends CameraController {
    private static final float SENSIBILITY = 0.05f;
    private float yaw, pitch, roll;
    private double lastX, lastY;
    private boolean setupLast = false;

    public FirstPersonController(Camera camera) {
        super(camera);
    }

    @Override
    public void update(Vector3f position, Vector3f rotation) {
        camera.getPosition().set(position);

        double rPitch = Math.toRadians(this.pitch);
        double cosPitch = Math.cos(rPitch);
        double rYaw = Math.toRadians(this.yaw);
        double sinYaw = Math.sin(rYaw);

        Vector3f front = new Vector3f(
                (float) (cosPitch * Math.cos(rYaw)),
                (float) Math.sin(rPitch),
                (float) (cosPitch * sinYaw)).normalize();
        camera.getPosition().add(front, camera.getLookAt());
        rotation.set(front);
    }

    @Override
    public void handleCursorMove(double x, double y) {
        if (!paused) {
            double yaw = (x - lastX) * SENSIBILITY;
            double pitch = (lastY - y) * SENSIBILITY;
            lastX = x;
            lastY = y;
            if (setupLast) {
                this.pitch += pitch;
                this.pitch = Math.min(89.0f, Math.max(-89.0f, this.pitch));
                this.yaw += yaw;
            } else setupLast = true;
        }
    }
}
