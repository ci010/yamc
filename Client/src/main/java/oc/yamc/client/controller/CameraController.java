package oc.yamc.client.controller;

import oc.yamc.client.scene.Camera;
import org.joml.Vector3f;

public abstract class CameraController {
    protected Camera camera;
    protected boolean paused = false;

    public CameraController(Camera camera) {
        this.camera = camera;
    }

    public Camera getCamera() {
        return camera;
    }

    public abstract void update(Vector3f position, Vector3f rotation);

    public abstract void handleCursorMove(double x, double y);

    public void handleScroll(double xoffset, double yoffset) { }
}
