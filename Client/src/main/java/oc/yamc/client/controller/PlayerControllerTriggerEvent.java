package oc.yamc.client.controller;

import oc.yamc.event.Event;

public class PlayerControllerTriggerEvent implements Event {
    public final Progress progress;

    public PlayerControllerTriggerEvent(Progress progress) {
        this.progress = progress;
    }

    public enum Direction {
        FORWARD, BACKWARD, LEFT, RIGHT
    }

    public static class LiftClick extends PlayerControllerTriggerEvent {
        public LiftClick(Progress progress) {
            super(progress);
        }
    }

    public static class RightClick extends PlayerControllerTriggerEvent {
        public RightClick(Progress progress) {
            super(progress);
        }
    }

    public static class Move extends PlayerControllerTriggerEvent {
        public final Direction direction;

        public Move(Direction direction, Progress progress) {
            super(progress);
            this.direction = direction;
        }
    }

    public static class Jump extends PlayerControllerTriggerEvent {
        public Jump(Progress progress) {
            super(progress);
        }
    }

    public static class Sneak extends PlayerControllerTriggerEvent {
        public Sneak(Progress progress) {
            super(progress);
        }
    }
}
