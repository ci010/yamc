package oc.yamc.client.util;

public interface Texture {
    int getWidth();

    int getHeight();

    int getRed(int x, int y);

    int getGreen(int x, int y);

    int getBlue(int x, int y);

    int getAlpha(int x, int y);

    Texture r(int x, int y, int v);

    Texture g(int x, int y, int v);

    Texture b(int x, int y, int v);

    Texture a(int x, int y, int v);

    Texture rgba(int x, int y, int r, int g, int b, int a);
}
