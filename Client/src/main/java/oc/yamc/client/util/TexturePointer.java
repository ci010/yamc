package oc.yamc.client.util;

import lombok.Getter;

public class TexturePointer implements Texture {
    private byte[] data;
    @Getter
    private int width, height;
    private int offset;

    public TexturePointer(byte[] data, int width, int height, int offset) {
        this.data = data;
        this.width = width;
        this.height = height;
        this.offset = offset;
    }

    @Override
    public int getRed(int x, int y) {
        ensureRange(x, y);
        return data[offset + width * 4 * y + x * 4] & 0xFF;
    }

    @Override
    public int getGreen(int x, int y) {
        ensureRange(x, y);
        return data[offset + width * 4 * y + x * 4 + 1] & 0xFF;
    }

    @Override
    public int getBlue(int x, int y) {
        ensureRange(x, y);
        return data[offset + width * 4 * y + x * 4 + 2] & 0xFF;
    }

    @Override
    public int getAlpha(int x, int y) {
        ensureRange(x, y);
        return data[offset + width * 4 * y + x * 4 + 3] & 0xFF;
    }

    @Override
    public Texture r(int x, int y, int v) {
        ensureRange(x, y);
        data[offset + width * 4 * y + x * 4] = (byte) (v & 0xFF);
        return this;
    }

    @Override
    public Texture g(int x, int y, int v) {
        ensureRange(x, y);
        data[offset + width * 4 * y + x * 4 + 1] = (byte) (v & 0xFF);
        return this;
    }

    @Override
    public Texture b(int x, int y, int v) {
        ensureRange(x, y);
        data[offset + width * 4 * y + x * 4 + 2] = (byte) (v & 0xFF);
        return this;
    }

    @Override
    public Texture a(int x, int y, int v) {
        ensureRange(x, y);
        data[offset + width * 4 * y + x * 4 + 3] = (byte) (v & 0xFF);
        return this;
    }

    @Override
    public Texture rgba(int x, int y, int r, int g, int b, int a) {
        ensureRange(x, y);
        int off = offset + width * 4 * y + x * 4;
        data[off] = (byte) (r & 0xFF);
        data[off + 1] = (byte) (g & 0xFF);
        data[off + 2] = (byte) (b & 0xFF);
        data[off + 3] = (byte) (a & 0xFF);
        return this;
    }

    private void ensureRange(int x, int y) {
        if (x >= this.width || x < 0) throw new IllegalArgumentException();
        if (y >= this.height || y < 0) throw new IllegalArgumentException();
    }
}
