package oc.yamc.client.util;

import lombok.Getter;

public class SimpleSemanticVersion implements Comparable<SimpleSemanticVersion> {
    @Getter
    private int major, minor, fix;

    @Override
    public int compareTo(SimpleSemanticVersion o) {
        if (o.major == major) {
            if (o.minor == minor) {
                if (o.fix == fix) {
                    return 0;
                }
                return this.fix - o.fix;
            }
            return this.minor - o.minor;
        }
        return this.major - o.major;
    }
}
