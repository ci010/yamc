package oc.yamc.client.util;

import lombok.Getter;

public class TextureSingle implements Texture {
    private byte[] data;
    @Getter
    private int width, height;

    public TextureSingle(byte[] data, int width, int height) {
        this.data = data;
        this.width = width;
        this.height = height;
    }

    @Override
    public int getRed(int x, int y) {
        return data[width * 4 * y + x * 4] & 0xFF;
    }

    @Override
    public int getGreen(int x, int y) {
        return data[width * 4 * y + x * 4 + 1] & 0xFF;
    }

    @Override
    public int getBlue(int x, int y) {
        return data[width * 4 * y + x * 4 + 2] & 0xFF;
    }

    @Override
    public int getAlpha(int x, int y) {
        return data[width * 4 * y + x * 4 + 3] & 0xFF;
    }

    @Override
    public Texture r(int x, int y, int v) {
        data[width * 4 * y + x * 4] = (byte) (v & 0xFF);
        return this;
    }

    @Override
    public Texture g(int x, int y, int v) {
        data[width * 4 * y + x * 4 + 1] = (byte) (v & 0xFF);
        return this;
    }

    @Override
    public Texture b(int x, int y, int v) {
        data[width * 4 * y + x * 4 + 2] = (byte) (v & 0xFF);
        return this;
    }

    @Override
    public Texture a(int x, int y, int v) {
        data[width * 4 * y + x * 4 + 3] = (byte) (v & 0xFF);
        return this;
    }

    @Override
    public Texture rgba(int x, int y, int r, int g, int b, int a) {
        int off = width * 4 * y + x * 4;
        data[off] = (byte) (r & 0xFF);
        data[off + 1] = (byte) (g & 0xFF);
        data[off + 2] = (byte) (b & 0xFF);
        data[off + 3] = (byte) (a & 0xFF);
        return this;
    }
}
