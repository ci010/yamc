package oc.yamc.client.util;

import oc.yamc.client.gl.GLVertexArray;
import org.lwjgl.BufferUtils;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.Objects;

/**
 * The vertex buffer for VBO. It force the output vertex array use {@link org.lwjgl.opengl.GL11#GL_TRIANGLES}.
 */
public class VertexBuffer {
    private VertexType[] vertexTypes;
    /**
     * Byte per vertex
     */
    private int stride;

    private FloatBuffer dataBuffer;
    private IntBuffer indexBuffer;
    private int count;

    private GLVertexArray.DrawMode mode;

    public VertexBuffer(int size, VertexType[] types, boolean useIndex, GLVertexArray.DrawMode drawMode) {
        Objects.requireNonNull(types, "The buffer vertex type definitely cannot be null!");
        this.dataBuffer = BufferUtils.createFloatBuffer(size / Float.BYTES);
        this.vertexTypes = types;
        this.stride = 0;
        for (int i = 0; i < types.length; i++) {
            Objects.requireNonNull(types[i], "The buffer vertex type arr cannot have null element!");
            this.stride += types[i].getSize() * Float.BYTES;
        }
        if (useIndex) this.indexBuffer = BufferUtils.createIntBuffer(size / 16);
        this.mode = drawMode;
    }

    public VertexBuffer(int size, VertexType[] types, boolean useIndex) {
        this(size, types, useIndex, GLVertexArray.DrawMode.TRIANGLES);
    }

    private static int roundUp(int number, int interval) {
        if (interval == 0)
            return 0;
        if (number == 0)
            return interval;
        if (number < 0)
            interval *= -1;
        int i = number % interval;
        return i == 0 ? number : number + interval - i;
    }

    public void flush(GLVertexArray vertexArray) {
        dataBuffer.flip();
        if (indexBuffer != null) indexBuffer.flip();

        int[] attrs = new int[this.vertexTypes.length];
        for (int i = 0; i < attrs.length; i++) {
            attrs[i] = this.vertexTypes[i].getSize();
        }
        vertexArray.putAll(mode.mode, this.count, attrs, dataBuffer, indexBuffer);
    }

    public void reset() {
        count = 0;
        dataBuffer.clear();
        if (indexBuffer != null) indexBuffer.clear();
    }

    public int getCount() {
        return count;
    }

    public VertexBuffer quad(float... floats) {
        if (floats.length * Float.BYTES != stride * 4)
            throw new IllegalArgumentException("The quad length mismatch! The quad must have 4 vertices putData!");
        if (mode == GLVertexArray.DrawMode.TRIANGLES) {
            if (indexBuffer != null) {
                ensureCapacity(4);
                dataBuffer.put(floats);
                indexBuffer.put(this.count).put(this.count + 1).put(this.count + 2)
                        .put(this.count + 2).put(this.count + 3).put(this.count);
                this.count += 4;
            } else {
                ensureCapacity(6);
                int floatPerVertex = stride / Float.BYTES;
                dataBuffer.put(floats, 0, floatPerVertex * 3);
                dataBuffer.put(floats, floatPerVertex * 2, floatPerVertex * 2)
                        .put(floats, 0, floatPerVertex);
                this.count += 6; // actually we put two triangle here
            }
        } else if (mode == GLVertexArray.DrawMode.LINES) {
            if (indexBuffer != null) {
                ensureCapacity(8);
                dataBuffer.put(floats);
                indexBuffer.put(this.count).put(this.count + 1)
                        .put(this.count + 1).put(this.count + 2)
                        .put(this.count + 2).put(this.count + 3)
                        .put(this.count + 3).put(this.count);
                this.count += 4;
            } else {
                ensureCapacity(8);
                int floatPerVertex = stride / Float.BYTES;
                int twoVertices = floatPerVertex * 2;
                dataBuffer.put(floats, 0, twoVertices)
                        .put(floats, floatPerVertex, twoVertices)
                        .put(floats, 2 * floatPerVertex, twoVertices)
                        .put(floats, 3 * floatPerVertex, floatPerVertex).put(floats, 0, floatPerVertex)
                ;
                this.count += 8;
            }
        }
        return this;
    }

    public VertexBuffer tri(float... floats) {
        if (floats.length * Float.BYTES != stride * 3)
            throw new IllegalArgumentException("The triangle length mismatch! The triangle must have 3 vertices putData!");

        ensureCapacity(3);
        dataBuffer.put(floats);
        if (indexBuffer != null) {
            indexBuffer.put(this.count)
                    .put(this.count + 1)
                    .put(this.count + 2);
        }
        this.count += 3;
        return this;
    }

    public VertexBuffer putAll(VertexBuffer buffer) {
        if (buffer.indexBuffer != null ^ this.indexBuffer != null) {
            throw new IllegalArgumentException("");
        }
        int count = buffer.count;
        ensureCapacity(count);
        dataBuffer.put(buffer.dataBuffer);
        if (indexBuffer != null) {
            indexBuffer.put(buffer.indexBuffer);
        }
        this.count += count;
        return this;
    }

    private void ensureCapacity(int deltaLen) {
        if (dataBuffer.position() + deltaLen > dataBuffer.capacity()) {
            int capacity = this.dataBuffer.capacity();
            int newSize = capacity + roundUp(deltaLen, 2097152);
            FloatBuffer floatBuffer = BufferUtils.createFloatBuffer(newSize)
                    .put(this.dataBuffer).rewind();
            int position = this.dataBuffer.position();
            this.dataBuffer = floatBuffer.position(position);
        }
        if (indexBuffer != null && indexBuffer.position() + deltaLen * 2 > indexBuffer.capacity()) {
            int capacity = indexBuffer.capacity();
            int newSize = capacity + roundUp(deltaLen, 2097152);
            IntBuffer intBuffer = BufferUtils.createIntBuffer(newSize)
                    .put(this.indexBuffer).rewind();
            int position = this.indexBuffer.position();
            this.indexBuffer = intBuffer.position(position);
        }
    }

    public enum DefaultVertexType implements VertexType {
        POSITION(3), TEXTURE(2), COLOR(4), NORMAL(3);

        private final int size;

        DefaultVertexType(int size) {
            this.size = size;
        }

        public int getSize() {
            return size;
        }
    }

    public interface VertexType {
        int getSize();

        default boolean valid(float v) {
            return true;
        }
    }
}
