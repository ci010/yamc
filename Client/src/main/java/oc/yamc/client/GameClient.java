package oc.yamc.client;

import lombok.Getter;
import lombok.Setter;
import oc.yamc.client.model.TextureAtlasBuilder;
import oc.yamc.client.scene.Scene;
import oc.yamc.game.Game;
import oc.yamc.resource.ResourceManager;
import oc.yamc.world.World;

import java.util.function.Supplier;

public interface GameClient extends Game {

    /**
     * @return the resource manager
     */
    ResourceManager getResourceManager();

    /**
     * Get client active world
     *
     * @return The current active world
     */
    World getWorld();

    Scene getScene();

    class Option extends Game.Option {
        @Getter
        @Setter
        private Supplier<TextureAtlasBuilder> atlasBuilderSupplier;
    }
}
