package oc.yamc.client.scene;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.joml.Vector3f;

@Data
@RequiredArgsConstructor
public class DirectionalLight {
    @NonNull
    private Vector3f color, direction;
    @NonNull
    private float intensity;

    public DirectionalLight(DirectionalLight light) {
        this(new Vector3f(light.getColor()), new Vector3f(light.getDirection()), light.getIntensity());
    }
}

