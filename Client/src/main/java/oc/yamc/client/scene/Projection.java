package oc.yamc.client.scene;

import org.joml.Matrix4f;

/**
 * The object provides projection matrix. For the people who doesn't have gl context.
 * Read
 * <p>
 * http://www.opengl-tutorial.org/beginners-tutorials/tutorial-3-matrices/
 * </p>
 */
public interface Projection {
    Matrix4f projection();

    float getZNear();

    float getZFar();

    Matrix4f projectionInRange(float zNear, float zFar);
}
