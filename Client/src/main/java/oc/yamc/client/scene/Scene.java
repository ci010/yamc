package oc.yamc.client.scene;

import com.artemis.World;
import com.google.common.collect.Lists;
import lombok.Data;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import oc.yamc.Engine;
import oc.yamc.client.gl.GLTexture2D;
import oc.yamc.client.gl.GLVertexArray;
import oc.yamc.client.model.ChunkModelFactory;
import oc.yamc.client.rendering.RendererCore;
import oc.yamc.event.Listener;
import oc.yamc.world.BlockChangeEvent;
import oc.yamc.world.ChunkColumn;
import org.joml.FrustumIntersection;
import org.joml.Matrix4f;
import org.joml.Quaternionf;
import org.joml.Vector3f;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * The actual client scene, containing the mesh and transformation of any rendered object
 * <p>The {@link RendererCore} renders this.</p>
 */
public class Scene {
    private static final Marker SCENE_DEBUG = MarkerFactory.getMarker("Scene");
    @Getter
    private Vector3f ambientLight = new Vector3f(0, 0, 0);
    @Getter
    private List<PointLight> pointLightList = Lists.newArrayList();
    @Getter
    private DirectionalLight directionalLight = new DirectionalLight(
            new Vector3f(1, 1, 1),
            new Vector3f(1, -1, 0.5F),
            1);

    @Getter
    private Terrain terrain;
    @Getter
    private Sky sky = new Sky();
    @Getter
    private Fog fog = new Fog();

    @Getter
    private List<Entity> entities = new ArrayList<>();

    @Getter
    private Canvas2D canvas2D = new Canvas2D();

    private World world = new World();

    @Getter
    private Camera camera = new Camera();
    private FrustumIntersection frustum = new FrustumIntersection();

    @Getter
    @Setter
    private Projection projection;

    private Matrix4f projViewMatrixCache;

    public void setupTerrain(ChunkModelFactory factory, GLTexture2D texture, int cachedSize) {
        this.terrain = new Terrain(factory, texture, cachedSize);
    }

    public Matrix4f getProjViewMatrix() {
        return projViewMatrixCache;
    }

    private void updateVisibility() {
        projViewMatrixCache = projection.projection().mul(camera.view());

        frustum.set(projViewMatrixCache, false);

        for (Entity entity : entities)
            entity.visible = frustum.testAab(entity.min, entity.max);
        for (Terrain.Chunk chunk : terrain.chunks)
            chunk.visible = frustum.testAab(chunk.x, chunk.y, chunk.z,
                    chunk.x + 16, chunk.y + 16, chunk.z + 16);
    }

    public void update() {
        this.updateVisibility();
    }

    @Listener
    public void handleChunkLoad(ChunkColumn.LoadEvent event) {
        Engine.getLogger().debug(SCENE_DEBUG, "CHUNK LOAD {}, {}", event.x, event.z);
        this.terrain.loadChunkColumn(event.x, event.z, event.blocks);
    }

    @Listener
    public void handleBlockChange(BlockChangeEvent event) {
        Engine.getLogger().debug(SCENE_DEBUG, "BLOCK CHANGE @({}, {}, {}) -> {}.", event.pos.getX(), event.pos.getY(), event.pos.getZ(), event.blockId);
        this.terrain.updateBlock(event.pos, event.blockId);
    }

    public interface Config {

    }

    @Data
    public static class Entity {
        private Vector3f position = new Vector3f();
        private Quaternionf rotation = new Quaternionf();
        private float scale = 1;

        private Vector3f min = new Vector3f(0, 0, 0), max = new Vector3f(1, 1, 1);  // render bounding box

        @NonNull
        private GLVertexArray mesh;
        private GLTexture2D texture;

        private boolean visible = true;
    }

    @Data
    public class Sky {
        private Vector3f light;
        private GLTexture2D texture;
        private GLVertexArray glMesh;
    }

    @Data
    public class Fog {
        private boolean enabled = true;
        private Vector3f color = new Vector3f(0.01F, 0.01F, 0.01F);
        private float density = 0.01F;
    }
}
