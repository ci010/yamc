package oc.yamc.client.scene;

import io.netty.util.collection.LongObjectHashMap;
import io.netty.util.collection.LongObjectMap;
import lombok.Data;
import oc.yamc.Engine;
import oc.yamc.client.gl.GLTexture2D;
import oc.yamc.client.gl.GLVertexArray;
import oc.yamc.client.model.ChunkModelFactory;
import oc.yamc.math.Position;

import java.util.LinkedList;

public class Terrain {
    LinkedList<Chunk> chunks;
    private ChunkModelFactory factory;
    private LongObjectMap<Chunk[]> posToChunks;
    private GLTexture2D texture;
    private int threshold;

    private boolean dirty;

    Terrain(ChunkModelFactory factory, GLTexture2D texture, int cachedSize) {
        this.factory = factory;
        this.texture = texture;
        this.threshold = cachedSize;
        this.chunks = new LinkedList<>();
        this.posToChunks = new LongObjectHashMap<>(threshold);
    }

    public LinkedList<Chunk> getChunks() {
        if (dirty) {
            gc();
        }
        return chunks;
    }

    public GLTexture2D getTexture() {
        return texture;
    }

    void gc() {
//            chunks.removeIf(next -> !next.valid);
    }

    void updateBlock(Position pos, int block) {
        long chunkPos = pos.getChunkXZ();

        Chunk[] chunks = posToChunks.get(chunkPos);
        if (chunks == null) {
            Engine.getLogger().warn("This should not happened! Update a block {}, {}, {}, at unloaded chunk!");
            return;
        }
        Chunk chunk = chunks[pos.getChunkY()];

        if (chunk == null) {
            Engine.getLogger().error("Internal Error! Update a block {}, {}, {}, at loaded chunk, but the chunk instance is null?!");
            return;
        }
        chunk.count += block == 0 ? -1 : 1;
        if (chunk.count != 0) {
            chunk.blocks[pos.pack()] = block;
            if (chunk.mesh == GLVertexArray.EMPTY) {
                chunk.mesh = GLVertexArray.alloc(true);
            }
            chunk.mesh = factory.update(chunk.mesh, chunk.blocks);
            chunk.valid = true;
        } else {
            chunk.blocks[pos.pack()] = block;
            chunk.mesh = GLVertexArray.EMPTY;
            chunk.valid = false;
        }
    }

    void loadChunkColumn(int x, int z, int[][] chunkColumn) {
        long chunkXZ = Position.getChunkXZ(x << 4, z << 4);
        Chunk[] chunks = posToChunks.computeIfAbsent(chunkXZ, (k) -> new Chunk[16]);

        for (int i = 0; i < chunkColumn.length; i++) {
            int y = i * 16;
            int[] chunk = chunkColumn[i];

            short count = 0;

            for (int j = 0; j < chunk.length; j++)
                if (chunk[j] != 0)
                    count++;
            Chunk inst = new Chunk(x << 4, y, z << 4, chunk, count != 0 ? factory.bake(chunk) : GLVertexArray.EMPTY, count);

            this.chunks.add(inst);
            Chunk old = chunks[i];
            chunks[i] = inst;

            if (old != null) {
                Engine.getLogger().warn("Force reload chunk column at {}, {}!", x, z);
                old.mesh.clean();
                old.valid = false;
            }
        }
    }

    void unloadChunk(Position pos) {
        long chunkPos = pos.getChunkXZ();
        Chunk[] chunks = posToChunks.get(chunkPos);
        Chunk chunk = chunks[pos.getChunkY()];
        if (chunk != null) {
            chunk.mesh.clean();
            chunk.valid = false;
            chunks[pos.getChunkY()] = null;
        }
    }

    @Data
    public class Chunk {
        int x, y, z;
        boolean visible = true;

        private int[] blocks;
        private GLVertexArray mesh;

        private short count;
        private boolean valid = true;

        Chunk(int x, int y, int z, int[] blocks, GLVertexArray mesh, short count) {
            this.x = x;
            this.y = y;
            this.z = z;
            this.blocks = blocks;
            this.mesh = mesh;
            this.count = count;
        }
    }
}
