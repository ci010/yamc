package oc.yamc.client.scene;

import org.joml.Matrix4f;
import org.joml.Vector3f;

/**
 * The lifecycle of controller is interesting... A controller can be killed after a game die.
 * <p>So im considering bind controller to {@link oc.yamc.game.Game}</p>
 */
public class Camera {
    public static Vector3f UP_VECTOR = new Vector3f(0, 1, 0);

    private Vector3f pos = new Vector3f();
    private Vector3f lookAt = new Vector3f(0, 0, -1);

    public Vector3f getPosition() {
        return pos;
    }

    public Vector3f getLookAt() {
        return lookAt;
    }

    public Vector3f getFrontVector() {
        return getLookAt().sub(getPosition(), new Vector3f()).normalize();
    }

    /**
     * alloc view matrix for shader to use
     *
     * @return
     */
    public Matrix4f view() {
        return new Matrix4f().lookAt(getPosition(), getLookAt(), Camera.UP_VECTOR);
    }
}
