package oc.yamc.client.scene;

import com.google.common.collect.Lists;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.collections.ListChangeListener;
import lombok.Getter;
import oc.yamc.client.ui.UIComponent;
import oc.yamc.client.ui.UIParent;
import oc.yamc.client.util.VertexBuffer;

import java.util.List;

public class Canvas2D {
    @Getter
    private VertexBuffer buffer = new VertexBuffer(1048576,
            new VertexBuffer.VertexType[]{
                    VertexBuffer.DefaultVertexType.POSITION,
                    VertexBuffer.DefaultVertexType.COLOR,
                    VertexBuffer.DefaultVertexType.TEXTURE
            },
            false);
    private TickObservable tick = new TickObservable();

    @Getter
    private UIParent uiRoot = new UIParent() {
        {
//            mount(Canvas2D.this);
        }

        @Override
        protected void relayout(ListChangeListener.Change<? extends UIComponent> change) { }
    };

    public Observable getTick() {
        return tick;
    }

    public void update() {
        tick.invalid();
    }

    private class TickObservable implements Observable {
        private List<InvalidationListener> listeners = Lists.newArrayList();

        @Override
        public void addListener(InvalidationListener listener) {
            listeners.add(listener);
        }

        @Override
        public void removeListener(InvalidationListener listener) {
            listeners.remove(listener);
        }

        void invalid() {
            for (InvalidationListener listener : listeners)
                listener.invalidated(this);
        }
    }
}
