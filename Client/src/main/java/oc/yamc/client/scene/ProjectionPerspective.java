package oc.yamc.client.scene;

import lombok.Data;
import org.joml.Matrix4f;

@Data
public class ProjectionPerspective implements Projection {
    private float fov = (float) (Math.toRadians(Math.max(1.0, Math.min(90.0, 60.0f)))),
            aspect, zNear = 0.01f, zFar = 700f;

    public ProjectionPerspective(float aspect) {
        this.aspect = aspect;
    }

    @Override
    public Matrix4f projection() {
        return new Matrix4f().perspective(fov, aspect, zNear, zFar);
    }

    @Override
    public Matrix4f projectionInRange(float zNear, float zFar) {
        return new Matrix4f().perspective(fov, aspect, zNear, zFar);
    }
}
