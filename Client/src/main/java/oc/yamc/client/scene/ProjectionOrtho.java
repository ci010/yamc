package oc.yamc.client.scene;

import lombok.Data;
import lombok.NonNull;
import org.joml.Matrix4f;

@Data
public class ProjectionOrtho implements Projection {
    @NonNull
    private float zNear, zFar, left, right, bottom, top;

    public static Projection symmetric(float width, float height, float zNear, float zFar) {
        return new ProjectionOrtho(zNear, zFar, -width, +width, -height, +height);
    }

    @Override
    public Matrix4f projection() {
        return new Matrix4f().setOrtho(left, right, bottom, top, zNear, zFar);
    }

    @Override
    public Matrix4f projectionInRange(float zNear, float zFar) {
        return new Matrix4f().setOrtho(left, right, bottom, top, zNear, zFar);
    }
}
