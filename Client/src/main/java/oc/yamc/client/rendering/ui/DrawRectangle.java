package oc.yamc.client.rendering.ui;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.ObjectBinding;
import oc.yamc.client.gl.GLVertexArray;
import oc.yamc.client.scene.Canvas2D;
import oc.yamc.client.ui.UIRectangle;
import oc.yamc.client.util.VertexBuffer;

public class DrawRectangle extends Drawer<UIRectangle> {
    private ObjectBinding<GLVertexArray> vertexArray;
    private GLVertexArray internalReference;

    @Override
    public void mount(Canvas2D canvas2D, UIRectangle component) {
        super.mount(canvas2D, component);
        this.vertexArray = Bindings.createObjectBinding(() -> {
            VertexBuffer buf = canvas2D.getBuffer();
            int height = component.getHeight();
            int width = component.getWidth();
            float r = component.getRed(), g = component.getGreen(), b = component.getBlue(), a = component.getAlpha();
            buf.quad(0, 0, 0, 0, 0, r, g, b, a,
                    0, height, 0, 0, 1, r, g, b, a,
                    width, height, 0, 1, r, g, b, a,
                    width, 0, 0, 1, 0, 0, r, g, b, a);
            internalReference = internalReference == null ? GLVertexArray.alloc(false) : internalReference;
            buf.flush(internalReference);
            buf.reset();
            return internalReference;
        }, component.width, component.height);
    }

    @Override
    public void draw(Context context) {
        this.vertexArray.get().render();
    }
}
