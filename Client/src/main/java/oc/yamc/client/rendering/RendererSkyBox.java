package oc.yamc.client.rendering;

import oc.yamc.client.gl.GLShader;
import oc.yamc.client.gl.GLShaderProgram;
import oc.yamc.client.scene.Scene;
import oc.yamc.resource.ResourceManager;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.opengl.GL11;

import java.io.IOException;

/**
 * Renderer for skybox. This will pick the scene {@link Scene.Sky} object to render.
 */
public class RendererSkyBox {
    private GLShaderProgram program;
    @Uniform
    private int u_Projection, u_View, u_SunPosition, u_TextureSampler, u_Time;

    private int time;

    void setup(ResourceManager manager) throws IOException {
        this.program = GLShaderProgram.alloc(
                GLShader.create(manager.load("yamc/shader/skybox.vert").cache(), GLShader.Type.VERTEX_SHADER),
                GLShader.create(manager.load("yamc/shader/fbm_cloud.glsl").cache(), GLShader.Type.FRAGMENT_SHADER),
                GLShader.create(manager.load("yamc/shader/atmosphere.glsl").cache(), GLShader.Type.FRAGMENT_SHADER),
                GLShader.create(manager.load("yamc/shader/skybox.frag").cache(), GLShader.Type.FRAGMENT_SHADER)
        );
        program.use();
        UniformDecorator.decorate(this, program);
    }

    private void setProjection(Matrix4f matrix4f) {
        GLShaderProgram.setUniform(u_Projection, matrix4f);
    }

    private void setView(Matrix4f matrix4f) {
        GLShaderProgram.setUniform(u_View, matrix4f);
    }

    private void setTexture(int t) {
        GLShaderProgram.setUniform(u_TextureSampler, t);
    }

    private void setSunPosition(Vector3f sunPosition) {
        GLShaderProgram.setUniform(u_SunPosition, sunPosition);
    }

    public void render(Scene scene, double partialTick) {
        time++;
        GL11.glEnable(GL11.GL_TEXTURE_2D);
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

        program.use();

        var light = scene.getDirectionalLight();

        Vector3f direction = light.getDirection();
        var sunPosition = direction.negate(new Vector3f());

        Matrix4f pro = scene.getProjection().projection();
        Matrix4f view = new Matrix4f(scene.getCamera().view());
        view.m30(0).m31(0).m32(0);

        GLShaderProgram.setUniform(u_Time, (float) (GLFW.glfwGetTime() * 0.2F - 0.0F));
        setProjection(pro);
        setView(view);
        setSunPosition(sunPosition);
        setTexture(0);

        Scene.Sky sky = scene.getSky();
        sky.getTexture().bind();
        sky.getGlMesh().render();

        program.unuse();
    }

}
