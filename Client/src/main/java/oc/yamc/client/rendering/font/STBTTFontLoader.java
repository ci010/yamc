package oc.yamc.client.rendering.font;

import oc.yamc.client.rendering.FontContext;
import oc.yamc.resource.Resource;

import java.io.IOException;
import java.nio.ByteBuffer;

public class STBTTFontLoader implements FontContext.Loader {
    @Override
    public boolean canLoad() {
        return false;
    }

    @Override
    public FontContext loadContext(Resource resource) throws IOException {
        byte[] cache = resource.cache();
        return new STBTTFontContext(ByteBuffer.allocateDirect(cache.length).put(cache).flip());
    }
}
