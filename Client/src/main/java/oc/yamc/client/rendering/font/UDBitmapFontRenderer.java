package oc.yamc.client.rendering.font;

import java.io.File;

class UDBitmapFontRenderer {
    private final UDBitmapFont font;

    public UDBitmapFontRenderer(File file) {
        font = UDBitmapFont.readFont(file);
    }

    public UDBitmapFont getFont() {
        return font;
    }

    /**
     * @param text
     * @param posX
     * @param posY     posY-coordinate fat text, where baseline is located
     * @param color
     */
//    public void drawText(String text, float posX, float posY, int color) {
//        Tessellator tessellator = Tessellator.getInstance();
//        BufferBuilder buffer = tessellator.getBuffer();
//        float startX = 0;
//        for (char c : text.toCharArray()) {
//            UDBitmapFont.CharInfo info = font.getFontData().get(c);
//            if (info == null) {
//                info = font.getFontData().get(' ');
//                if (info == null)
//                    continue;
//            }
//            buffer.begin(GL11.GL_QUADS, true, true, true, false);
//            float r = ((color >> 16) & 255) / 255f;
//            float g = ((color >> 8) & 255) / 255f;
//            float b = (color & 255) / 255f;
//            float a = ((color >> 24) & 255) / 255f;
//            buffer.pos(posX + startX, posY - info.getBaseline(), 0).color(r, g, b, a)
//                    .tex(info.getPosX() / (float) font.getWidth(), info.getPosX() / (float) font.getWidth())
//                    .end();
//            buffer.pos(posX + startX, posY - info.getBaseline() + info.getHeight(), 0).color(r, g, b, a)
//                    .tex(info.getPosX() / (float) font.getWidth(),
//                            (info.getPosY() + info.getHeight()) / (float) font.getHeight())
//                    .end();
//            buffer.pos(posX + startX + info.getWidth(), posY - info.getBaseline() + info.getHeight(), 0).color(r, g, b, a)
//                    .tex((info.getPosX() + info.getWidth()) / (float) font.getWidth(),
//                            (info.getPosY() + info.getHeight()) / (float) font.getHeight())
//                    .end();
//            buffer.pos(posX + startX + info.getWidth(), posY - info.getBaseline(), 0).color(r, g, b, a)
//                    .tex((info.getPosX() + info.getWidth()) / (float) font.getWidth(),
//                            info.getPosY() / (float) font.getHeight())
//                    .end();
//
//            tessellator.draw();
//            startX += info.getWidth();
//        }
//    }
}
