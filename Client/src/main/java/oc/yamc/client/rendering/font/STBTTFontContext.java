package oc.yamc.client.rendering.font;

import lombok.Getter;
import oc.yamc.client.rendering.FontContext;
import oc.yamc.client.util.VertexBuffer;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.opengl.GL11;
import org.lwjgl.stb.STBTTAlignedQuad;
import org.lwjgl.stb.STBTTBakedChar;
import org.lwjgl.stb.STBTTFontinfo;
import org.lwjgl.stb.STBTruetype;
import org.lwjgl.system.MemoryStack;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.Map;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.stb.STBTruetype.*;
import static org.lwjgl.system.MemoryStack.stackPush;

public class STBTTFontContext implements FontContext {
    @Getter
    private final STBTTFontinfo fontInfo;

    private ByteBuffer ttfBuf;
    private String name;

    private final int ascent;
    private final int descent;
    private final int lineGap;

    public static final int SUPPORTING_CHARACTER_COUNT = 256;

    private float contentScaleX;
    private float contentScaleY;
    private Map<Integer, Pair<Integer, STBTTBakedChar.Buffer>> bufMap = new HashMap<>();

    public STBTTFontContext(ByteBuffer ttfBuf) {
        this.ttfBuf = ttfBuf;
        fontInfo = STBTTFontinfo.create();
        if (!stbtt_InitFont(fontInfo, ttfBuf)) {
            throw new IllegalStateException("Failed in initializing ttf font info");
        }
        try (MemoryStack stack = stackPush()) {
            IntBuffer pAscent = stack.mallocInt(1);
            IntBuffer pDescent = stack.mallocInt(1);
            IntBuffer pLineGap = stack.mallocInt(1);

            stbtt_GetFontVMetrics(fontInfo, pAscent, pDescent, pLineGap);

            ascent = pAscent.get(0);
            descent = pDescent.get(0);
            lineGap = pLineGap.get(0);

            long monitor = GLFW.glfwGetPrimaryMonitor();
            FloatBuffer p1 = stack.mallocFloat(1);
            FloatBuffer p2 = stack.mallocFloat(1);
            GLFW.glfwGetMonitorContentScale(monitor, p1, p2);
            contentScaleX = p1.get(0);
            contentScaleY = p2.get(0);
        }
    }

    private Pair<Integer, STBTTBakedChar.Buffer> getCharDataBuffer(int fontHeight) {
        if (!bufMap.containsKey(fontHeight)) {
            int texId = GL11.glGenTextures();
            STBTTBakedChar.Buffer cdata = STBTTBakedChar.malloc(SUPPORTING_CHARACTER_COUNT);
            int side = getBitmapSide(fontHeight, SUPPORTING_CHARACTER_COUNT);
            ByteBuffer bitmap = BufferUtils.createByteBuffer(side * side);
            stbtt_BakeFontBitmap(ttfBuf, fontHeight, bitmap, side, side, 0, cdata);

            glBindTexture(GL_TEXTURE_2D, texId);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, side, side, 0, GL_RED, GL_UNSIGNED_BYTE, bitmap);
            glBindTexture(GL_TEXTURE_2D, 0);
            glEnable(GL_TEXTURE_2D);
            bufMap.put(fontHeight, new ImmutablePair<>(texId, cdata));
        }
        return bufMap.get(fontHeight);
    }

    @Override
    public String getFontName() {
        return name;
    }

    @Override
    public int getWidth(String text, int size) {
        float scale = STBTruetype.stbtt_ScaleForPixelHeight(fontInfo, size);
        int width = 0;
        try (MemoryStack stack = stackPush()) {
            IntBuffer pCodePoint = stack.mallocInt(1);
            IntBuffer pAdvancedWidth = stack.mallocInt(1);
            IntBuffer pLeftSideBearing = stack.mallocInt(1);

            int i = 0;
            while (i < text.length()) {
                i += getCodePoint(text, i, pCodePoint);
                int cp = pCodePoint.get(0);

                STBTruetype.stbtt_GetCodepointHMetrics(fontInfo, cp, pAdvancedWidth, pLeftSideBearing);
                width += pAdvancedWidth.get(0);
            }
        }
        return (int) (width * scale);
    }

    private void cleanCharDataBuffer(int fontHeight) {
        if (bufMap.containsKey(fontHeight)) {
            glDeleteTextures(bufMap.get(fontHeight).getLeft());
            bufMap.remove(fontHeight);
        }
    }

    @Override
    public int putData(String text, float x, float y, int color, int fontHeight, VertexBuffer vertexBuffer) {
        float scale = stbtt_ScaleForPixelHeight(fontInfo, fontHeight);
        Pair<Integer, STBTTBakedChar.Buffer> pair = getCharDataBuffer(fontHeight);

        STBTTBakedChar.Buffer cdata = pair.getRight();
        try (MemoryStack stack = MemoryStack.stackPush()) {
            IntBuffer charPointBuffer = stack.mallocInt(1);
            FloatBuffer posX = stack.floats(x);
            FloatBuffer posY = stack.floats(y + fontHeight);

            float factorX = 1.0f / getContentScaleX();
            float factorY = 1.0f / getContentScaleY();

            float r = ((color >> 16) & 255) / 255f;
            float g = ((color >> 8) & 255) / 255f;
            float b = (color & 255) / 255f;
            float a = ((color >> 24) & 255) / 255f;

            float centerY = y + fontHeight;

            int side = getBitmapSide(fontHeight, SUPPORTING_CHARACTER_COUNT);
            STBTTAlignedQuad stbQuad = STBTTAlignedQuad.mallocStack(stack);
            for (int i = 0; i < text.length(); ) {
                i += getCodePoint(text, i, charPointBuffer);

                int charPoint = charPointBuffer.get(0);

                float centerX = posX.get(0);
                stbtt_GetBakedQuad(cdata, side, side, charPoint, posX, posY, stbQuad, true);
                posX.put(0, scale(centerX, posX.get(0), factorX));
                if (i < text.length()) {
                    getCodePoint(text, i, charPointBuffer);
                    posX.put(0, posX.get(0)
                            + stbtt_GetCodepointKernAdvance(fontInfo, charPoint, charPointBuffer.get(0)) * scale);
                }
                float x0 = scale(centerX, stbQuad.x0(), factorX), x1 = scale(centerX, stbQuad.x1(), factorX),
                        y0 = scale(centerY, stbQuad.y0(), factorY), y1 = scale(centerY, stbQuad.y1(), factorY);

                vertexBuffer.quad(
                        x0, y0, 0, r, g, b, a, stbQuad.s0(), stbQuad.t0(),
                        x0, y1, 0, r, g, b, a, stbQuad.s0(), stbQuad.t1(),
                        x1, y1, 0, r, g, b, a, stbQuad.s1(), stbQuad.t1(),
                        x1, y0, 0, r, g, b, a, stbQuad.s1(), stbQuad.t0());
            }
        }
        return pair.getLeft();
    }

    private float getStringWidth(STBTTFontinfo info, String text, int from, int to, int fontHeight) {
        int width = 0;

        try (MemoryStack stack = stackPush()) {
            IntBuffer pCodePoint = stack.mallocInt(1);
            IntBuffer pAdvancedWidth = stack.mallocInt(1);
            IntBuffer pLeftSideBearing = stack.mallocInt(1);

            int i = from;
            while (i < to) {
                i += getCodePoint(text, i, pCodePoint);
                int cp = pCodePoint.get(0);

                stbtt_GetCodepointHMetrics(info, cp, pAdvancedWidth, pLeftSideBearing);
                width += pAdvancedWidth.get(0);
            }
        }

        return width * stbtt_ScaleForPixelHeight(info, fontHeight);
    }

    private float scale(float center, float offset, float factor) {
        return (offset - center) * factor + center;
    }

    private static int getCodePoint(String text, int i, IntBuffer cpOut) {
        char c1 = text.charAt(i);
        if (Character.isHighSurrogate(c1) && i + 1 < text.length()) {
            char c2 = text.charAt(i + 1);
            if (Character.isLowSurrogate(c2)) {
                cpOut.put(0, Character.toCodePoint(c1, c2));
                return 2;
            }
        }
        cpOut.put(0, c1);
        return 1;
    }

    private int getBitmapSide(int fontHeight, int countOfChar) {
        return (int) Math.ceil((fontHeight + 2 * fontHeight / 16.0f) * Math.sqrt(countOfChar));
    }

    private float getContentScaleX() {
        return contentScaleX;
    }

    private float getContentScaleY() {
        return contentScaleY;
    }
}
