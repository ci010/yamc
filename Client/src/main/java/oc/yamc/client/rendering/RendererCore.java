package oc.yamc.client.rendering;

import oc.yamc.client.GameWindow;
import oc.yamc.client.gl.GLBuffer;
import oc.yamc.client.gl.GLShader;
import oc.yamc.client.gl.GLShaderProgram;
import oc.yamc.client.gl.GLVertexArray;
import oc.yamc.client.model.Mesh;
import oc.yamc.client.scene.Scene;
import oc.yamc.client.scene.Terrain;
import oc.yamc.resource.ResourceManager;
import org.joml.Matrix4f;
import org.lwjgl.opengl.GL11;

import java.io.IOException;
import java.util.List;

import static org.lwjgl.opengl.GL11.*;

public class RendererCore {
    private ShaderProgramChunk shaderSolidChunk;

    private RendererSkyBox skyBox = new RendererSkyBox();
    private RendererGui gui = new RendererGui();
    private RendererSelectedBlock selectedBlock = new RendererSelectedBlock();
    private RendererShadowMap shadowMap = new RendererShadowMap();

    /**
     * Store all shared uniforms here.
     */
    private Uniforms uniforms;

    private GLVertexArray showTextureMap = Mesh.compact(new Mesh(new float[]{ 0, 0, 0, 2, 0, 0, 2, 2, 0, 0, 2, 0, },
            new float[]{ 0, 1, 1, 1, 1, 0, 0, 0, }, new float[]{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    }, new int[]{ 0, 2, 1, 0, 3, 2 }, GL11.GL_TRIANGLES));

    /**
     * Entry render function
     *
     * @param partialTick Should be a fractional number how much logic ticks have passed when this function called
     */
    public void render(GameWindow gameWindow, Scene scene, double partialTick) {
        Terrain.Chunk[] chunks = scene.getTerrain().getChunks().stream()
                .filter(c -> c.getMesh() != null && c.isValid() && c.isVisible())
                .toArray(Terrain.Chunk[]::new);

        uniforms.setTransform(scene.getProjection().projection(), scene.getCamera().view());
        uniforms.setFog(scene.getFog());
        shadowMap.render(scene, chunks);
        glClear(GL_DEPTH_BUFFER_BIT);
        glViewport(0, 0, (int) gameWindow.getWidth(), (int) gameWindow.getHeight());
        glCullFace(GL_BACK);
        skyBox.render(scene, partialTick);
        renderSolid(scene, chunks);
        selectedBlock.render(scene, partialTick);
        gui.render(scene, partialTick);
    }

    /**
     * Hard code function load up all the resources.
     *
     * @param fontContextMap The Font loaders, probability not be here later.
     * @param manager        The resource manager
     * @throws IOException Any IOException
     */
    public void load(List<FontContext.Loader> fontContextMap, ResourceManager manager) throws IOException {

        shadowMap.setup(manager);
        selectedBlock.setup(manager);
        gui.setup(manager);
        skyBox.setup(manager);

        shaderSolidChunk = new ShaderProgramChunk(
                GLShaderProgram.alloc(
                        GLShader.create(manager.load("yamc/shader/exp_fog.glsl").cache(), GLShader.Type.FRAGMENT_SHADER),
                        GLShader.create(manager.load("yamc/shader/pcf_shadow.glsl").cache(), GLShader.Type.FRAGMENT_SHADER),
                        GLShader.create(manager.load("yamc/shader/opaque.vert").cache(), GLShader.Type.VERTEX_SHADER),
                        GLShader.create(manager.load("yamc/shader/opaque.frag").cache(), GLShader.Type.FRAGMENT_SHADER)),
                shadowMap.getCascadeZ());

        shaderSolidChunk.program.use();
        shaderSolidChunk.program.bindUniformBlock("Matrices", 0);
        shaderSolidChunk.program.bindUniformBlock("Fog", 1);
        shaderSolidChunk.program.unuse();

        uniforms = new Uniforms();
    }

    /* *********** */
    /* Debug Start */
    /* *********** */

    /**
     * This should render all the solid objects. This currently not include the entities. We should do it later.
     */
    private void renderSolid(Scene scene, Terrain.Chunk[] chunks) {
        shaderSolidChunk.use();

        shaderSolidChunk.useShadow(true);
        shaderSolidChunk.setLightView(shadowMap.getLightProjectViews());

        int shadowMapActiveOffset = 1;
        shaderSolidChunk.setUniformTexture(0); // bind uniform texture goes to 0 active texture slot
        shaderSolidChunk.setUniformShadowMap(shadowMapActiveOffset); // bind uniform shadow map textures go to 1 to N active texture slot

        scene.getTerrain().getTexture().bind(0); // bind the texture to the active slot 0

        DepthBuffer[] buffers = shadowMap.getBuffers();
        for (int i = 0; i < buffers.length; i++) {
            buffers[i].getTexture().bind(i + shadowMapActiveOffset); // bind the depth texture to the active slot 1 to N
        }

        for (Terrain.Chunk chunk : chunks) {
            shaderSolidChunk.setUniformModel(new Matrix4f().translation(chunk.getX(), chunk.getY(), chunk.getZ()));
            chunk.getMesh().render();
        }

        for (int i = 0; i < buffers.length; i++) {
            buffers[i].getTexture().unbind();
        }

        renderDebug();
    }

    private void renderDebug() {
        shaderSolidChunk.useShadow(false);
        DepthBuffer[] buffers = shadowMap.getBuffers();
        for (int i = 0; i < buffers.length; i++) {
            buffers[i].getTexture().bind(0);
            shaderSolidChunk.setUniformModel(new Matrix4f().translation(i * 2.5F, 15, 0));
            showTextureMap.render();
            buffers[i].getTexture().unbind();
        }
    }

    /* ********* */
    /* Debug End */
    /* ********* */

    class Uniforms {
        GLBuffer matrices, fog;

        Uniforms() {
            GLBuffer alloc = GLBuffer.alloc(GLBuffer.Type.UNIFORM_BUFFER);
            alloc.bind();
            alloc.bufferData(256, GLBuffer.Usage.DYNAMIC_DRAW);
            alloc.glBindBufferRange(0, 0, 128);
            alloc.unbind();
            this.matrices = alloc;
            alloc = GLBuffer.alloc(GLBuffer.Type.UNIFORM_BUFFER);
            alloc.bind();
            alloc.bufferData(32, GLBuffer.Usage.DYNAMIC_DRAW);
            alloc.glBindBufferRange(1, 0, 32);
            alloc.unbind();
            this.fog = alloc;
        }

        void setTransform(Matrix4f p, Matrix4f v) {
            matrices.bind();
            matrices.bufferSubData(p, 0);
            matrices.bufferSubData(v, 64);
            matrices.unbind();
        }

        void setFog(Scene.Fog m) {
            fog.bind();
            fog.bufferSubData(m.isEnabled(), 0);
            fog.bufferSubData(m.getDensity(), 4);
            fog.bufferSubData(m.getColor(), 16);
            fog.unbind();
        }
    }


    public static class ShaderProgramChunk {
        private GLShaderProgram program;
        private int u_Model;
        private int u_Texture;
        private int u_UseShadowMap;

        private int[] u_ShadowMaps;
        private int[] u_LightViewMatrices;
        private int[] u_ShadowCascadeZ;

        ShaderProgramChunk(GLShaderProgram program, float[] shadowCascadeZ) {
            this.program = program;
            u_Model = program.getUniformLocation("u_ModelMatrix");
            u_Texture = program.getUniformLocation("u_Texture");
            u_UseShadowMap = program.getUniformLocation("u_UseShadowMap");

            int nOfShadowMaps = shadowCascadeZ.length;
            u_ShadowCascadeZ = new int[nOfShadowMaps];
            u_ShadowMaps = new int[nOfShadowMaps];
            u_LightViewMatrices = new int[nOfShadowMaps];
            for (int i = 0; i < nOfShadowMaps; i++) {
                u_ShadowMaps[i] = program.getUniformLocation("u_ShadowMaps[" + i + "]");
                u_LightViewMatrices[i] = program.getUniformLocation("u_LightViewMatrices[" + i + "]");
                u_ShadowCascadeZ[i] = program.getUniformLocation("u_ShadowCascadeZ[" + i + "]");
            }

            this.program.use();
            for (int i = 0; i < nOfShadowMaps; i++) {
                GLShaderProgram.setUniform(u_ShadowCascadeZ[i], shadowCascadeZ[i]);
            }
            this.program.unuse();
        }

        void use() {
            program.use();
            glEnable(GL_CULL_FACE);
            glEnable(GL_TEXTURE_2D);
            glEnable(GL_DEPTH_TEST);
//            glEnable(GL_BLEND);
//            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        }

        void unuse() {
            program.unuse();
        }

        void setLightView(Matrix4f[] m) {
            for (int i = 0; i < u_LightViewMatrices.length; i++) {
                GLShaderProgram.setUniform(u_LightViewMatrices[i], m[i]);
            }
        }

        void setUniformShadowMap(int offset) {
            for (int i = 0; i < u_ShadowMaps.length; i++) {
                GLShaderProgram.setUniform(u_ShadowMaps[i], offset + i);
            }
        }

        void setUniformModel(Matrix4f m) {
            GLShaderProgram.setUniform(u_Model, m);
        }

        void setUniformTexture(int i) {
            GLShaderProgram.setUniform(u_Texture, i);
        }

        void useShadow(boolean use) {
            GLShaderProgram.setUniform(u_UseShadowMap, use);
        }
    }
}
