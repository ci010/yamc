package oc.yamc.client.rendering;

import oc.yamc.client.gl.GLShaderProgram;

import java.lang.reflect.Field;

public class UniformDecorator {
    static void decorate(Object o, GLShaderProgram program) {
        for (Field field : o.getClass().getDeclaredFields()) {
            Uniform uniform = field.getAnnotation(Uniform.class);
            if (uniform != null) {
                String name = field.getName();
                int loc = program.getUniformLocation(name);
                field.setAccessible(true);
                try {
                    field.set(o, loc);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                field.setAccessible(false);
            }
        }
    }
}
