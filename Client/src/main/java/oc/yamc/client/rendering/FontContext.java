package oc.yamc.client.rendering;

import oc.yamc.client.util.VertexBuffer;
import oc.yamc.resource.Resource;

import java.io.IOException;

public interface FontContext {
    String getFontName();

    /**
     * Get the width of current text with a certain size
     *
     * @param text The text content
     * @param size The font size of the text
     * @return Should return the pixel width of the text
     */
    int getWidth(String text, int size);

    /**
     * @param text         The text content
     * @param x            Text position y on parent
     * @param y            Text position x on parent
     * @param color        The color of the font
     * @param fontHeight   The height of the font
     * @param vertexBuffer The vertex array will fill
     * @return The OpenGL texture id
     */
    int putData(String text, float x, float y, int color, int fontHeight, VertexBuffer vertexBuffer);

    /**
     * The font loader of the font context
     */
    interface Loader {
        boolean canLoad();

        FontContext loadContext(Resource resource) throws IOException;
    }
}
