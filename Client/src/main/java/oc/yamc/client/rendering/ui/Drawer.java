package oc.yamc.client.rendering.ui;

import oc.yamc.client.scene.Canvas2D;
import oc.yamc.client.ui.UIComponent;
import org.joml.Matrix4f;

public abstract class Drawer<T extends UIComponent> {
    protected T component;
    protected Canvas2D canvas2D;

    public void mount(Canvas2D canvas2D, T component) {
        this.canvas2D = canvas2D;
        this.component = component;
    }

    public abstract void draw(Context context);

    public interface Context {
        Matrix4f getTransform();

        void setTransform(Matrix4f transform);

        void useAlpha(boolean alpha);

        void useTexture(boolean useTexture);
    }
}
