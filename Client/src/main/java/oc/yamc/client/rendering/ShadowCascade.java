package oc.yamc.client.rendering;

import lombok.Getter;
import lombok.Setter;
import oc.yamc.client.scene.DirectionalLight;
import oc.yamc.client.scene.Projection;
import org.joml.Matrix4f;
import org.joml.Vector3f;

/**
 * This class should
 */
public class ShadowCascade {
    @Getter
    private final Matrix4f view = new Matrix4f(), projection = new Matrix4f();
    @Getter
    @Setter
    private float zNear, zFar;
    private Vector3f[] corners;
    private Vector3f temp;

    public ShadowCascade(float zNear, float zFar) {
        this.zNear = zNear;
        this.zFar = zFar;
        this.initCache();
    }

    private void initCache() {
        this.temp = new Vector3f();
        this.corners = new Vector3f[8];
        for (int i = 0; i < 8; i++)
            this.corners[i] = new Vector3f();
    }

    private void updateCorners(Matrix4f projViewMatrix) {
        for (int i = 0; i < 8; i++)
            projViewMatrix.frustumCorner(i, corners[i]);
    }


    public void update(DirectionalLight light, Matrix4f cameraView, Projection perspective) {
        Matrix4f projViewMatrix =
                perspective.projectionInRange(zNear, zFar)
                        .mul(cameraView);
        Vector3f lightDirection = light.getDirection();

        updateCorners(projViewMatrix);
        Vector3f centroid = new Vector3f();
        for (int i = 0; i < 8; i++)
            centroid.add(this.corners[i]);
        centroid.div(8);

        Vector3f from = new Vector3f(centroid).sub(lightDirection),
                to = new Vector3f(centroid).add(lightDirection);
        this.view.setLookAt(centroid, to, new Vector3f(0, 1, 0));

        float minX = Float.MAX_VALUE;
        float maxX = -Float.MAX_VALUE;
        float minY = Float.MAX_VALUE;
        float maxY = -Float.MAX_VALUE;
        float minZ = Float.MAX_VALUE;
        float maxZ = -Float.MAX_VALUE;
        for (int i = 0; i < 8; i++) {
            view.transformPosition(corners[i], temp);
            minX = Math.min(temp.x, minX);
            maxX = Math.max(temp.x, maxX);
            minY = Math.min(temp.y, minY);
            maxY = Math.max(temp.y, maxY);
            minZ = Math.min(temp.z, minZ);
            maxZ = Math.max(temp.z, maxZ);
        }

        this.projection.setOrtho(minX, maxX, minY, maxY, minZ - 28, maxZ + 28); // these magic numbers... fix up the missing shading
    }
}
