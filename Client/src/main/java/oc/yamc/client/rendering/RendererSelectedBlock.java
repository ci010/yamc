package oc.yamc.client.rendering;

import oc.yamc.client.Main;
import oc.yamc.client.gl.GLShader;
import oc.yamc.client.gl.GLShaderProgram;
import oc.yamc.client.gl.GLVertexArray;
import oc.yamc.client.scene.Scene;
import oc.yamc.client.util.VertexBuffer;
import oc.yamc.resource.ResourceManager;
import oc.yamc.util.RayCast;
import org.joml.AABBd;
import org.joml.Matrix4f;
import org.lwjgl.opengl.GL11;

import java.io.IOException;

public class RendererSelectedBlock {
    private GLShaderProgram program;
    private GLVertexArray vertexArray = GLVertexArray.alloc(false);
    private VertexBuffer buffer = new VertexBuffer(1024,
            new VertexBuffer.VertexType[]{ VertexBuffer.DefaultVertexType.POSITION },
            false,
            GLVertexArray.DrawMode.LINES);

    private int u_ModelMatrix;

    void setup(ResourceManager resourceManager) throws IOException {
        program = GLShaderProgram.alloc(
                GLShader.create(resourceManager.load("yamc/shader/frame.vert").cache(), GLShader.Type.VERTEX_SHADER),
                GLShader.create(resourceManager.load("yamc/shader/frame.frag").cache(), GLShader.Type.FRAGMENT_SHADER)
        );
        u_ModelMatrix = program.getUniformLocation("u_ModelMatrix");

        program.use();
        program.bindUniformBlock("Matrices", 0);
        program.unuse();
    }

    public void render(Scene scene, double partialTick) {
        program.use();
        RayCast.Hit hit = Main.getGame().getWorld().raycast(scene.getCamera().getPosition(), scene.getCamera().getFrontVector(), 5);
        GL11.glEnable(GL11.GL_LINE_SMOOTH);
        GL11.glLineWidth(2);

        if (hit != null) {
            for (AABBd box : hit.block.getBoundingBoxes()) {
                float off = 0.0001F;
                float mix = (float) box.minX - off, miy = (float) box.minY - off, miz = (float) box.minZ - off,
                        max = (float) box.maxX + off, may = (float) box.maxY + off, maz = (float) box.maxZ + off;
                buffer
                        .quad(mix, miy, maz,
                                mix, may, maz,
                                max, may, maz,
                                max, miy, maz)
                        .quad(mix, miy, miz,
                                mix, may, miz,
                                max, may, miz,
                                max, miy, miz)
                        .quad(max, miy, miz,
                                max, may, miz,
                                max, may, maz,
                                max, miy, maz)
                        .quad(mix, miy, miz,
                                mix, may, miz,
                                mix, may, maz,
                                mix, miy, maz)
                        .quad(mix, miy, maz,
                                mix, miy, miz,
                                max, miy, miz,
                                max, miy, maz)
                        .quad(mix, may, maz,
                                mix, may, miz,
                                max, may, miz,
                                max, may, maz);
                buffer.flush(vertexArray);
                buffer.reset();
                GLShaderProgram.setUniform(u_ModelMatrix, new Matrix4f()
                        .translate(hit.position.getX(), hit.position.getY(), hit.position.getZ()));
                vertexArray.render();
            }
        }
        program.unuse();
    }
}
