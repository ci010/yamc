package oc.yamc.client.rendering;

import lombok.Getter;
import oc.yamc.client.gl.GLFrameBuffer;
import oc.yamc.client.gl.GLShader;
import oc.yamc.client.gl.GLShaderProgram;
import oc.yamc.client.scene.Projection;
import oc.yamc.client.scene.Scene;
import oc.yamc.client.scene.Terrain;
import oc.yamc.resource.ResourceManager;
import org.joml.Matrix4f;

import java.io.IOException;

import static org.lwjgl.opengl.GL11.*;

/**
 * Response to provide the shadow data to the rendering element.
 */
public class RendererShadowMap {
    @Uniform
    private int u_LightProjectView, u_Model;
    private GLShaderProgram program;

    /**
     * The shadow cascade for shadow map
     */
    private ShadowCascade[] cascades;
    @Getter
    private DepthBuffer[] buffers;

    @Getter
    private Matrix4f[] lightProjectViews;

    private float[] cascadeZ;

    float[] getCascadeZ() {
        return cascadeZ;
    }

    void setup(ResourceManager manager) throws IOException {
        program = GLShaderProgram.alloc(GLShader.create(manager.load("yamc/shader/shadow_map.vert").cache(), GLShader.Type.VERTEX_SHADER),
                GLShader.create(manager.load("yamc/shader/shadow_map.frag").cache(), GLShader.Type.FRAGMENT_SHADER));
        cascadeZ = new float[]{ 30, 100, 200 };
        cascades = new ShadowCascade[3];
        float plain = 0.1F;
        for (int i = 0; i < cascadeZ.length; i++) {
            cascades[i] = new ShadowCascade(plain, cascadeZ[i]);
            plain = cascadeZ[i];
        }
        lightProjectViews = new Matrix4f[cascades.length];
        buffers = new DepthBuffer[]{
                DepthBuffer.create(1024 * 2, 1024 * 2),
                DepthBuffer.create(1024 * 2, 1024 * 2),
                DepthBuffer.create(1024 * 2, 1024 * 2) };
        UniformDecorator.decorate(this, program);
    }

    private void setClipSpaceTransform(Matrix4f m) {
        GLShaderProgram.setUniform(u_LightProjectView, m);
    }

    private void setModel(Matrix4f m) {
        GLShaderProgram.setUniform(u_Model, m);
    }

    /**
     * Perform shadow map algorithm, see some tutorials teaching how shadow map works. The vertex shader code is on
     * /assets/yamc/shader/shadow_map.vert.
     * <p>
     * This function basically performs a render pass from the light source along with the light direction.
     * In this pass, it only stores the depth info to the framebuffer, which is a texture currently.
     * Later, we can use the depth info in texture to determine whether an pixel is in shadow.
     *
     * @see <a href="https://learnopengl.com/Advanced-Lighting/Shadows/Shadow-Mapping">LearnOpenGL tutorial</a>
     * @see <a href="https://docs.microsoft.com/en-us/windows/desktop/dxtecharts/common-techniques-to-improve-shadow-depth-maps">Advanced optimization of shadow map in MS doc</a>
     */
    void render(Scene scene, Terrain.Chunk[] chunks) {
        program.use();

        Projection projection = scene.getProjection();

        glEnable(GL_DEPTH_TEST);
        glEnable(GL_TEXTURE_2D);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
//        glCullFace(GL_FRONT);

        for (int i = 0; i < cascades.length; i++) {
            DepthBuffer buffer = buffers[i];
            ShadowCascade cascade = cascades[i];

            buffer.getFrameBuffer().bind();
            glViewport(0, 0, buffer.getWidth(), buffer.getHeight());
            glClear(GL_DEPTH_BUFFER_BIT);

            cascade.update(scene.getDirectionalLight(), scene.getCamera().view(), projection);
            setClipSpaceTransform(lightProjectViews[i] = cascade.getProjection().mul(cascade.getView(), new Matrix4f()));

            for (Terrain.Chunk chunk : chunks) {
                setModel(new Matrix4f().translation(chunk.getX(), chunk.getY(), chunk.getZ()));
                chunk.getMesh().render();
            }
        }

        GLFrameBuffer.unbind();
        program.unuse();
    }
}
