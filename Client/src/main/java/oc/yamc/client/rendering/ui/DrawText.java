package oc.yamc.client.rendering.ui;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.ObjectBinding;
import oc.yamc.client.gl.GLTexture2D;
import oc.yamc.client.gl.GLVertexArray;
import oc.yamc.client.rendering.FontContext;
import oc.yamc.client.scene.Canvas2D;
import oc.yamc.client.ui.UIText;
import oc.yamc.client.util.VertexBuffer;

public class DrawText extends Drawer<UIText> {
    private FontContext context;
    private ObjectBinding<GLVertexArray> vertexArray;
    private GLVertexArray internal = GLVertexArray.alloc(false);
    private int texture = 0;

    public DrawText(FontContext context) { this.context = context; }

    public void mount(Canvas2D canvas2D, UIText component) {
        super.mount(canvas2D, component);
        component.width.bind(Bindings.createIntegerBinding(() -> context.getWidth(component.getText(), component.getSize()), component.content, component.size));
        this.vertexArray = Bindings.createObjectBinding(() -> {
            VertexBuffer buffer = canvas2D.getBuffer();
            this.texture = this.context.putData(component.content.get(), (float) component.getX(), (float) component.getY(), component.getColor(), component.getSize(), buffer);
            buffer.flush(internal);
            buffer.reset();
            return internal;
        }, component.content, component.size);
    }

    @Override
    public void draw(Context context) {
        GLVertexArray vertexArray = this.vertexArray.get();
        GLTexture2D.bind(0, texture);
//        GL11.glBindTexture(GL11.GL_TEXTURE_2D, texture);
        vertexArray.render();
    }
}
