package oc.yamc.client.rendering;

import lombok.Value;
import oc.yamc.client.gl.GLFrameBuffer;
import oc.yamc.client.gl.GLTexture2D;

import java.io.IOException;
import java.nio.ByteBuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.GL_CLAMP_TO_BORDER;
import static org.lwjgl.opengl.GL14.GL_DEPTH_TEXTURE_MODE;
import static org.lwjgl.opengl.GL14.GL_TEXTURE_COMPARE_FUNC;
import static org.lwjgl.opengl.GL30.*;

@Value
public class DepthBuffer {
    private GLFrameBuffer frameBuffer;
    private GLTexture2D texture;
    private int width, height;

    public static DepthBuffer create(int width, int height) throws IOException {
        // Create a FBO to render the depth map
        GLFrameBuffer fbo = GLFrameBuffer.alloc();

        // Create the depth map texture
        GLTexture2D texture2D = GLTexture2D.alloc();

        texture2D.bind();
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, (ByteBuffer) null);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
        glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, new float[]{ 1.0f, 0.0f, 0.0f, 0.0f });
        // this will break windows shadow map, dont really know why
//        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
        glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE, GL_LUMINANCE);

        // Attach the the depth map texture to the FBO
        fbo.bind();
        GLFrameBuffer.bindTexture2d(texture2D);
        // Set only depth
        GLFrameBuffer.drawBuffer(GL_NONE);
        GLFrameBuffer.readBuffer(GL_NONE);

        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
            throw new IOException("Could not alloc FrameBuffer");
        }

        GLFrameBuffer.unbind();

        return new DepthBuffer(fbo, texture2D, width, height);
    }
}
