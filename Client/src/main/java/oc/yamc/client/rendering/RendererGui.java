package oc.yamc.client.rendering;

import com.google.common.collect.Maps;
import javafx.util.Pair;
import oc.yamc.client.gl.GLShader;
import oc.yamc.client.gl.GLShaderProgram;
import oc.yamc.client.rendering.font.STBTTFontContext;
import oc.yamc.client.rendering.ui.DrawRectangle;
import oc.yamc.client.rendering.ui.DrawText;
import oc.yamc.client.rendering.ui.Drawer;
import oc.yamc.client.scene.Canvas2D;
import oc.yamc.client.scene.Scene;
import oc.yamc.client.ui.UIComponent;
import oc.yamc.client.ui.UIParent;
import oc.yamc.client.ui.UIRectangle;
import oc.yamc.client.ui.UIText;
import oc.yamc.resource.ResourceManager;
import org.joml.Matrix4f;
import org.lwjgl.opengl.GL11;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.Map;
import java.util.function.Supplier;

/**
 * render for any gui
 */
public class RendererGui {
    private GLShaderProgram program;
    private LinkedList<Pair<UIComponent, Matrix4f>> queue = new LinkedList<>();
    private DContext context;
    private final Object NO_RENDER = new Object();

    private int v_Model, u_UsingAlpha, u_UsingTexture;
    private Map<Class<?>, Supplier<Drawer<?>>> drawerMap = Maps.newHashMap();

    void setup(ResourceManager resourceManager) throws IOException {
        this.program = GLShaderProgram.alloc(GLShader.create(resourceManager.load("yamc/shader/gui.vert").cache(), GLShader.Type.VERTEX_SHADER),
                GLShader.create(resourceManager.load("yamc/shader/gui.frag").cache(), GLShader.Type.FRAGMENT_SHADER));
        program.use();

        v_Model = program.getUniformLocation("model");
        u_UsingAlpha = program.getUniformLocation("usingAlpha");
        u_UsingTexture = program.getUniformLocation("hasTexture");

        program.setUniform("projection", new Matrix4f().setOrtho(0, 854f, 480f, 0, 10, -10));
        GLShaderProgram.setUniform(v_Model, new Matrix4f().identity());

        context = new DContext();
        context.useTexture(true);
        context.useAlpha(true);

        byte[] cache = resourceManager.load("yamc/fonts/arial.ttf").cache();
        var context = new STBTTFontContext(ByteBuffer.allocateDirect(cache.length).put(cache).flip());
        drawerMap.put(UIText.class, () -> new DrawText(context));
        drawerMap.put(UIRectangle.class, DrawRectangle::new);
    }

    @SuppressWarnings("unchecked")
    private <T extends UIComponent> Drawer<T> getDrawer(Canvas2D canvas2D, T component) {
        Object cache = component.getCache();
        if (cache == null) {
            Supplier<Drawer<?>> supplier = drawerMap.get(component.getClass());
            if (supplier != null) {
                Drawer drawer = supplier.get();
                drawer.mount(canvas2D, component);
                component.setCache(drawer);
                cache = drawer;
            } else {
                component.setCache(NO_RENDER);
                cache = NO_RENDER;
            }
        }
        if (!(cache instanceof Drawer)) return null;
        return (Drawer) cache;
    }

    public void render(Scene scene, double partialTick) {
        program.use();

        GL11.glEnable(GL11.GL_TEXTURE_2D);
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        Canvas2D canvas2D = scene.getCanvas2D();

        canvas2D.update();
        queue.add(new Pair<>(canvas2D.getUiRoot(), new Matrix4f()));

        while (!queue.isEmpty()) {
            var pair = queue.poll();
            UIComponent child = pair.getKey();

            Matrix4f trans = new Matrix4f(pair.getValue())
                    .translate((float) child.getX(), (float) child.getY(), 0);
            Drawer<?> drawer = getDrawer(canvas2D, child);
            if (drawer != null) {
                context.setTransform(trans);
                drawer.draw(context);
            }
            if (child instanceof UIParent)
                for (UIComponent component : ((UIParent) child).getChildren())
                    queue.add(new Pair<>(component, trans));
        }
        program.unuse();
    }

    private class DContext implements Drawer.Context {
        Matrix4f transform = new Matrix4f();

        @Override
        public Matrix4f getTransform() {
            return transform;
        }

        @Override
        public void setTransform(Matrix4f transform) {
            GLShaderProgram.setUniform(v_Model, transform);
            this.transform = transform;
        }

        @Override
        public void useAlpha(boolean alpha) {
            GLShaderProgram.setUniform(u_UsingAlpha, alpha);
        }

        @Override
        public void useTexture(boolean useTexture) {
            GLShaderProgram.setUniform(u_UsingTexture, useTexture);
        }
    }
}
