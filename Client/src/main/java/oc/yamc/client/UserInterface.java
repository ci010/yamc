package oc.yamc.client;

import org.lwjgl.glfw.GLFWCursorPosCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWMouseButtonCallback;
import org.lwjgl.glfw.GLFWScrollCallback;

/**
 * The class that know wtf the user is actually "doing". It interprets the user inputs by passing the event to the actual user interface state.
 * <p>
 * Another way to do such things is that you just use a global event bus, and let a bus carries the raw user inputs and responses to dispatch them.
 * This will be a more flexible way with much freedom.
 * Though, it will "pay for the freedom", since it might be harder to be organized in some ways.
 * </p>
 */
public final class UserInterface {
    private final GameWindow gameWindow;
    private State state;

    UserInterface(GameWindow gameWindow) {
        this.gameWindow = gameWindow;
    }

    /**
     * Transit the current state to another state.
     *
     * @param state The state of user interface.
     */
    public final void transit(UserInterface.State state) {
        if (state == null) throw new IllegalStateException("Cannot transit when the user interface is not mounted!");
        if (this.state != null) this.state.onUnmounted();
        this.state = state;
        this.state.onMounted();
    }

    /**
     * Setup the input callback with GLFW interface.
     */
    void setupGLFWCallback() {
        new GLFWKeyCallback() {
            @Override
            public void invoke(long window, int key, int scancode, int action, int mods) {
                handleKeyPress(key, scancode, action, mods);
            }
        }.set(gameWindow.getHandle());
        new GLFWMouseButtonCallback() {
            @Override
            public void invoke(long window, int button, int action, int mods) {
                handleMousePress(button, action, mods);
            }
        }.set(gameWindow.getHandle());
        new GLFWCursorPosCallback() {
            @Override
            public void invoke(long window, double xpos, double ypos) {
                handleCursorMove(xpos, ypos);
            }
        }.set(gameWindow.getHandle());
        new GLFWScrollCallback() {
            @Override
            public void invoke(long window, double xoffset, double yoffset) {
                handleScroll(xoffset, yoffset);
            }
        }.set(gameWindow.getHandle());
    }

    /**
     * Handle the cursor move event
     */
    private void handleCursorMove(double x, double y) { state.handleCursorMove(x, y); }

    /**
     * Handle the mouse scroll event
     */
    private void handleScroll(double xoffset, double yoffset) { state.handleScroll(xoffset, yoffset); }

    /**
     * Handle the mouse button press event
     *
     * @param button    The button code
     * @param action    The action code
     * @param modifiers The modifier code
     * @see org.lwjgl.glfw.GLFW#GLFW_MOUSE_BUTTON_1
     */
    private void handleMousePress(int button, int action, int modifiers) { state.handleMousePress(button, action, modifiers); }

    /**
     * @param key
     * @param scancode
     * @param action
     * @param modifiers
     */
    private void handleKeyPress(int key, int scancode, int action, int modifiers) { state.handleKeyPress(key, scancode, action, modifiers); }

    /**
     * A certain game state that have an individual way to interpret the user input
     */
    public interface State {
        boolean handleCursorMove(double x, double y);

        boolean handleScroll(double xoffset, double yoffset);

        boolean handleMousePress(int button, int action, int modifiers);

        boolean handleKeyPress(int key, int scancode, int action, int modifiers);

        default void onMounted() {}

        default void onUnmounted() {}
    }
}
