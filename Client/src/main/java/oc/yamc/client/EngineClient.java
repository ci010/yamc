package oc.yamc.client;

import oc.yamc.Engine;
import oc.yamc.client.game.GameClientStandalone;
import oc.yamc.event.AsmEventBus;
import oc.yamc.event.EventBus;
import oc.yamc.game.Game;
import oc.yamc.mod.ModRepository;
import oc.yamc.mod.ModRepositoryCollection;
import oc.yamc.mod.ModStore;
import oc.yamc.mod.ModStoreLocal;
import oc.yamc.player.Player;

import java.nio.file.Paths;
import java.util.UUID;

public class EngineClient implements Engine {
    private GameWindow window;

    private EventBus bus;
    private ModStore store;
    private ModRepository repository;
    private Player.Profile playerProfile;
    private UserInterface userInterface;

    private GameClientStandalone game;

    EngineClient(int width, int height) {
        window = new GameWindow(width, height, Main.getName());
        userInterface = new UserInterface(window);
        bus = new AsmEventBus();
        store = new ModStoreLocal(Paths.get("mods"));
        repository = new ModRepositoryCollection();
        playerProfile = new Player.Profile(UUID.randomUUID(), 12);
    }

    public static UserInterface userInterface() {
        return ((EngineClient) Engine.instance()).userInterface;
    }

    void init() {
        window.init();
        userInterface.setupGLFWCallback();
    }

    @Override
    public Game startGame(Game.Option option) {
        if (game != null) game.terminate();

        if (option == null) option = new GameClient.Option();
        GameClient.Option clientOption;
        if (option instanceof GameClient.Option) {
            clientOption = (GameClient.Option) option;
        } else {
            clientOption = new GameClient.Option();
            clientOption.setMods(option.getMods());
            clientOption.setResources(option.getResources());
        }
        game = new GameClientStandalone(clientOption, repository, store, bus, window);
        game.run();

        return game;
    }

    public UserInterface getUserInterface() {
        return userInterface;
    }

    @Override
    public Side getSide() {
        return Side.CLIENT;
    }

    @Override
    public ModRepository getModRepository() {
        return repository;
    }

    @Override
    public ModStore getModStore() {
        return store;
    }

    @Override
    public GameClientStandalone getCurrentGame() {
        return game;
    }

    @Override
    public EventBus getEventBus() {
        return bus;
    }
}
