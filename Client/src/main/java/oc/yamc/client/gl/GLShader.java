package oc.yamc.client.gl;

import oc.yamc.Engine;
import org.lwjgl.opengl.*;

import java.lang.ref.Cleaner;
import java.nio.charset.StandardCharsets;

import static org.lwjgl.opengl.GL20.*;

public class GLShader implements Cleaner.Cleanable {
    private int shaderId;
    private Type type;
    private Cleaner.Cleanable cleanable;

    private GLShader(int shaderId, Type type) {
        this.shaderId = shaderId;
        this.type = type;
        this.cleanable = GLManager.INSTANCE.register(this, () -> glDeleteShader(shaderId));
    }

    public static GLShader create(byte[] content, Type type) {
        return create(new String(content, StandardCharsets.UTF_8), type);
    }

    public static GLShader create(String content, Type type) {
        int shaderId = glCreateShader(type.getGlEnum());
        glShaderSource(shaderId, content);

        glCompileShader(shaderId);
        if (glGetShaderi(shaderId, GL_COMPILE_STATUS) == 0) {
            Engine.getLogger().warn(String.format("Error compiling shader code for %s, log: %s", content,
                    glGetShaderInfoLog(shaderId, 2048)));
        }
        return new GLShader(shaderId, type);
    }

    public Type getType() {
        return type;
    }

    @Override
    public String toString() {
        return "GLShader{" + "programId=" + shaderId + ", type=" + type + '}';
    }

    int getShaderId() {
        return shaderId;
    }

    @Override
    public void clean() {
        cleanable.clean();
    }

    public enum Type {
        VERTEX_SHADER(GL_VERTEX_SHADER),
        TESSELLATION_CONTROL_SHADER(GL40.GL_TESS_CONTROL_SHADER),
        TESSELLATION_EVALUATION_SHADER(GL40.GL_TESS_EVALUATION_SHADER),
        TESSELLATION_CONTROL_SHADER_ARB(ARBTessellationShader.GL_TESS_CONTROL_SHADER),
        TESSELLATION_EVALUATION_SHADER_ARB(ARBTessellationShader.GL_TESS_EVALUATION_SHADER),
        GEOMATRY_SHADER(GL32.GL_GEOMETRY_SHADER),
        FRAGMENT_SHADER(GL_FRAGMENT_SHADER),
        COMPUTE_SHADER(GL43.GL_COMPUTE_SHADER),
        COMPUTE_SHADER_ARB(ARBComputeShader.GL_COMPUTE_SHADER),
        ;

        private int glEnum;

        Type(int gl) {
            glEnum = gl;
        }

        public int getGlEnum() {
            return glEnum;
        }
    }
}
