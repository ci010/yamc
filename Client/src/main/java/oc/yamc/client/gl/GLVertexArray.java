package oc.yamc.client.gl;

import lombok.Getter;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

import java.lang.ref.Cleaner;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL30.*;

public class GLVertexArray implements Cleaner.Cleanable {
    public enum DrawMode {
        TRIANGLES(GL_TRIANGLES), LINES(GL_LINES);

        public final int mode;

        DrawMode(int mode) {this.mode = mode;}
    }

    public static final GLVertexArray EMPTY = new GLVertexArray(0, -1, -1, 0, 0) {
        @Override
        public void render() {
        }
    };
    private final int id, data, index;
    private final Cleaner.Cleanable cleanable;
    @Getter
    private int counts, mode, size = 0;

    private GLVertexArray(int id, int data, int index, int counts, int mode) {
        this.id = id;
        this.data = data;
        this.index = index;
        this.counts = counts;
        this.mode = mode;
        this.cleanable = GLManager.INSTANCE.register(this, () -> {
            // Delete the VBOs
            glBindBuffer(GL_ARRAY_BUFFER, 0);

            glDeleteBuffers(data);
            if (index != -1) glDeleteBuffers(index);

            // Delete the VAO
            glBindVertexArray(0);
            glDeleteVertexArrays(id);
        });
    }

    public static void enableVertexAttrib(int location) {
        GL20.glEnableVertexAttribArray(location);
    }

    public static void disableVertexAttrib(int location) {
        glDisableVertexAttribArray(location);
    }

    public static void assignAttribute(int location, int size, int stride, int offset) {
        GL20.glVertexAttribPointer(location, size, GL11.GL_FLOAT, false, stride, offset);
        GL20.glEnableVertexAttribArray(location);
    }

    public static GLVertexArray alloc(boolean useIndex) {
        return new GLVertexArray(glGenVertexArrays(), glGenBuffers(), useIndex ? glGenBuffers() : -1, 0, GL11.GL_TRIANGLES);
    }

    public static GLVertexArray alloc() {
        return new GLVertexArray(glGenVertexArrays(), glGenBuffers(), -1, 0, GL11.GL_TRIANGLES);
    }

    @Override
    public String toString() {
        return "GLVertexArray { id: " + id + ", count: " + counts + " }";
    }

    public void bind() {
        glBindVertexArray(id);
    }

    public void bindData() {
        glBindBuffer(GL_ARRAY_BUFFER, data);
    }

    public void unbindData() {
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }

    public void putAll(int mode, int counts, int[] attributes, FloatBuffer buffer, IntBuffer indexBuffer) {
        if (indexBuffer != null ^ index != -1) {
            throw new IllegalArgumentException();
        }
        bind();

        if (this.size > counts) {
            glBindBuffer(GL_ARRAY_BUFFER, data);
            glBufferSubData(GL_ARRAY_BUFFER, 0, buffer);
        } else {
            glBindBuffer(GL_ARRAY_BUFFER, data);
            glBufferData(GL_ARRAY_BUFFER, buffer, GL_DYNAMIC_DRAW);
        }

        int stride = 0;

        for (int i = 0; i < attributes.length; i++) {
            stride += attributes[i] * Float.BYTES;
        }
        int offset = 0;
        for (int i = 0; i < attributes.length; i++) {
            assignAttribute(i, attributes[i], stride, offset);
            offset += attributes[i] * Float.BYTES;
        }

        if (indexBuffer != null) {
            if (this.size > counts) {
                glBindBuffer(GL_ARRAY_BUFFER, data);
                glBufferSubData(GL_ARRAY_BUFFER, 0, buffer);
            } else {
                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index);
                glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexBuffer, GL_DYNAMIC_DRAW);
            }
        }

        this.mode = mode;
        this.counts = counts;
        this.size = Math.max(size, counts);

        unbind();
    }

    public void render() {
        if (counts == 0) return;
        // Draw the mesh
        glBindVertexArray(id);

        if (this.index != -1)
            glDrawElements(mode, counts, GL_UNSIGNED_INT, 0);
        else
            glDrawArrays(mode, 0, counts);

        glBindVertexArray(0);
    }

    public void unbind() {
        glBindVertexArray(0);
    }

    @Override
    public void clean() {
        this.cleanable.clean();
    }
}
