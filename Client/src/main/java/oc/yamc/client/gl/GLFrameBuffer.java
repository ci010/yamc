package oc.yamc.client.gl;

import org.lwjgl.system.NativeType;

import java.lang.ref.Cleaner;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL30.*;

public class GLFrameBuffer implements Cleaner.Cleanable {
    private final int id;
    private final Cleaner.Cleanable cleanable;

    private GLFrameBuffer() {
        final int id = glGenFramebuffers();
        this.id = id;
        this.cleanable = GLManager.INSTANCE.register(this, () -> glDeleteFramebuffers(id));
    }

    public static GLFrameBuffer alloc() {
        return new GLFrameBuffer();
    }

    public static void unbind() {
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    public static void bindTexture2d(GLTexture2D texture) {
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, texture.id, 0);
    }

    public static void drawBuffer(@NativeType("GLenum") int buf) {
        glDrawBuffer(buf);
    }

    public static void readBuffer(@NativeType("GLenum") int buf) {
        glReadBuffer(buf);
    }

    public void bind() {
        glBindFramebuffer(GL_FRAMEBUFFER, id);
    }

    @Override
    public void clean() {
        cleanable.clean();
    }
}
