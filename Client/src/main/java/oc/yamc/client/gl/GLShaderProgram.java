package oc.yamc.client.gl;

import org.joml.*;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL31;
import org.lwjgl.system.MemoryStack;

import java.lang.ref.Cleaner;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

public class GLShaderProgram implements Cleaner.Cleanable {
    private final Cleaner.Cleanable cleanable;
    private int programId;
    private GLShader[] usingShader; // we just hold the reference here
    // if there is no shader program hold the shader, the shader will be cleaned automatically.

    private GLShaderProgram(int programId, GLShader[] usingShader) {
        this.programId = programId;
        this.cleanable = GLManager.INSTANCE.register(this, () -> GL20.glDeleteProgram(programId));
        this.usingShader = usingShader;
    }

    public static GLShaderProgram alloc(GLShader... shaders) {
        int programId = GL20.glCreateProgram();

        for (GLShader s : shaders)
            GL20.glAttachShader(programId, s.getShaderId());

        GL20.glLinkProgram(programId);
        GL20.glUseProgram(programId);
        GL20.glValidateProgram(programId);

        for (GLShader s : shaders)
            GL20.glDetachShader(programId, s.getShaderId());

        GL20.glUseProgram(0);

        return new GLShaderProgram(programId, shaders);
    }

    public int getUniformLocation(String name) {
        return GL20.glGetUniformLocation(programId, name);
    }

    public void use() {
        GL20.glUseProgram(programId);
    }

    public void unuse() {
        GL20.glUseProgram(0);
    }

    public void destroy() {
        GL20.glDeleteProgram(programId);
        programId = -1;
    }

    public int getAttributeLocation(String name) {
        return GL20.glGetAttribLocation(programId, name);
    }

    public void setUniform(String location, int value) {
        GLShaderProgram.setUniform(getUniformLocation(location), value);
    }

    public void setUniform(String location, float value) {
        GLShaderProgram.setUniform(getUniformLocation(location), value);
    }

    public void setUniform(String location, boolean value) {
        GLShaderProgram.setUniform(getUniformLocation(location), value);
    }

    public void setUniform(String location, Vector2f value) {
        GLShaderProgram.setUniform(getUniformLocation(location), value);
    }

    public void setUniform(String location, Vector3f value) {
        GLShaderProgram.setUniform(getUniformLocation(location), value);
    }

    public void setUniform(String location, Vector4f value) {
        GLShaderProgram.setUniform(getUniformLocation(location), value);
    }

    public void setUniform(String location, Matrix3f value) {
        GLShaderProgram.setUniform(getUniformLocation(location), value);
    }

    public void setUniform(String location, Matrix4f value) {
        GLShaderProgram.setUniform(getUniformLocation(location), value);
    }

    public int getUniformBlockIndex(String location) {
        return GL31.glGetUniformBlockIndex(programId, location);
    }

    public void bindUniformBlock(int blockIndex, int bindingPoint) {
        GL31.glUniformBlockBinding(programId, blockIndex, bindingPoint);
    }

    public void bindUniformBlock(String blockLocation, int bindingPoint) {
        GL31.glUniformBlockBinding(programId, getUniformBlockIndex(blockLocation), bindingPoint);
    }

    public int[] getUniformIndices(String... names) {
        int[] result = new int[names.length];
        try (MemoryStack stack = MemoryStack.stackPush()) {
            IntBuffer buffer = stack.mallocInt(names.length);
            GL31.glGetUniformIndices(programId, names, buffer);
            buffer.get(result);
        }
        return result;
    }

    public int[] getUniformsOffsets(String... names) {
        int[] result = new int[names.length];
        try (MemoryStack stack = MemoryStack.stackPush()) {
            IntBuffer buffer = stack.mallocInt(names.length),
                    resultBuffer = stack.mallocInt(names.length);
            GL31.glGetUniformIndices(programId, names, buffer);
            GL31.glGetActiveUniformsiv(programId, buffer, GL31.GL_UNIFORM_OFFSET, resultBuffer);
            resultBuffer.get(result);
        }
        return result;
    }

    public static void setUniform(int location, int value) {
        GL20.glUniform1i(location, value);
    }

    public static void setUniform(int location, float value) {
        GL20.glUniform1f(location, value);
    }

    public static void setUniform(int location, boolean value) {
        GL20.glUniform1i(location, value ? 1 : 0);
    }

    public static void setUniform(int location, Vector2f value) {
        try (MemoryStack stack = MemoryStack.stackPush()) {
            FloatBuffer buffer = stack.mallocFloat(2);
            value.get(buffer);
            GL20.glUniform2fv(location, buffer);
        }
    }

    public static void setUniform(int location, Vector3f value) {
        try (MemoryStack stack = MemoryStack.stackPush()) {
            FloatBuffer buffer = stack.mallocFloat(3);
            value.get(buffer);
            GL20.glUniform3fv(location, buffer);
        }
    }

    public static void setUniform(int location, Vector4f value) {
        try (MemoryStack stack = MemoryStack.stackPush()) {
            FloatBuffer buffer = stack.mallocFloat(4);
            value.get(buffer);
            GL20.glUniform4fv(location, buffer);
        }
    }

    public static void setUniform(int location, Matrix3f value) {
        try (MemoryStack stack = MemoryStack.stackPush()) {
            FloatBuffer buffer = stack.mallocFloat(3 * 3);
            value.get(buffer);
            GL20.glUniformMatrix3fv(location, false, buffer);
        }
    }

    public static void setUniform(int location, Matrix4f value) {
        try (MemoryStack stack = MemoryStack.stackPush()) {
            FloatBuffer buffer = stack.mallocFloat(4 * 4);
            value.get(buffer);
            GL20.glUniformMatrix4fv(location, false, buffer);
        }
    }

    @Override
    public void clean() {
        cleanable.clean();
    }
}
