package oc.yamc.client.gl;

import de.matthiasmann.twl.utils.PNGDecoder;
import org.lwjgl.system.NativeType;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.Cleaner;
import java.nio.ByteBuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;
import static org.lwjgl.opengl.GL30.glGenerateMipmap;

public class GLTexture2D implements Cleaner.Cleanable {
    public final int id;
    private final Cleaner.Cleanable cleanable;

    private GLTexture2D() {
        id = glGenTextures();
        cleanable = GLManager.INSTANCE.register(this, () -> glDeleteTextures(id));
    }

    public static GLTexture2D alloc() {
        return new GLTexture2D();
    }

    public static GLTexture2D of(int width, int height, ByteBuffer buf) {
        if (!buf.isDirect()) {
            ByteBuffer direct = ByteBuffer.allocateDirect(buf.capacity());
            direct.put(buf);
            buf = direct;
        }
        GLTexture2D glTexture = GLTexture2D.alloc();
        glBindTexture(GL_TEXTURE_2D, glTexture.id);
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, buf);

        glGenerateMipmap(GL_TEXTURE_2D);
        return glTexture;
    }

    public static void parami(GLTexture2D texture2D, @NativeType("GLenum") int pname, @NativeType("GLint") int param) {
        glTexParameteri(GL_TEXTURE_2D, pname, param);
    }

    public static GLTexture2D ofPNG(InputStream stream) throws IOException {
        PNGDecoder decoder = new PNGDecoder(stream);
        ByteBuffer buf = ByteBuffer.allocateDirect(4 * decoder.getWidth() * decoder.getHeight());
        decoder.decode(buf, decoder.getWidth() * 4, PNGDecoder.Format.RGBA);
        buf.flip();

        return of(decoder.getWidth(), decoder.getHeight(), buf);
    }

    public void bind(int slot) {
        glActiveTexture(GL_TEXTURE0 + (slot % 32));
        glBindTexture(GL_TEXTURE_2D, id);
    }

    public void bind() {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, id);
    }

    public static void bind(int slot, int id) {
        glActiveTexture(GL_TEXTURE0 + (slot % 32));
        glBindTexture(GL_TEXTURE_2D, id);
    }

    @Override
    public String toString() {
        return "GLTexture2D { id: " + id + " }";
    }

    public void unbind() {
        glBindTexture(GL_TEXTURE_2D, 0);
    }

    @Override
    public void clean() {
        cleanable.clean();
    }
}
