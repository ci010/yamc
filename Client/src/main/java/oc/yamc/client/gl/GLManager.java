package oc.yamc.client.gl;

import oc.yamc.Engine;

import java.lang.ref.Cleaner;

enum GLManager {
    INSTANCE;

    private Cleaner cleaner = Cleaner.create();

    /**
     * TODO: queue the runnable to GL thread
     *
     * @param obj    The watching gl object
     * @param action The clean action
     * @return The cleanable object
     */
    Cleaner.Cleanable register(Object obj, Runnable action) {
        return cleaner.register(obj, () -> Engine.game().getContext().nextTick(action));
    }
}
