package oc.yamc.client.gl;

import lombok.Getter;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL30;
import org.lwjgl.system.MemoryStack;

import java.lang.ref.Cleaner;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

public class GLBuffer implements Cleaner.Cleanable {
    private Cleaner.Cleanable cleanable;
    private int id;
    @Getter
    private Type type;

    private long size;

    private GLBuffer(Type type) {
        this.id = GL15.glGenBuffers();
        this.type = type;
        this.cleanable = GLManager.INSTANCE.register(this, () -> GL15.glDeleteBuffers(id));
    }

    public static GLBuffer alloc(Type type) {
        return new GLBuffer(type);
    }

    public void bind() {
        GL15.glBindBuffer(type.code, id);
    }

    public void bindBufferBase(int bindingPoint) {
        GL30.glBindBufferBase(type.code, bindingPoint, id);
    }

    public void glBindBufferRange(int bindingPoint, long offset, long size) {
        GL30.glBindBufferRange(type.code, bindingPoint, id, offset, size);
    }

    public void bufferData(ByteBuffer b, Usage usage) {
        this.size = b.remaining();
        GL15.glBufferData(type.code, b, usage.code);
    }

    public void bufferData(long size, Usage usage) {
        this.size = size;
        GL15.glBufferData(type.code, size, usage.code);
    }


    public void bufferSubData(float value, long offset) {
        try (MemoryStack stack = MemoryStack.stackPush()) {
            FloatBuffer buffer = stack.mallocFloat(1);
            buffer.put(value).flip();
            GL15.glBufferSubData(type.code, offset, buffer);
        }
    }

    public void bufferSubData(boolean value, long offset) {
        try (MemoryStack stack = MemoryStack.stackPush()) {
            IntBuffer buffer = stack.mallocInt(1);
            buffer.put(value ? 1 : 0).flip();
            GL15.glBufferSubData(type.code, offset, buffer);
        }
    }

    public void bufferSubData(Vector3f value, long offset) {
        try (MemoryStack stack = MemoryStack.stackPush()) {
            FloatBuffer buffer = stack.mallocFloat(3);
            value.get(buffer);
            GL15.glBufferSubData(type.code, offset, buffer);
        }
    }

    public void bufferSubData(Matrix4f value, long offset) {
        try (MemoryStack stack = MemoryStack.stackPush()) {
            FloatBuffer buffer = stack.mallocFloat(4 * 4);
            value.get(buffer);
            GL15.glBufferSubData(type.code, offset, buffer);
        }
    }

    public void bufferSubData(ByteBuffer b, long offset) {
        GL15.glBufferSubData(type.code, offset, b);
    }

    public void unbind() {
        GL15.glBindBuffer(type.code, 0);
    }

    @Override
    public void clean() {
        cleanable.clean();
    }

    public enum Usage {
        STREAM_DRAW(0x88E0),
        STREAM_READ(0x88E1),
        STREAM_COPY(0x88E2),
        STATIC_DRAW(0x88E4),
        STATIC_READ(0x88E5),
        STATIC_COPY(0x88E6),
        DYNAMIC_DRAW(0x88E8),
        DYNAMIC_READ(0x88E9),
        DYNAMIC_COPY(0x88EA);

        private int code;

        Usage(int code) {this.code = code;}
    }

    public enum Type {
        UNIFORM_BUFFER(0x8A11), TEXTURE_BUFFER(0x8C2A), ARRAY_BUFFER(0x8892), ELEMENT_ARRAY_BUFFER(0x8893);

        private int code;

        Type(int code) {this.code = code;}
    }
}
