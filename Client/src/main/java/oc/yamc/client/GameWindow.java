package oc.yamc.client;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.glfw.GLFWWindowCloseCallback;
import org.lwjgl.opengl.GL;

import java.io.PrintStream;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryUtil.NULL;

/**
 * The class maintains the GLFW game window handle. It responses to communicate with GLFW things, like some controls over frame and frame rates.
 */
public class GameWindow {
    private long handle;
    private int width, height;
    private String title;
    private boolean resized;
    private long lastFPS = getTime();
    private int fps = 0;
    private int displayFPS = 0;

    GameWindow(int width, int height, String title) {
        this.title = title;
        this.width = width;
        this.height = height;
        resized = false;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public String getTitle() {
        return title;
    }

    public void setSize(int width, int height) {
        this.width = width;
        this.height = height;
        this.resized = true;
        glViewport(0, 0, width, height);
    }

    public void setTitle(String title) {
        glfwSetWindowTitle(handle, title);
    }

    public void init() {
        initErrorCallback(System.err);
        if (!glfwInit())
            throw new IllegalStateException("Unable to initialize GLFW");
        initWindowHint();
        handle = glfwCreateWindow(width, height, title, NULL, NULL);
        if (!checkCreated())
            throw new RuntimeException("Failed to alloc the GLFW window");
        initCallbacks();
        setWindowPosCenter();
        glfwMakeContextCurrent(handle);
        GL.createCapabilities();
        enableVSync();
        showWindow();

        int[] w = new int[1], h = new int[1];
        GLFW.glfwGetFramebufferSize(handle, w, h);
        this.width = w[0];
        this.height = h[0];
    }

    private boolean checkCreated() {
        return handle != NULL;
    }

    private void initCallbacks() {
        glfwSetFramebufferSizeCallback(handle, (window, width, height) -> {
            setSize(width, height);
        });
        new GLFWWindowCloseCallback() {
            @Override
            public void invoke(long window) {
            }
        }.set(handle);
    }

    private void initWindowHint() {
        glfwDefaultWindowHints();
        glfwWindowHint(GLFW_VISIBLE, GL_FALSE);
        glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    }

    private void initErrorCallback(PrintStream stream) {
        GLFWErrorCallback.createPrint(stream).set();
    }

    private void setWindowPosCenter() {
        GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
        // Center our window
        glfwSetWindowPos(handle, (vidmode.width() - width) / 2, (vidmode.height() - height) / 2);
    }

    private void enableVSync() {
        glfwSwapInterval(1);
    }

    private void showWindow() {
        glfwShowWindow(handle);
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        hideCursor();
    }

    private void setupTextCallback() {
//        glfwSetCharModsCallback(handle, (window, codepoint, mods) -> engineClient.handleTextInput(codepoint, mods));
    }

    public boolean shouldClose() {
        return glfwWindowShouldClose(handle);
    }

    public boolean getKey(int keyCode, int action) {
        return glfwGetKey(handle, keyCode) == action;
    }

    public void beginDraw() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        updateFPS();
    }

    public void endDraw() {
        glfwSwapBuffers(handle);
        glfwPollEvents();
    }

    private long getTime() {
        return System.nanoTime() / 1000000;
    }

    private void updateFPS() {
        long time = getTime();
        if (time - lastFPS > 1000) {
            displayFPS = fps;
            fps = 0; // reset the FPS counter
            lastFPS += 1000; // add one second
        }
        fps++;
    }

    public int getFps() {
        return displayFPS;
    }

    public long getHandle() {
        return handle;
    }

    public boolean isResized() {
        return resized;
    }

    public void showCursor() {
        glfwSetInputMode(handle, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    }

    public void hideCursor() {
        glfwSetInputMode(handle, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    }
}
