package oc.yamc.client;

import oc.yamc.Stub;
import oc.yamc.client.game.GameClientStandalone;
import org.apache.commons.lang3.SystemUtils;

public class Main {

    private static final String NAME = "${project.name}";
    private static final String VERSION = "${project.version}";

    public static final int WIDTH = 854, HEIGHT = 480;
    private static EngineClient engine;

    public static void main(String[] args) {
        if (SystemUtils.IS_OS_MAC && SystemUtils.JAVA_AWT_HEADLESS != null) { // TODO: require review: where should we
            // put this OS checking
            System.setProperty(SystemUtils.JAVA_AWT_HEADLESS, "true");
        }
        engine = new EngineClient(WIDTH, HEIGHT);
        Stub.setEngine(engine);
        engine.init();
        engine.startGame(null);
    }

    public static EngineClient getEngine() {
        return engine;
    }

    public static GameClientStandalone getGame() {
        return engine.getCurrentGame();
    }

    public static String getName() {
        return NAME;
    }

    public static String getVersion() {
        return VERSION;
    }
}
