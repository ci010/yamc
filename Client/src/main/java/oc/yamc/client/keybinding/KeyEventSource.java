package oc.yamc.client.keybinding;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Setter;
import oc.yamc.GameContext;
import oc.yamc.client.controller.Progress;
import oc.yamc.event.Event;
import oc.yamc.event.EventSource;
import oc.yamc.registry.Impl;

import javax.annotation.Nullable;

@Data
@EqualsAndHashCode(exclude = { "active", "valid", "target" })
public class KeyEventSource<T extends Event> extends Impl<EventSource<? extends Event>> implements EventSource<T> {
    private final KeyCode code;
    private final KeyModifier[] modifiers;
    private final ActionMode actionMode;
    private final Emitter<T> delegate;
    @Setter
    private boolean active = false;
    @Setter
    private boolean valid = true;
    @Setter
    private Progress status = Progress.BEGIN;

    private final Class<T> eventType;

    private KeyEventSource(Emitter<T> delegate, Class<T> eventType, KeyCode code, ActionMode actionMode, KeyModifier... keyMods) {
        this.delegate = delegate;
        this.eventType = eventType;
        this.code = code;
        this.actionMode = actionMode;
        this.modifiers = keyMods;
    }

    public static <T extends Event> KeyEventSource<T> create(Emitter<T> delegate, Class<T> eventType, KeyCode code, ActionMode actionMode, KeyModifier... keyMods) {
        return new KeyEventSource<>(delegate,
                eventType, code == null ? KeyCode.KEY_UNKNOWN : code,
                actionMode != null ? actionMode : ActionMode.PRESS,
                keyMods == null ? KeyModifier.EMPTY : keyMods);
    }

    @Override
    public T construct(GameContext gameContext) {
        return delegate.emit(this);
    }

    @Override
    public Class<T> getEventType() {
        return eventType;
    }

    @FunctionalInterface
    public interface Emitter<T> {
        T emit(@Nullable KeyEventSource options);
    }
}

