package oc.yamc.client.ui;

import lombok.Getter;
import oc.yamc.world.Block;

public class UIDisplayBlock extends UIComponent {
    @Getter
    private Block block;

    public UIDisplayBlock(Block block) {this.block = block;}
}
