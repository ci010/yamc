package oc.yamc.client.ui;

import javafx.beans.Observable;
import javafx.beans.binding.Bindings;
import javafx.collections.ListChangeListener;

/**
 * The basic layout that doesn't rearrange children's x, y...
 */
public class UIStackLayout extends UIParent {
    @Override
    protected void relayout(ListChangeListener.Change<? extends UIComponent> change) {
        Observable[] arr = new Observable[this.children.size()];
        for (int i = 0; i < this.children.size(); i++)
            arr[i] = children.get(i).height;
        this.height.bind(Bindings.createIntegerBinding(this::computeHeight, arr));

        arr = new Observable[this.children.size()];
        for (int i = 0; i < this.children.size(); i++)
            arr[i] = children.get(i).width;
        this.width.bind(Bindings.createIntegerBinding(this::computeWidth, arr));
    }

    protected int computeWidth() {
        double width = 0;
        for (int i = 0; i < this.children.size(); i++) {
            UIComponent child = this.children.get(i);
            width = Math.max(child.getX() + child.getWidth(), width);
        }
        return (int) width;
    }

    protected int computeHeight() {
        double height = 0;
        for (int i = 0; i < this.children.size(); i++) {
            UIComponent child = this.children.get(i);
            height = Math.max(child.getY() + child.getWidth(), height);
        }
        return (int) height;
    }
}
