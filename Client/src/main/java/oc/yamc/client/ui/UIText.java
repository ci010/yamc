package oc.yamc.client.ui;

import javafx.beans.binding.StringBinding;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import lombok.NonNull;

public class UIText extends UIComponent {
    public final IntegerProperty size = new SimpleIntegerProperty(16);
    public final IntegerProperty color = new SimpleIntegerProperty(0xffffffff);
    public final StringBinding content;

    public UIText(@NonNull StringBinding content) {
        this.content = content;
        this.height.bind(size);
    }

    public int getSize() {
        return size.get();
    }

    public void setSize(int size) {
        this.size.set(size);
    }

    public int getColor() {
        return color.get();
    }

    public String getText() {
        return content.get();
    }
}
