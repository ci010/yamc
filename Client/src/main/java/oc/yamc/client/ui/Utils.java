package oc.yamc.client.ui;

import lombok.experimental.UtilityClass;

@UtilityClass
class Utils {
    boolean inside(int x, int y, int sx, int sy, int w, int h) {
        return sx < x && x < sx + w && sy < y && y < sy + h;
    }

    boolean inside(double x, double y, double sx, double sy, double w, double h) {
        return sx < x && x < sx + w && sy < y && y < sy + h;
    }
}
