package oc.yamc.client.ui;

import javafx.beans.Observable;
import javafx.beans.binding.Bindings;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.ListChangeListener;

public class UIHorizontalLayout extends UIParent {
    private IntegerProperty space = new SimpleIntegerProperty(5);
    private IntegerProperty cellSize = new SimpleIntegerProperty(-1);

//    private IntegerBinding heightBinding = Bindings.createIntegerBinding(this::computeHeight, space, cellSize);
//    private IntegerBinding widthBinding = Bindings.createIntegerBinding(this::computeWidth);

    @Override
    protected void relayout(ListChangeListener.Change<? extends UIComponent> change) {

//        for (UIComponent component : change.getAddedSubList()) {
//            component.height.addListener(heightBinding);
//        }
        Observable[] arr = new Observable[this.children.size() + 2];
        for (int i = 0; i < this.children.size(); i++)
            arr[i] = children.get(i).height;
        arr[children.size()] = space;
        arr[children.size() + 1] = cellSize;

        this.height.bind(Bindings.createIntegerBinding(this::computeHeight, arr));
        arr = new Observable[this.children.size() + 2];
        for (int i = 0; i < this.children.size(); i++)
            arr[i] = children.get(i).width;

        arr[children.size()] = space;
        arr[children.size() + 1] = cellSize;

        this.width.bind(Bindings.createIntegerBinding(this::computeWidth, arr));
    }

    protected int computeWidth() {
        int offset = 0;
        for (int i = 0; i < this.children.size(); i++) {
            UIComponent child = this.children.get(i);
            child.setX(0);
            child.setY(offset);
            offset += getCellSize(child) + space.get();
        }
        return offset;
    }

    protected int computeHeight() {
        int height = 0;
        for (int i = 0; i < this.children.size(); i++) {
            UIComponent child = this.children.get(i);
            height = Math.max(child.getHeight(), height);
        }
        return height;
    }

    protected int getCellSize(UIComponent child) {
        return cellSize.get() == -1 ? child.getWidth() : cellSize.get();
    }
}
