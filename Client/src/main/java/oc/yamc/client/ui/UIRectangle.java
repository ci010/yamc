package oc.yamc.client.ui;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import org.joml.Vector4f;

public class UIRectangle extends UIComponent {
    public final ObjectProperty<Vector4f> color = new SimpleObjectProperty<>();

    public float getRed() { return color.get().x(); }

    public float getGreen() { return color.get().y(); }

    public float getBlue() { return color.get().z(); }

    public float getAlpha() { return color.get().w(); }
}
