package oc.yamc.client.ui;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.WeakListChangeListener;
import oc.yamc.client.keybinding.KeyCode;
import oc.yamc.client.keybinding.KeyModifier;

public abstract class UIParent extends UIComponent {
    private final ObservableList<UIComponent> internal = FXCollections.observableArrayList();
    protected final ObservableList<UIComponent> children = FXCollections.unmodifiableObservableList(internal);

    public UIParent() {
        this.internal.addListener(new WeakListChangeListener<UIComponent>(this::relayout));
    }

    protected abstract void relayout(ListChangeListener.Change<? extends UIComponent> change);

    public ObservableList<UIComponent> getChildren() {
        return children;
    }

    @Override
    public void onMouseMove(double x, double y) {
        boolean handled = false;
        for (UIComponent child : children) {
            if (Utils.inside(x, y, child.getX(), child.getY(), child.getWidth(), child.getHeight())) {
                child.onMouseMove(x - child.getX(), y - child.getY());
                handled = true;
            } else {
                child.hovered.set(false);
            }
        }
        if (!handled) super.onMouseMove(x, y);
    }

    @Override
    public void handleScroll(double x, double y, double xoffset, double yoffset) {
        boolean scrolled = false;
        for (UIComponent child : children) {
            if (Utils.inside(x, y, child.getX(), child.getY(), child.getWidth(), child.getHeight())) {
                child.handleScroll(x - child.getX(), y - child.getY(), xoffset, yoffset);
                scrolled = true;
            } else {
                child.hovered.set(false);
            }
        }
        if (!scrolled) super.handleScroll(x, y, xoffset, yoffset);
    }

    @Override
    public boolean handleMousePress(double x, double y, KeyCode keyCode, KeyModifier... modifier) {
        boolean prevent = false;
        for (UIComponent child : children) {
            if (Utils.inside(x, y, child.getX(), child.getY(), child.getWidth(), child.getHeight())) {
                prevent |= child.handleMousePress(x - child.getX(), y - child.getY(), keyCode, modifier);
            } else {
                child.focus.set(false);
            }
        }
        if (!prevent) return super.handleMousePress(x, y, keyCode, modifier);
        return true;
    }

    @Override
    public boolean handleMouseRelease(double x, double y, KeyCode keyCode, KeyModifier... modifier) {
        boolean prevent = false;
        for (UIComponent child : children) {
            if (Utils.inside(x, y, child.getX(), child.getY(), child.getWidth(), child.getHeight())) {
                prevent |= child.handleMouseRelease(x - child.getX(), y - child.getY(), keyCode, modifier);
            }
        }
        if (!prevent) return super.handleMouseRelease(x, y, keyCode, modifier);
        return true;
    }

//    @Override
//    protected void mount(Canvas2D canvas2D) {
//        super.mount(canvas2D);
//        for (UIComponent child : this.children)
//            child.mount(canvas2D);
//    }

    public UIParent addChild(UIComponent component) {
//        component.mount(this.getCanvas2D());
        internal.add(component);
        return this;
    }

    public void removeChild(UIComponent component) {
//        component.mount(null);
        internal.remove(component);
    }
}
