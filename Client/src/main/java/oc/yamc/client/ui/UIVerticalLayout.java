package oc.yamc.client.ui;

import javafx.beans.Observable;
import javafx.beans.binding.Bindings;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.ListChangeListener;

public class UIVerticalLayout extends UIParent {
    private IntegerProperty space = new SimpleIntegerProperty(5);
    private IntegerProperty cellSize = new SimpleIntegerProperty(-1);

    @Override
    protected void relayout(ListChangeListener.Change<? extends UIComponent> change) {
        Observable[] arr = new IntegerProperty[this.children.size() + 2];
        for (int i = 0; i < this.children.size(); i++)
            arr[i] = children.get(i).height;
        arr[children.size()] = space;
        arr[children.size() + 1] = cellSize;

        this.height.bind(Bindings.createIntegerBinding(this::computeHeight, arr));
        arr = new IntegerProperty[this.children.size() + 2];
        for (int i = 0; i < this.children.size(); i++)
            arr[i] = children.get(i).width;

        arr[children.size()] = space;
        arr[children.size() + 1] = cellSize;

        this.width.bind(Bindings.createIntegerBinding(this::computeWidth, arr));
    }

    protected int computeWidth() {
        int width = 0;
        for (int i = 0; i < this.children.size(); i++) {
            UIComponent child = this.children.get(i);
            width = Math.max(child.getWidth(), width);
        }
        return width;
    }

    protected int computeHeight() {
        int offset = 0;
        for (int i = 0; i < this.children.size(); i++) {
            UIComponent child = this.children.get(i);
            child.setX(0);
            child.setY(offset);
            offset += getCellSize(child) + space.get();
        }
        return offset;
    }

    protected int getCellSize(UIComponent child) {
        return cellSize.get() == -1 ? child.getHeight() : cellSize.get();
    }
}
