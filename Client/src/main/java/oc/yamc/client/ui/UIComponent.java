package oc.yamc.client.ui;

import com.google.common.collect.Lists;
import javafx.beans.property.*;
import lombok.NonNull;
import oc.yamc.client.keybinding.KeyCode;
import oc.yamc.client.keybinding.KeyModifier;

import java.util.List;

public class UIComponent {

    /**
     * The local position of x
     */
    public final DoubleProperty x = new SimpleDoubleProperty(0);
    /**
     * The local position of y
     */
    public final DoubleProperty y = new SimpleDoubleProperty(0);

    public final IntegerProperty width = new SimpleIntegerProperty(0),
            height = new SimpleIntegerProperty(0);

    public final BooleanProperty hovered = new SimpleBooleanProperty(false),
            focus = new SimpleBooleanProperty(false),
            visible = new SimpleBooleanProperty(false);

    public int getWidth() {
        return width.get();
    }

    public int getHeight() {
        return height.get();
    }

    private List<MouseMoveListener> mMove = null;
    private List<MouseScrollListener> mScroll = null;
    private List<KeyEventListener> kPress = null, kPressing = null, kRelease = null;
    private List<MouseKeyEventListener> mPress = null, mPressing = null, mRelease = null;
    /**
     * Cache for the client render
     */
    private Object cache;

    public double getY() {
        return y.get();
    }

    public void setY(double y) {
        this.y.set(y);
    }

    public double getX() {
        return x.get();
    }

    public void setX(double x) {
        this.x.set(x);
    }

    /**
     * @param x local position of the x
     * @param y local position of the y
     */
    public void onMouseMove(double x, double y) {
        this.hovered.set(true);
        if (mMove != null) {
            for (MouseMoveListener listener : mMove) {
                listener.handle(this, x, y);
            }
        }
    }

    /**
     * @param x local position of the x
     * @param y local position of the y
     */
    public void handleScroll(double x, double y, double xoffset, double yoffset) {
        if (mScroll != null) {
            for (MouseScrollListener listener : mScroll) {
                listener.handle(this, x, y, xoffset, yoffset);
            }
        }
    }

    /**
     * @param x local position of the x
     * @param y local position of the y
     */
    public boolean handleMousePress(double x, double y, KeyCode keyCode, KeyModifier... modifier) {
        this.focus.set(true);
        return fireMouseEvent(mPress, x, y, keyCode, modifier);
    }

    public boolean handleMousePressing(double x, double y, KeyCode keyCode, KeyModifier... modifier) {
        return fireMouseEvent(mPressing, x, y, keyCode, modifier);
    }

    public boolean handleMouseRelease(double x, double y, KeyCode keyCode, KeyModifier... modifier) {
        return fireMouseEvent(mRelease, x, y, keyCode, modifier);
    }

    private boolean fireMouseEvent(List<MouseKeyEventListener> ls, double x, double y, KeyCode keyCode, KeyModifier... modifier) {
        boolean prevent = false;
        if (ls != null)
            for (MouseKeyEventListener listener : ls) {
                prevent |= listener.handle(this, x, y, keyCode, modifier);
            }
        return prevent;
    }

    public boolean handleKeyPress(KeyCode keyCode, KeyModifier... modifier) { return fireKeyEvent(kPress, keyCode, modifier); }

    public boolean handleKeyRelease(KeyCode keyCode, KeyModifier... modifier) { return fireKeyEvent(kRelease, keyCode, modifier); }

    public boolean handleKeyPressing(KeyCode keyCode, KeyModifier... modifier) { return fireKeyEvent(kPressing, keyCode, modifier); }

    private boolean fireKeyEvent(List<KeyEventListener> ls, KeyCode keyCode, KeyModifier... modifier) {
        boolean prevent = false;
        if (ls != null) {
            for (KeyEventListener listener : ls) {
                prevent |= listener.handle(this, keyCode, modifier);
            }
        }
        return prevent;
    }

    public UIComponent onKeyPress(@NonNull KeyEventListener listener) {
        if (kPress == null) kPress = Lists.newArrayList();
        kPress.add(listener);
        return this;
    }

    public UIComponent onKeyPressing(@NonNull KeyEventListener listener) {
        if (kPressing == null) kPressing = Lists.newArrayList();
        kPressing.add(listener);
        return this;
    }

    public UIComponent onKeyPressRelease(@NonNull KeyEventListener listener) {
        if (kRelease == null) kRelease = Lists.newArrayList();
        kRelease.add(listener);
        return this;
    }

    public UIComponent onMousePress(@NonNull MouseKeyEventListener listener) {
        if (mPress == null) mPress = Lists.newArrayList();
        mPress.add(listener);
        return this;
    }

    public UIComponent onMousePressing(@NonNull MouseKeyEventListener listener) {
        if (mPressing == null) mPressing = Lists.newArrayList();
        mPressing.add(listener);
        return this;
    }

    public UIComponent onMousePressRelease(@NonNull MouseKeyEventListener listener) {
        if (mRelease == null) mRelease = Lists.newArrayList();
        mRelease.add(listener);
        return this;
    }

    public UIComponent onMouseScroll(@NonNull MouseScrollListener listener) {
        if (mScroll == null) mScroll = Lists.newArrayList();
        mScroll.add(listener);
        return this;
    }

    public UIComponent onMouseMove(@NonNull MouseMoveListener listener) {
        if (mMove == null) mMove = Lists.newArrayList();
        mMove.add(listener);
        return this;
    }

    public final Object getCache() {
        return cache;
    }

    public final void setCache(Object cache) {
        this.cache = cache;
    }

    @FunctionalInterface
    interface MouseScrollListener {
        void handle(UIComponent self, double x, double y, double xoffset, double yoffset);
    }

    @FunctionalInterface
    interface MouseMoveListener {
        void handle(UIComponent self, double x, double y);
    }

    @FunctionalInterface
    interface MouseKeyEventListener {
        boolean handle(UIComponent self, double x, double y, KeyCode code, KeyModifier... modifiers);
    }

    @FunctionalInterface
    interface KeyEventListener {
        boolean handle(UIComponent self, KeyCode code, KeyModifier... modifiers);
    }
}
