package oc.yamc.client.model;

import lombok.Value;
import oc.yamc.client.gl.GLTexture2D;
import oc.yamc.registry.Registry;
import oc.yamc.resource.ResourceManager;
import oc.yamc.world.Block;

import java.io.IOException;

public interface BlockModelFactory {
    Result process(ResourceManager resourceManager, Registry<Block> registry) throws IOException;

    @Value
    class Result {
        ResolvedBlockModel[] resolvedBlockModels;
        TextureAtlas atlas;
        GLTexture2D texture2D;

    }
}
