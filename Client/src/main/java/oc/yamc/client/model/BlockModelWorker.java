package oc.yamc.client.model;

import oc.yamc.registry.Registry;
import oc.yamc.resource.ResourceManager;
import oc.yamc.world.Block;

import java.io.IOException;
import java.util.Collection;

/**
 * The async worker for the block model. It should load the block model according to the block registry. This worker will run on other thread. Make sure you don't access gl context here.
 */
public abstract class BlockModelWorker {
    private ResourceManager resourceManager;
    private Registry<Block> blockRegistry;

    public Registry<Block> getBlockRegistry() {
        return blockRegistry;
    }

    public ResourceManager getResourceManager() {
        return resourceManager;
    }

    /**
     * Setup the context of this block model worker
     *
     * @param manager       The resource manager we are using
     * @param blockRegistry The block registry
     */
    final void setup(ResourceManager manager, Registry<Block> blockRegistry) throws IOException {
        this.resourceManager = manager;
        this.blockRegistry = blockRegistry;
        this.setup();
    }

    /**
     * A call hook to setup resources if necessary.
     */
    protected abstract void setup() throws IOException;

    /**
     * Resolve the textures and stitch the atlas by atlas builder.
     *
     * @return All texture parts
     */
    public abstract Collection<TextureAtlas.Part> resolveTextures() throws IOException;

    /**
     * The final step that we put the resolved the model to model registry.
     *
     * @param textureAtlas  The texture atlas we have
     * @param modelRegistry The output model registry.
     */
    public abstract void resolveModels(TextureAtlas textureAtlas, Registry.Extension<Block, ResolvedBlockModel> modelRegistry);

    public final void dispose() {
        this.resourceManager = null;
        this.blockRegistry = null;
        this.clear();
    }

    /**
     * Clear all cache store in this context cycle.
     */
    protected abstract void clear();
}
