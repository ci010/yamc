package oc.yamc.client.model;

import lombok.Data;

@Data
public class ResolvedBlockModel {
    private final Cube[] cubes;
    private final boolean full;
    private final Transparency transparency;
    private final Mesh wholeBlock;

    @Data
    public static class Cube {
        private final Mesh[] faces;
    }

    public enum Transparency {
        /**
         * All pixel has alpha 255
         */
        OPAQUE,
        /**
         * Exist a pixel with alpha in range (0, 255).
         */
        TRANSLUCENT,
        /**
         * Exist a pixel with alpha 0. Doesn't exist a pixel in range (0, 255).
         */
        TRANSPARENT
    }
}
