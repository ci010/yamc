package oc.yamc.client.model;

import com.google.common.collect.ImmutableMap;
import lombok.Value;

import java.nio.ByteBuffer;

@Value
public class TextureAtlas {
    int dimension;
    ImmutableMap<String, Part> atlas;

    @Value
    public static class Part {
        public final String name;
        public final int width;
        public final int height;
        public final int offsetX;
        public final int offsetY;
        public final ByteBuffer buffer;

        public Part(String name, int width, int height, int offsetX, int offsetY, ByteBuffer buffer) {
            this.name = name;
            this.width = width;
            this.height = height;
            this.offsetX = offsetX;
            this.offsetY = offsetY;
            this.buffer = buffer;
        }

        public Part(String name, int width, int height, ByteBuffer buffer) {
            this.name = name;
            this.width = width;
            this.height = height;
            this.buffer = buffer;
            this.offsetX = 0;
            this.offsetY = 0;
        }
    }
}
