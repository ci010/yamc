package oc.yamc.client.model;

import lombok.Data;
import oc.yamc.client.gl.GLVertexArray;
import org.lwjgl.system.MemoryUtil;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

@Data
public class Mesh {
    private final float[] vertices;
    private final float[] uv;
    private final float[] normals;
    private final int[] indices;
    private final int mode;

    public static GLVertexArray compact(Mesh t) {
        if (t == null) return null;
        FloatBuffer dataBuf = null;
        IntBuffer indicesBuffer = null;
        int[] indices = t.getIndices();
        float[] positions = t.getVertices();
        float[] textCoords = t.getUv();
        float[] normals = t.getNormals();
        int mode = t.getMode();
        GLVertexArray alloc = GLVertexArray.alloc(true);
        try {
            int vertexCount = indices.length;
            dataBuf = MemoryUtil.memAllocFloat(positions.length + textCoords.length + normals.length);
            int pIndex = 0, tIndex = 0, nIndex = 0;
            while (pIndex < positions.length) {
                dataBuf.put(positions, pIndex, 3)
                        .put(textCoords, tIndex, 2)
                        .put(normals, nIndex, 3);
                pIndex += 3;
                tIndex += 2;
                nIndex += 3;
            }
            dataBuf.flip();
            indicesBuffer = MemoryUtil.memAllocInt(indices.length);
            indicesBuffer.put(indices).flip();

            alloc.putAll(mode, vertexCount, new int[]{ 3, 2, 3 }, dataBuf, indicesBuffer);
            return alloc;
        } finally {
            if (dataBuf != null) {
                MemoryUtil.memFree(dataBuf);
            }
            if (indicesBuffer != null) {
                MemoryUtil.memFree(indicesBuffer);
            }
        }
    }
}
