package oc.yamc.client.model;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

import java.nio.ByteBuffer;
import java.util.*;
import java.util.stream.Collectors;

public class DefaultTextureAtlasBuilder implements TextureAtlasBuilder {
    @Override
    public TextureAtlas stitch(Collection<TextureAtlas.Part> inParts) {
        var parts = Lists.newArrayList(inParts).stream().map((v) -> new TexturePart(v.name, v.width, v.height, v.buffer))
                .sorted(Comparator.<TexturePart>comparingInt(a -> a.height).reversed()).collect(Collectors.toList());
        PriorityQueue<FreeSpace> queue = new PriorityQueue<>(Comparator.comparingInt(a -> a.height * a.width));
        List<FreeSpace> unaccepted = new ArrayList<>();
        int dimension = 512;
        queue.add(new FreeSpace(0, 0, dimension, dimension));

        for (TexturePart part : parts) {
            boolean accepted = false;
            while (!accepted) {
                while (!queue.isEmpty()) {
                    FreeSpace free = queue.poll();
                    if (free == null) {
                        break;
                    }
                    if (free.accept(queue, part)) {
                        accepted = true;
                        break;
                    }
                    unaccepted.add(free);
                }
                unaccepted.addAll(queue);
                queue.clear();

                for (FreeSpace a : unaccepted) {
                    if (!a.valid)
                        continue;
                    for (int i = 0; i < unaccepted.size(); i++) {
                        FreeSpace b = unaccepted.get(i);
                        if (a == b || !b.valid || !a.merge(b))
                            continue;
                        i = 0;
                    }
                }
                for (FreeSpace space : unaccepted)
                    if (space.valid)
                        queue.add(space);

                unaccepted.clear();
                if (!accepted) {
                    queue.add(new FreeSpace(dimension, 0, dimension, dimension));
                    queue.add(new FreeSpace(0, dimension, dimension + dimension, dimension));
                    dimension = dimension * 2;
                }
            }
        }

        return new TextureAtlas(dimension, ImmutableMap.copyOf(parts.stream().collect(Collectors.toMap((v) -> v.name, (v) ->
                new TextureAtlas.Part(v.name, v.width, v.height, v.offsetX, v.offsetY, v.buffer)))));
    }

    class TexturePart {
        String name;
        int width;
        int height;
        int offsetX;
        int offsetY;
        ByteBuffer buffer;

        TexturePart(String name, int width, int height, ByteBuffer buffer) {
            this.name = name;
            this.width = width;
            this.height = height;
            this.buffer = buffer;
        }

        @Override
        public String toString() {
            return "TexturePart{" + "width=" + width + ", height=" + height + ", offsetX=" + offsetX + ", offsetY="
                    + offsetY + '}';
        }
    }

    class FreeSpace {
        int x, y, width, height;
        boolean valid = true;

        FreeSpace(int x, int y, int width, int height) {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }

        boolean merge(FreeSpace other) {
            if (x + width == other.x && other.height == this.height) {
                this.width += other.width;
                other.valid = false;
                return true;
            }
            if (x + height == other.y && other.width == this.width) {
                this.height += other.height;
                other.valid = false;
                return true;
            }
            return false;
        }

        boolean accept(PriorityQueue<FreeSpace> others, TexturePart part) {
            if (part.width <= width && part.height <= height) {
                int remainedWidth = width - part.width;
                int remainedHeight = height - part.height;
                part.offsetX = x;
                part.offsetY = y;
                if (remainedHeight != 0 && remainedWidth != 0) {
                    others.add(new FreeSpace(x + part.width, y, remainedWidth, part.height));
                    others.add(new FreeSpace(x, y + part.height, width + remainedWidth, remainedHeight));
                } else if (remainedWidth != 0)
                    others.add(new FreeSpace(x + part.width, y, remainedWidth, part.height));
                else
                    others.add(new FreeSpace(x, y + part.height, part.width, remainedHeight));
                return true;
            }
            return false;
        }

        @Override
        public String toString() {
            return "FreeSpace{" + "posX=" + x + ", posY=" + y + ", width=" + width + ", height=" + height + '}';
        }
    }
}
