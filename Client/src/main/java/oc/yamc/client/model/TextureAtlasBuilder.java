package oc.yamc.client.model;

import java.util.Collection;

public interface TextureAtlasBuilder {
    TextureAtlas stitch(Collection<TextureAtlas.Part> parts);
}
