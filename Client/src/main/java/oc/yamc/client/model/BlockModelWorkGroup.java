package oc.yamc.client.model;

import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import oc.yamc.client.gl.GLTexture2D;
import oc.yamc.registry.Registry;
import oc.yamc.resource.ResourceManager;
import oc.yamc.util.Call;
import oc.yamc.world.Block;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Supplier;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL30.glGenerateMipmap;

/**
 * The worker group of the block model parsing. It can accept different model format of block.
 */
@Slf4j
public class BlockModelWorkGroup {
    private List<BlockModelWorker> blockModelFactoryList;
    private Supplier<TextureAtlasBuilder> builderSupplier;

    public BlockModelWorkGroup(List<BlockModelWorker> blockModelFactoryList, Supplier<TextureAtlasBuilder> builderSupplier) {
        this.blockModelFactoryList = blockModelFactoryList;
        this.builderSupplier = builderSupplier;
    }

    /**
     * Process the model resources by block registry.
     *
     * @param resourceManager The resource manager
     * @param blockRegistry   The current block registry
     * @return The baked texture atlas
     */
    @SuppressWarnings("unchecked")
    public GLTexture2D process(ResourceManager resourceManager, Registry<Block> blockRegistry) {
        int size = blockModelFactoryList.size();
        CompletableFuture<?>[] futures = new CompletableFuture[size];
        boolean[] broken = new boolean[size]; // this record if one worker is broken during the process

        // setup stage
        for (int i = 0; i < size; i++) {
            BlockModelWorker worker = blockModelFactoryList.get(i);
            futures[i] = CompletableFuture.runAsync(Call.run(() -> worker.setup(resourceManager, blockRegistry)));
        }

        // wait the setup end
        for (int i = 0; i < size; i++) {
            try {
                futures[i].get();
            } catch (InterruptedException | ExecutionException e) {
                log.error("The worker {} fail during the setup stage.", blockModelFactoryList.get(i));
                log.error("The error is.", e);
                broken[i] = true;
            }
        }

        // resolve texture stage
        TextureAtlasBuilder builder = builderSupplier.get();
        for (int i = 0; i < size; i++) {
            if (broken[i]) continue;
            BlockModelWorker worker = blockModelFactoryList.get(i);
            futures[i] = CompletableFuture.supplyAsync(Call.call(worker::resolveTextures));
        }

        List<TextureAtlas.Part> parts = Lists.newArrayList();
        for (int i = 0; i < size; i++) {
            CompletableFuture<?> future = futures[i];
            List<TextureAtlas.Part> sub = Collections.emptyList();
            try {
                sub = (List<TextureAtlas.Part>) future.get();
            } catch (InterruptedException | ExecutionException e) {
                log.error("The worker {} fail during the resolving texture stage.", blockModelFactoryList.get(i));
                log.error("The error is.", e);
                broken[i] = true;
            }
            parts.addAll(sub);
        }

        // actually stitch texture
        TextureAtlas atlas = builder.stitch(parts);
        int dimension = atlas.getDimension();

        GLTexture2D glTexture = GLTexture2D.alloc();
        glBindTexture(GL_TEXTURE_2D, glTexture.id);
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

        nglTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, dimension, dimension, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
        for (TextureAtlas.Part part : atlas.getAtlas().values()) {
            glTexSubImage2D(GL_TEXTURE_2D, 0, part.offsetX, dimension - part.offsetY - part.height, part.width,
                    part.height, GL_RGBA, GL_UNSIGNED_BYTE, part.buffer);
        }
        glGenerateMipmap(GL_TEXTURE_2D);

        var extension = blockRegistry.getExtension(ResolvedBlockModel.class);
        for (int i = 0; i < size; i++) {
            if (broken[i]) continue;
            BlockModelWorker worker = blockModelFactoryList.get(i);
            futures[i] = CompletableFuture.runAsync(() -> worker.resolveModels(atlas, extension));
        }

        for (int i = 0; i < size; i++) {
            try {
                futures[i].get();
            } catch (InterruptedException | ExecutionException e) {
                log.error("The worker {} fail during the resolving model stage.", blockModelFactoryList.get(i));
                log.error("The error is.", e);
                broken[i] = true;
            }
        }

        for (int i = 0; i < size; i++) {
            futures[i] = CompletableFuture.runAsync(blockModelFactoryList.get(i)::dispose);
        }

        for (int i = 0; i < size; i++) {
            try {
                futures[i].get();
            } catch (InterruptedException | ExecutionException e) {
                log.error("The worker {} fail during the dispose stage.", blockModelFactoryList.get(i));
                log.error("The error is.", e);
                broken[i] = true;
            }
        }
        return glTexture;
    }
}
