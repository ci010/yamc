package oc.yamc.client.model;

import oc.yamc.client.gl.GLVertexArray;
import oc.yamc.registry.Registry;
import oc.yamc.world.Block;
import org.lwjgl.opengl.GL11;
import org.lwjgl.system.MemoryUtil;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.Arrays;

public class ChunkModelFactory {
    private Builder bakeBuilder = new Builder();
    private Registry.Extension<Block, ResolvedBlockModel> resolvedBlockModels;

    public ChunkModelFactory(Registry.Extension<Block, ResolvedBlockModel> resolvedBlockModels) {
        this.resolvedBlockModels = resolvedBlockModels;
    }

    public GLVertexArray update(GLVertexArray mesh, int[] chunk) {
        bakeBuilder.clear();

        ResolvedBlockModel[] bakeds = new ResolvedBlockModel[chunk.length];
        for (int j = 0; j < chunk.length; j++) {
            if (chunk[j] == 0)
                continue;
            bakeds[j] = resolvedBlockModels.getValue(chunk[j]);
        }
        boolean[] full = bake0(bakeds);

//        Baked baked = new Baked(full, bakeBuilder.bake(mesh));
        return bakeBuilder.bake(mesh);
    }

    public GLVertexArray bake(int[] chunk) {
        bakeBuilder.clear();
        ResolvedBlockModel[] bakeds = new ResolvedBlockModel[chunk.length];
        for (int j = 0; j < chunk.length; j++) {
            if (chunk[j] == 0)
                continue;
            bakeds[j] = resolvedBlockModels.getValue(chunk[j]);
        }
        boolean[] full = bake0(bakeds);
        return bakeBuilder.bake();
    }

    private boolean[] bake0(ResolvedBlockModel[] chunk) {
        int[] offsets = new int[]{ 1, -1, 256, -256, 16, -16 };
        int[] mask = new int[]{ 0xFF0, 0xFF0, 0x0FF, 0x0FF, 0xF0F, 0xF0F };

        int[] invMask = new int[]{ 0x00F, 0x00F, 0x0F0, 0x0F0, 0xF00, 0xF00 };
        int[] invFaceValidations = new int[]{ 0x00F, 0x0, 0x0F0, 0x0, 0xF00, 0x0 };
        ResolvedBlockModel[] facesCache = new ResolvedBlockModel[6];
        // facesCache represents the current pos block's 6 neighbor blocks

        boolean[] fullFace = new boolean[6];
        Arrays.fill(fullFace, true);
        for (int pos = 0; pos < 4096; bakeBuilder.mark(pos++)) {
            ResolvedBlockModel cur = chunk[pos];
            if (cur == null)
                continue; // if no block here, ignore

            Arrays.fill(facesCache, null);

            for (int i = 0; i < 6; i++) {
                if ((pos & invMask[i]) == invFaceValidations[i]) { // check the pos in global
                    if (!cur.isFull()) {
                        fullFace[i] = false;
                        break;
                    }
                }
            }

            for (int face = 0; face < 6; face++) { // iterate each face
                ResolvedBlockModel neighborBlock = null;
                int neighbor = pos + offsets[face]; // get the offset by lookup table
                if (neighbor < 4096 && neighbor >= 0 &&
                        ((neighbor ^ pos) & mask[face]) == 0) // this condition is interesting
                    // it detect if the offset overflow the bit
                    // for example:
                    // before 1 1 1 0 | 1 1 1 1 | 1 1 1 1
                    // offset 0 0 0 0 | 0 0 0 1 | 0 0 0 0
                    // after  1 1 1 1 | 0 0 0 0 | 1 1 1 1
                    // xor before and after and take mask for adding posY coord
                    // 0 0 0 1 | 0 0 0 0 | 0 0 0 0
                    // we will see the bit on posX is changed which we don't want
                    neighborBlock = chunk[neighbor];
                facesCache[face] = neighborBlock;
            }

            boolean allAround = true;
            for (int i = 0; i < 6; i++) {
                if (facesCache[i] == null || !facesCache[i].isFull()) { // check neighbor
                    allAround = false;
                    break;
                }
            }
            if (allAround)
                continue; // rounded block will never be rendered.

            // get the in getChunkXZ xyz position
            int x = ((pos >> 8) & 0xF);
            int y = ((pos >> 4) & 0xF);
            int z = (pos & 0xF);

            if (!cur.isFull()) { // if the current is not a full block, just put all faces into it
                for (int i = 0; i < cur.getCubes().length; i++) {
                    ResolvedBlockModel.Cube cube = cur.getCubes()[i];
                    for (int j = 0; j < 6; j++)
                        bakeBuilder.face(cube.getFaces()[j], x, y, z);
                }
            } else {
                for (int i = 0; i < cur.getCubes().length; i++)
                    for (int j = 0; j < 6; j++)
                        if (facesCache[j] == null)
                            bakeBuilder.face(cur.getCubes()[i].getFaces()[j], x, y, z); // TODO: emit the face to different layer of gl
            }
        }
        return fullFace;
    }

    static class Builder {
        int index = 0;
        int count = 0;
        private FloatBuffer fb = MemoryUtil.memAllocFloat(1048576);
        private IntBuffer ib = MemoryUtil.memAllocInt(1048576);
        private int[] lengthSum = new int[4096];

        void clear() {
            index = 0;
            count = 0;
            fb.clear();
            ib.clear();
            Arrays.fill(lengthSum, 0);
        }

        void face(Mesh fa, int x, int y, int z) {
            if (fa == null)
                return;
            float[] vert = fa.getVertices(), uv = fa.getUv(), norm = fa.getNormals();
            for (int i = 0; i < 4; i += 1) {
                int vertPointer = i * 3, uvPointer = i * 2, normPointer = i * 3;
                fb.put(vert[vertPointer] + x).put(vert[vertPointer + 1] + y).put(vert[vertPointer + 2] + z)
                        .put(uv[uvPointer]).put(uv[uvPointer + 1])
                        .put(norm[normPointer]).put(norm[normPointer + 1]).put(norm[normPointer + 2]);
            }
            ib.put(index).put(index + 1).put(index + 2).put(index + 2).put(index + 3).put(index);
            index += 4;

            count += 32; // 4 vertices, each vertex takes 8 floats
        }

        void mark(int pos) {
            lengthSum[pos] = count + (pos == 0 ? 0 : (lengthSum[pos - 1]));
        }

        GLVertexArray bake(GLVertexArray mesh) {
            fb.flip();
            ib.flip();
            mesh.putAll(GL11.GL_TRIANGLES, ib.limit(), new int[]{ 3, 2, 3 }, fb, ib);
            return mesh;
        }

        GLVertexArray bake() {
            return bake(GLVertexArray.alloc(true));
        }
    }
}
