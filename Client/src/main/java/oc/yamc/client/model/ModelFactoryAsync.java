package oc.yamc.client.model;

import java.util.List;

@Deprecated
public class ModelFactoryAsync {
    private List<BlockModelWorker> blockModelFactoryList;

//    static class Sub implements TextureAtlasBuilder {
//        private TextureAtlasBuilder builder;
//        private CountDownLatch latch;
//        private CompletableFuture<TextureAtlas> future;
//
//        Sub(TextureAtlasBuilder builder, CountDownLatch latch, CompletableFuture<TextureAtlas> future) {
//            this.builder = builder;
//            this.latch = latch;
//            this.future = future;
//        }
//
//        @Override
//        public void add(String name, int width, int height, ByteBuffer data) {
//            builder.add(name, width, height, data);
//        }
//
//        @Override
//        public TextureAtlas stitch() {
//            latch.countDown();
//            try {
//                return future.get();
//            } catch (InterruptedException | ExecutionException e) {
//                e.printStackTrace();
//            }
//            return null;
//        }
//    }
//
//    public void processModels(ResourceManager resourceManager, Registry<Block> blockRegistry) {
//        DefaultTextureAtlasBuilder builder = new DefaultTextureAtlasBuilder();
//        var latch = new CountDownLatch(blockModelFactoryList.size());
//        var future = new CompletableFuture<TextureAtlas>();
//
//        CompletableFuture<?>[] futures = new CompletableFuture[blockModelFactoryList.size()];
//        Registry.Extension<Block, ResolvedBlockModel> extension = blockRegistry.getExtension(ResolvedBlockModel.class);
//        for (int i = 0; i < blockModelFactoryList.size(); i++) {
//            BlockModelWorker worker = blockModelFactoryList.get(i);
//            futures[i] = CompletableFuture.runAsync(() -> {
////                try {
////                    worker.process(resourceManager, blockRegistry, extension, new Sub(builder, latch, future));
////                } catch (IOException e) {
////                    e.printStackTrace();
////                }
//            });
//        }
//
//        try {
//            latch.await();  // wait until latch counted down to 0
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//
//        TextureAtlas textureAtlas = builder.stitch();
//        int dimension = textureAtlas.getDimension();
//
//        GLTexture2D glTexture = GLTexture2D.alloc();
//        glBindTexture(GL_TEXTURE_2D, glTexture.id);
//        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
//        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
//        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
//
//        nglTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, dimension, dimension, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
//        for (TextureAtlas.Part part : textureAtlas.getAtlas().values()) {
//            glTexSubImage2D(GL_TEXTURE_2D, 0, part.offsetX, dimension - part.offsetY - part.height, part.width,
//                    part.height, GL_RGBA, GL_UNSIGNED_BYTE, part.buffer);
//        }
//        glGenerateMipmap(GL_TEXTURE_2D);
//
//        future.complete(textureAtlas);
//
//        try {
//            CompletableFuture.allOf(futures).get();
//        } catch (InterruptedException | ExecutionException e) {
//            e.printStackTrace();
//        }
//    }
}
