package oc.yamc.client.sound;

import oc.yamc.Engine;
import org.lwjgl.openal.AL10;
import org.lwjgl.stb.STBVorbis;
import org.lwjgl.system.MemoryStack;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

public class ALSound {
    private int soundId;
    private int channel;
    private int rate;
    private byte bitDepth;

    public ALSound(int soundId, int channel, int rate, byte bitDepth) {
        this.soundId = soundId;
        this.channel = channel;
        this.rate = rate;
        this.bitDepth = bitDepth;
    }

    public static ALSound ofOGG(ByteBuffer buffer) {
        MemoryStack.stackPush();
        IntBuffer channelb = MemoryStack.stackMallocInt(1);
        MemoryStack.stackPush();
        IntBuffer rateb = MemoryStack.stackMallocInt(1);

        ShortBuffer raw = STBVorbis.stb_vorbis_decode_memory(buffer, channelb, rateb);
        int channel = channelb.get();
        int rate = rateb.get();
        MemoryStack.stackPop();
        MemoryStack.stackPop();

        return of(raw, (byte) 16, rate, channel);
    }

    public static ALSound of(ShortBuffer buffer, byte bitDepth, int rate, int channel) {
        AL10.alGetError();
        int soundId = AL10.alGenBuffers();
        int format = 0;
        if (bitDepth == 8) {
            if (channel == 1) format = AL10.AL_FORMAT_MONO8;
            else if (channel == 2) format = AL10.AL_FORMAT_STEREO8;
        } else if (bitDepth == 16) {
            if (channel == 1) format = AL10.AL_FORMAT_MONO16;
            else if (channel == 2) format = AL10.AL_FORMAT_STEREO16;
        }
        AL10.alBufferData(soundId, format, buffer, rate);
        int err = AL10.alGetError();
        switch (err) {
            case AL10.AL_NO_ERROR:
                break;
            case AL10.AL_INVALID_ENUM:
                Engine.getLogger().warn("Cannot load sound! (Invalid enum) bit depth: {} channel: {}", bitDepth, channel);
                AL10.alDeleteBuffers(soundId);
                return null;
            case AL10.AL_OUT_OF_MEMORY:
                Engine.getLogger().warn("Cannot load sound! (Out fat memory)!");
                AL10.alDeleteBuffers(soundId);
                return null;
        }
        return new ALSound(soundId, channel, rate, bitDepth);
    }

    public int getSoundId() {
        return soundId;
    }

    public byte getBitDepth() {
        return bitDepth;
    }

    public int getChannel() {
        return channel;
    }

    public int getRate() {
        return rate;
    }

    public void dispose() {
        if (soundId != -1) {
            AL10.alDeleteBuffers(soundId);
            soundId = -1;
        }
    }
}
