package oc.yamc.client.game;

import lombok.Getter;
import oc.yamc.client.model.BlockModelWorker;
import oc.yamc.client.rendering.FontContext;
import oc.yamc.client.scene.Scene;
import oc.yamc.event.EventBus;
import oc.yamc.registry.RegistryManager;
import oc.yamc.resource.ResourceManager;

import java.util.Collections;
import java.util.List;

/**
 * The client resource setup event. It will be fired before the game start to setup resources
 */
public class ResourceSetupEvent extends oc.yamc.game.ResourceSetupEvent {
    @Getter
    private final Scene scene;
    private final List<BlockModelWorker> blockModelWorkers;
    private final List<FontContext.Loader> fontContexts;

    public ResourceSetupEvent(EventBus bus, RegistryManager registry, ResourceManager resourceManager, Scene scene, List<BlockModelWorker> blockModelWorkers, List<FontContext.Loader> fontContexts) {
        super(bus, registry, resourceManager);
        this.scene = scene;
        this.blockModelWorkers = blockModelWorkers;
        this.fontContexts = fontContexts;
    }

    /**
     * Register a block model worker to the game.
     *
     * @param worker The block model worker.
     * @see BlockModelWorker
     */
    public void addBlockModelWorker(BlockModelWorker worker) {
        blockModelWorkers.add(worker);
    }

    public void putFontContext(FontContext.Loader contextLoader) {
        fontContexts.add(contextLoader);
    }

    public List<BlockModelWorker> getBlockModelWorkers() {
        return Collections.unmodifiableList(blockModelWorkers);
    }
}
