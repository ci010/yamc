package oc.yamc.client.game;

import lombok.Getter;
import oc.yamc.GameContext;
import oc.yamc.client.scene.Scene;

public class GameReadyEvent extends oc.yamc.game.GameReadyEvent {
    @Getter
    private final Scene scene;

    public GameReadyEvent(GameContext context, Scene scene) {
        super(context);
        this.scene = scene;
    }
}
