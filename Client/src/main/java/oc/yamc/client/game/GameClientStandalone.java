package oc.yamc.client.game;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import oc.minecraft.MinecraftMod;
import oc.yamc.GameContext;
import oc.yamc.client.EngineClient;
import oc.yamc.client.GameClient;
import oc.yamc.client.GameWindow;
import oc.yamc.client.controller.*;
import oc.yamc.client.gl.GLTexture2D;
import oc.yamc.client.keybinding.ActionMode;
import oc.yamc.client.keybinding.KeyCode;
import oc.yamc.client.keybinding.KeyEventSource;
import oc.yamc.client.keybinding.KeyModifier;
import oc.yamc.client.model.*;
import oc.yamc.client.rendering.FontContext;
import oc.yamc.client.rendering.RendererCore;
import oc.yamc.client.scene.ProjectionPerspective;
import oc.yamc.client.scene.Scene;
import oc.yamc.entity.Entity;
import oc.yamc.event.EventBus;
import oc.yamc.event.EventHandler;
import oc.yamc.event.EventHandlerModule;
import oc.yamc.event.Listener;
import oc.yamc.game.GameBase;
import oc.yamc.game.RegisterEvent;
import oc.yamc.item.Item;
import oc.yamc.math.FixStepTicker;
import oc.yamc.mod.*;
import oc.yamc.player.Player;
import oc.yamc.registry.FrozenRegistryManager;
import oc.yamc.registry.ImmutableRegistry;
import oc.yamc.registry.Registry;
import oc.yamc.resource.ResourceManager;
import oc.yamc.resource.ResourceManagerSingleThread;
import oc.yamc.resource.ResourceSource;
import oc.yamc.resource.ResourceSourceBuiltin;
import oc.yamc.util.RayCast;
import oc.yamc.world.ChunkStore;
import oc.yamc.world.World;
import oc.yamc.world.WorldCommon;
import org.joml.Quaternionf;
import org.joml.Vector3f;
import org.lwjgl.opengl.GL11;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Supplier;

import static oc.yamc.client.controller.PlayerControllerTriggerEvent.Direction.*;

@Slf4j
public class GameClientStandalone extends GameBase implements GameClient {
    private ResourceManager resourceManager;

    private CameraController cameraController;
    private InterfaceInGame userInterface;

    private WorldCommon world;
    private Player player;
    private FixStepTicker.Dynamic ticker;

    private GameWindow window;
    private Scene scene = new Scene();
    private RendererCore rendererCore;

    // atlasBuilderSupplier are single usage fields... maybe remove later?
    private Supplier<TextureAtlasBuilder> atlasBuilderSupplier;

    public GameClientStandalone(GameClient.Option option, ModRepository repository, ModStore store, EventBus bus, GameWindow window) {
        super(option, repository, store, bus);
        this.window = window;
        this.atlasBuilderSupplier = option.getAtlasBuilderSupplier() != null ? option.getAtlasBuilderSupplier() : DefaultTextureAtlasBuilder::new;
        this.ticker = new FixStepTicker.Dynamic(this::clientTick, this::renderTick, 30);
    }

    /**
     * Get player client
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * @return the client world
     */
    @Override
    public WorldCommon getWorld() {
        return world;
    }

    @Override
    public ResourceManager getResourceManager() {
        return resourceManager;
    }

    public Scene getScene() {
        return scene;
    }

    @Override
    protected Stage createInitStage() { return new ClientInitStage(); }


    @Override
    public void run() {
        super.run();
        ticker.start(); // start to tick
    }

    @Override
    public void terminate() {
        ticker.stop();
    }

    @Override
    public boolean isTerminated() {
        return ticker.isStop();
    }

    @Listener
    public void reg(RegisterEvent event) {
        event.getRegistry().registerAll(
                KeyEventSource.create((b) -> new PlayerControllerTriggerEvent.Move(FORWARD, b.getStatus()), PlayerControllerTriggerEvent.Move.class, KeyCode.KEY_W, ActionMode.PRESS).localName("player.move.forward"),
                KeyEventSource.create((b) -> new PlayerControllerTriggerEvent.Move(BACKWARD, b.getStatus()), PlayerControllerTriggerEvent.Move.class, KeyCode.KEY_S, ActionMode.PRESS).localName("player.move.backward"),
                KeyEventSource.create((b) -> new PlayerControllerTriggerEvent.Move(LEFT, b.getStatus()), PlayerControllerTriggerEvent.Move.class, KeyCode.KEY_A, ActionMode.PRESS).localName("player.move.left"),
                KeyEventSource.create((b) -> new PlayerControllerTriggerEvent.Move(RIGHT, b.getStatus()), PlayerControllerTriggerEvent.Move.class, KeyCode.KEY_D, ActionMode.PRESS).localName("player.move.right"),
                KeyEventSource.create((b) -> new PlayerControllerTriggerEvent.Jump(b.getStatus()), PlayerControllerTriggerEvent.Jump.class, KeyCode.KEY_SPACE, ActionMode.PRESS).localName("player.move.jump"),
                KeyEventSource.create((b) -> new PlayerControllerTriggerEvent.Sneak(b.getStatus()), PlayerControllerTriggerEvent.Sneak.class, KeyCode.KEY_LEFT_SHIFT, ActionMode.PRESS, KeyModifier.SHIFT).localName("player.move.sneak"),
                KeyEventSource.create((b) -> new PlayerControllerTriggerEvent.LiftClick(b.getStatus()), PlayerControllerTriggerEvent.LiftClick.class, KeyCode.MOUSE_BUTTON_LEFT, ActionMode.PRESS).localName("player.mouse.left"),
                KeyEventSource.create((b) -> new PlayerControllerTriggerEvent.RightClick(b.getStatus()), PlayerControllerTriggerEvent.RightClick.class, KeyCode.MOUSE_BUTTON_RIGHT, ActionMode.PRESS).localName("player.mouse.right")
        );
        EventHandlerModule.Type click, motion;
        event.getRegistry().registerAll(
                click = new EventHandlerModule.Type() {
                    @Override
                    public EventHandlerModule create() {
                        return new ClickHandlerModule(this);
                    }
                }.localName("player.click"),
                motion = new EventHandlerModule.Type() {
                    @Override
                    public EventHandlerModule create() {
                        return new MotionHandlerModule(this);
                    }
                }.localName("player.motion")
        );
        bus.register(click.create());
        bus.register(motion.create());
    }

    private void clientTick() {
        for (Runnable nextTick = nextTicks.poll(); nextTick != null; nextTick = nextTicks.poll())
            nextTick.run();
        world.tick();
        scene.getDirectionalLight().getDirection().rotateX(0.01F);
    }

    /**
     * Actual render call
     *
     * @param partialTic
     */
    private void renderTick(double partialTic) {
        cameraController.update(player.getMountingEntity().getPosition(), player.getMountingEntity().getRotation());
        scene.update();
        window.beginDraw();
        rendererCore.render(window, scene, partialTic);
        window.endDraw();
    }

    protected class ClientInitStage extends Stage {
        @Override
        protected void constructStage() {
            super.constructStage();
            resourceManager = new ResourceManagerSingleThread();
            resourceManager.addResourceSource(new ResourceSourceBuiltin());

            scene = new Scene();
            scene.setProjection(new ProjectionPerspective(window.getWidth() / window.getHeight()));
//          scene.setProjection(ProjectionOrtho.symmetric(window.getWidth() / 100, window.getHeight() / 100, 1, 200));
            cameraController = new FirstPersonController(scene.getCamera());
        }

        @Override
        protected void resourceStage() {
            List<BlockModelWorker> workers = Lists.newArrayList();
            List<FontContext.Loader> loaders = Lists.newArrayList();
            bus.post(new ResourceSetupEvent(bus, context.getRegistry(), resourceManager, scene, workers, loaders));
            scene.setupTerrain(
                    new ChunkModelFactory(context.getBlockRegistry().getExtension(ResolvedBlockModel.class)),
                    new BlockModelWorkGroup(workers, atlasBuilderSupplier)
                            .process(resourceManager, context.getBlockRegistry()),
                    0);
            try {
                scene.getSky().setTexture(GLTexture2D.ofPNG(resourceManager.load("yamc/textures/skybox.png").open()));
                scene.getSky().setGlMesh(Mesh.compact(new Mesh(
                        new float[]{
                                128, 128, -128,
                                128, 128, 128,
                                128, -128, -128,
                                128, -128, 128, //East
                                -128, 128, -128,
                                -128, 128, 128,
                                -128, -128, -128,
                                -128, -128, 128, //West
                                -128, 128, -128,
                                128, 128, -128,
                                -128, -128, -128,
                                128, -128, -128, //North
                                128, 128, 128,
                                -128, 128, 128,
                                128, -128, 128,
                                -128, -128, 128, //South
                                128, 128, -128,
                                128, 128, 128,
                                -128, 128, -128,
                                -128, 128, 128, //Up
                                128, -128, -128,
                                128, -128, 128,
                                -128, -128, -128,
                                -128, -128, 128, //Down
                        },
                        new float[]{
                                2 * (1 / 3f), 0 * (1 / 2f),
                                (1 + 2) * (1 / 3f), 0 * (1 / 2f),
                                2 * (1 / 3f), 1 * (1 / 2f),
                                (1 + 2) * (1 / 3f), 1 * (1 / 2f), //East
                                1 * (1 / 3f), 0 * (1 / 2f),
                                0 * (1 / 3f), 0 * (1 / 2f),
                                1 * (1 / 3f), 1 * (1 / 2f),
                                0 * (1 / 3f), 1 * (1 / 2f), //West
                                1 * (1 / 3f), 0 * (1 / 2f),
                                (1 + 1) * (1 / 3f), 0 * (1 / 2f),
                                1 * (1 / 3f), 1 * (1 / 2f),
                                (1 + 1) * (1 / 3f), 1 * (1 / 2f), //North
                                0 * (1 / 3f), 1 * (1 / 2f),
                                1 * (1 / 3f), 1 * (1 / 2f),
                                0 * (1 / 3f), (1 + 1) * (1 / 2f),
                                1 * (1 / 3f), (1 + 1) * (1 / 2f), //South
                                1 * (1 / 3f), (1 + 1) * (1 / 2f),
                                (1 + 1) * (1 / 3f), (1 + 1) * (1 / 2f),
                                1 * (1 / 3f), 1 * (1 / 2f),
                                (1 + 1) * (1 / 3f), 1 * (1 / 2f), //Up
                                2 * (1 / 3f), 1 * (1 / 2f),
                                (1 + 2) * (1 / 3f), 1 * (1 / 2f),
                                2 * (1 / 3f), (1 + 1) * (1 / 2f),
                                (1 + 2) * (1 / 3f), (1 + 1) * (1 / 2f), //Down
                        },
                        new float[]{
                                0, 0, 0,
                                0, 0, 0,
                                0, 0, 0,
                                0, 0, 0,

                                0, 0, 0,
                                0, 0, 0,
                                0, 0, 0,
                                0, 0, 0,

                                0, 0, 0,
                                0, 0, 0,
                                0, 0, 0,
                                0, 0, 0,

                                0, 0, 0,
                                0, 0, 0,
                                0, 0, 0,
                                0, 0, 0,

                                0, 0, 0,
                                0, 0, 0,
                                0, 0, 0,
                                0, 0, 0,

                                0, 0, 0,
                                0, 0, 0,
                                0, 0, 0,
                                0, 0, 0
                        },
                        new int[]{
                                0, 2, 1,
                                2, 3, 1, //East
                                4, 5, 6,
                                6, 5, 7, //West
                                8, 10, 9,
                                10, 11, 9, //North
                                12, 14, 13,
                                13, 14, 15, //South
                                16, 17, 18,
                                18, 17, 19, //Up
                                20, 22, 21,
                                21, 22, 23 //Down

                        },
                        GL11.GL_TRIANGLES
                )));
            } catch (IOException e) {
                log.error("An error occurred during loading skybox", e);
            }

            rendererCore = new RendererCore();
            try {
                rendererCore.load(loaders, resourceManager);
            } catch (IOException e) {
                log.error("An error occurred during loading render core", e);
            }
        }

        @Override
        protected void finishStage() {
            bus.register(scene);

            ImmutableMap.Builder<Class<?>, ImmutableRegistry<?>> builder = ImmutableMap.builder();
            for (Map.Entry<Class<?>, Registry<?>> entry : context.getRegistry().getEntries())
                builder.put(entry.getKey(), ImmutableRegistry.freeze(entry.getValue()));
            context = new GameContext(new FrozenRegistryManager(builder.build()), bus, nextTicks);

            spawnWorld(null);

            player = world.playerJoin(new Player.Profile(UUID.randomUUID(), 12));
            player.getMountingEntity().getPosition().set(0, 15, -1);

            userInterface = new InterfaceInGame(context);
            userInterface.setCameraController(cameraController);
            EngineClient.userInterface().transit(userInterface);

            bus.post(new GameReadyEvent(context, scene));
        }

        @Override
        protected void decorateLoader(ModLoaderWrapper wrapper) {
            super.decorateLoader(wrapper);
            wrapper.addBuiltin(new ModContainerBuiltin(
                    ModMetadata.builder().identifier(MinecraftMod.identifier).build(),
                    null,
                    new MinecraftMod(),
                    ResourceSource.empty()));
        }
    }

    @Override
    public World spawnWorld(World.Config config) {
        world = new WorldCommon(context, new ChunkStore(context), null);
        return world;
    }

    class ClickHandlerModule extends EventHandlerModule {
        private final Type type;
        private final EventHandler[] handlers = new EventHandler[]{
                EventHandler.wrap(PlayerControllerTriggerEvent.LiftClick.class, (e) -> {
                    if (e.progress == Progress.BEGIN) handleLeft();
                }),
                EventHandler.wrap(PlayerControllerTriggerEvent.RightClick.class, (e) -> {
                    if (e.progress == Progress.BEGIN) handleRight();
                })
        };

        ClickHandlerModule(Type type) {this.type = type;}

        private void handleRight() {
            Player player = GameClientStandalone.this.player;

            if (player == null) return;
            Entity mountingEntity = player.getMountingEntity();
            World world = player.getWorld();

            if (mountingEntity == null || world == null) return;

            Entity.TwoHands twoHands = mountingEntity.getBehavior(Entity.TwoHands.class);
            if (twoHands == null) return;

            RayCast.Hit hit = world.raycast(mountingEntity.getPosition(), mountingEntity.getRotation(), 5);

            Item hand = twoHands.getMainHand();
            if (hand != null) {
                if (hit != null) {
                    hand.onUseBlockStart(world, mountingEntity, hand, hit);
                } else {
                    hand.onUseStart(world, mountingEntity, hand);
                }
            }
            if (hit != null) {
                if (hit.block.shouldActivated(world, mountingEntity, hit.position, hit.block)) {
                    hit.block.onActivated(world, mountingEntity, hit.position, hit.block);
                }
            }
        }

        private void handleLeft() {
            Player player = GameClientStandalone.this.player;

            if (player == null) return;
            Entity mountingEntity = player.getMountingEntity();
            World world = player.getWorld();

            if (mountingEntity == null || world == null) return;

            Entity.TwoHands twoHands = mountingEntity.getBehavior(Entity.TwoHands.class);
            if (twoHands == null) return;

            RayCast.Hit hit = world.raycast(mountingEntity.getPosition(), mountingEntity.getRotation(), 5);
            if (twoHands.getMainHand() != null) {
                if (hit != null) {
                    world.setBlock(hit.position, null);
//                            mainHand.onUseBlockStart(world, this, mainHand, hit);
                } else {
//                            mainHand.onUseStart(world, this, mainHand);
                }
            }
            if (hit != null) {
                world.setBlock(hit.position, null);
//                        if (hit.block.shouldActivated(world, this, hit.position, hit.block)) {
//                            hit.block.onActivated(world, this, hit.position, hit.block);
//                        }
            }
        }

        @Override
        public EventHandler[] get() {
            return handlers;
        }

        @Override
        public Type getType() {
            return type;
        }
    }

    class MotionHandlerModule extends EventHandlerModule {
        static final int RUNNING = 4, SNEAKING = 5,
                FORWARD = 0, BACKWARD = 1, LEFT = 2, RIGHT = 3,
                JUMPING = 6;
        float factor = 0.3f;
        private Vector3f movingDirection = new Vector3f();
        private boolean[] phases = new boolean[16];
        private final Type type;
        private final EventHandler[] all = new EventHandler[]{
                EventHandler.wrap(PlayerControllerTriggerEvent.Move.class, (e) -> accept(e.direction.ordinal(), e.progress)),
                EventHandler.wrap(PlayerControllerTriggerEvent.Jump.class, (e) -> accept(JUMPING, e.progress)),
                EventHandler.wrap(PlayerControllerTriggerEvent.Sneak.class, (e) -> accept(SNEAKING, e.progress)),
        };

        MotionHandlerModule(Type type) {this.type = type;}

        private void accept(int action, Progress progress) {
            if (progress == Progress.DOING) return;
            boolean phase = progress == Progress.BEGIN;
            if (phases[action] == phase) return;
            this.phases[action] = phase;

            Vector3f movingDirection = this.movingDirection;
            switch (action) {
                case FORWARD:
                case BACKWARD:
                    if (this.phases[FORWARD] == this.phases[BACKWARD]) {
                        movingDirection.z = 0;
                    }
                    if (this.phases[FORWARD]) movingDirection.z = -factor;
                    else if (this.phases[BACKWARD]) movingDirection.z = +factor;
                    break;
                case LEFT:
                case RIGHT:
                    if (this.phases[LEFT] == this.phases[RIGHT]) {
                        movingDirection.x = 0;
                    }
                    if (this.phases[RIGHT]) movingDirection.x = factor;
                    else if (this.phases[LEFT]) movingDirection.x = -factor;
                    break;
                case RUNNING:
                    break;
                case JUMPING:
                case SNEAKING:
                    if (this.phases[JUMPING] && !this.phases[SNEAKING]) {
                        movingDirection.y = factor;
                    } else if (this.phases[SNEAKING] && !this.phases[JUMPING]) {
                        movingDirection.y = -factor;
                    } else {
                        movingDirection.y = 0;
                    }
                    break;
            }
            Vector3f f = player.getMountingEntity().getRotation();
            f.y = 0;
            movingDirection.rotate(new Quaternionf().rotateTo(new Vector3f(0, 0, -1), f),
                    player.getMountingEntity().getMotion());
        }


        @Override
        public EventHandler[] get() {
            return all;
        }

        @Override
        public Type getType() {
            return type;
        }
    }
}