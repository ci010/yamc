package oc.minecraft.client;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.gson.Gson;
import de.matthiasmann.twl.utils.PNGDecoder;
import lombok.Data;
import oc.yamc.Engine;
import oc.yamc.client.model.BlockModelWorker;
import oc.yamc.client.model.Mesh;
import oc.yamc.client.model.ResolvedBlockModel;
import oc.yamc.client.model.ResolvedBlockModel.Transparency;
import oc.yamc.client.model.TextureAtlas;
import oc.yamc.registry.Registry;
import oc.yamc.resource.Resource;
import oc.yamc.resource.ResourceManager;
import oc.yamc.util.Facing;
import oc.yamc.world.Block;
import org.joml.AABBf;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.lwjgl.opengl.GL11;

import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.util.*;

import static oc.yamc.client.model.ResolvedBlockModel.Transparency.TRANSPARENT;

public final class MinecraftModelWorker extends BlockModelWorker {
    private Model[] models;

    @Override
    protected void setup() throws IOException {
        Collection<Block> values = getBlockRegistry().getValues();
        String[] paths = new String[values.size() - 1];
        models = new Model[values.size() - 1];
        for (Block value : values) {
            if (value.getLocalName().equals("air"))
                continue;
            int id = getBlockRegistry().getId(value) - 1;
            String[] split = value.getUniqueName().split("\\.");
            var ls = Lists.newArrayList(split);
            ls.add(1, "models");
            paths[id] = "/" + String.join("/", ls) + ".json";
        }

        int size = paths.length;
        for (int i = 0; i < size; i++) {
            String path = paths[i];
            Model model = loadModel(getResourceManager(), path);
            if (model == null) {
                Engine.getLogger().warn("The cannot load model from {}!", path);
            }
            models[i] = model;
        }
    }

    @Override
    public Collection<TextureAtlas.Part> resolveTextures() throws IOException {
        Set<String> visited = Sets.newHashSet();
        List<TextureAtlas.Part> list = Lists.newArrayList();
        for (int i = 0; i < models.length; i++) {
            Model model = models[i];
            if (model == null)
                continue;
            for (String variant : model.textures.keySet()) {
                String path = model.textures.get(variant);
                while (path.startsWith("#")) {
                    String next = model.textures.get(path.substring(1));
                    if (next == null) {
                        Engine.getLogger().warn("Missing texture for " + variant);
                        path = null;
                        break;
                    }
                    path = next;
                }

                if (path == null) {
                    Engine.getLogger().warn("Missing texture for " + model);
                    continue;
                }
                model.textures.put(variant, path);
                if (!visited.contains(path)) {
                    Resource resource = getResourceManager().load("minecraft/textures/" + path + ".png");
                    PNGDecoder decoder = new PNGDecoder(resource.open());
                    ByteBuffer buf = ByteBuffer.allocateDirect(4 * decoder.getWidth() * decoder.getHeight());
                    decoder.decode(buf, decoder.getWidth() * 4, PNGDecoder.Format.RGBA);
                    buf.flip();
                    list.add(new TextureAtlas.Part(path, decoder.getWidth(), decoder.getHeight(), 0, 0, buf));
                    visited.add(path);
                }
            }
        }
        return list;
    }

    @Override
    public void resolveModels(TextureAtlas textureAtlas, Registry.Extension<Block, ResolvedBlockModel> modelRegistry) {
        Map<String, Transparency> tMap = Maps.newHashMap();
        Map<Model, Transparency> transparencyMap = Maps.newHashMap();
        textureAtlas.getAtlas().forEach((k, v) -> tMap.put(k, resolveTransparency(v.getBuffer())));
        int dimension = textureAtlas.getDimension();
        for (Model m : models) {
            if (m == null)
                continue;
            Transparency transparency = Transparency.OPAQUE;
            for (Model.Element e : m.elements) {
                for (var face : e.faces.values()) {
                    if (face == null)
                        continue;
                    String path = m.textures.get(face.texture.substring(1));

                    var p = textureAtlas.getAtlas().get(path);

                    var trans = tMap.get(path);
                    if (trans == Transparency.TRANSLUCENT) {
                        transparency = Transparency.TRANSLUCENT;
                    } else if (trans == Transparency.TRANSPARENT && transparency == Transparency.OPAQUE) {
                        transparency = Transparency.TRANSPARENT;
                    }

                    if (face.uv == null)
                        face.uv = new float[]{ 0, 0, 16, 16 };
                    face.uv[0] = (face.uv[0] + p.offsetX) / dimension;
                    face.uv[1] = 1 - (face.uv[1] + p.offsetY) / dimension;
                    face.uv[2] = (face.uv[2] + p.offsetX) / dimension;
                    face.uv[3] = 1 - (face.uv[3] + p.offsetY) / dimension;
                }
            }
            transparencyMap.put(m, transparency);
        }

        for (int i = 0; i < models.length; i++) {
            Model model = models[i];
            ResolvedBlockModel resolvedBlockModel = resolveBlock(model, transparencyMap.get(model));
            if (resolvedBlockModel == null) {
                Engine.getLogger().warn("The cannot bake block model faces for {}!", models[i].toString());
            }
            modelRegistry.register(getBlockRegistry().getValue(i + 1), resolvedBlockModel);
        }

        for (int i = 0; i < models.length; i++) {
            Model model = models[i];
            ResolvedBlockModel resolvedBlockModel = resolveBlock(model, transparencyMap.get(model));
            if (resolvedBlockModel == null) {
                Engine.getLogger().warn("The cannot bake block model faces for {}!", models[i].toString());
            }
            modelRegistry.register(getBlockRegistry().getValue(i + 1), resolvedBlockModel);
        }
    }

    @Override
    protected void clear() {
        models = null;
    }

    ResolvedBlockModel resolveBlock(Model model, Transparency transparency) {
        if (model == null)
            return null;
        List<BakedBlockMinecraft.Cube> cubes = new ArrayList<>(6);
        final int X = 0, Y = 1, Z = 2;

        AABBf box = null;
        int volume = 0;
        boolean hasFull = false;
        for (Model.Element element : model.elements) {
            AABBf aabb = new AABBf(element.from[0], element.from[1], element.from[2], element.to[0], element.to[1],
                    element.to[2]);
            if (aabb.minZ == 0 && aabb.minX == 0 && aabb.minY == 0 && aabb.maxZ == 16 && aabb.maxX == 16
                    && aabb.maxY == 16) {
                hasFull = true;
                break;
            }
            volume += (aabb.maxX - aabb.minX) * (aabb.maxY - aabb.minX) * (aabb.maxZ - aabb.minZ);
            if (box == null) {
                box = aabb;
            } else {
                box.union(aabb);
            }
        }

        boolean full = hasFull || (volume == 4096 && box.minZ == 0 && box.minX == 0 && box.minY == 0 && box.maxZ == 16
                && box.maxX == 16 && box.maxY == 16);

        for (Model.Element element : model.elements) {
            float[] thisUv;
            float fx = element.from[X] / 16F, fy = element.from[Y] / 16F, fz = element.from[Z] / 16F;
            float tx = element.to[X] / 16F, ty = element.to[Y] / 16F, tz = element.to[Z] / 16F;
            Mesh[] faces = new Mesh[6];
            cubes.add(new BakedBlockMinecraft.Cube(faces, element.shade));

            // north
            if (element.faces.containsKey("north")) {
                thisUv = element.faces.get("north").uv;

                faces[Facing.NORTH.index] = new Mesh(new float[]{ fx, fy, tz, tx, fy, tz, tx, ty, tz, fx, ty, tz },
                        new float[]{ thisUv[0], thisUv[1], thisUv[2], thisUv[1], thisUv[2], thisUv[3], thisUv[0],
                                thisUv[3] },
                        new float[]{ 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1 }, new int[]{ 0, 1, 2, 0, 2, 3 }, -1);
            }

            // south
            if (element.faces.containsKey("south")) {
                thisUv = element.faces.get("south").uv;

                faces[Facing.SOUTH.index] = new Mesh(new float[]{ tx, fy, fz, fx, fy, fz, fx, ty, fz, tx, ty, fz },
                        new float[]{ thisUv[0], thisUv[1], thisUv[2], thisUv[1], thisUv[2], thisUv[3], thisUv[0],
                                thisUv[3] },
                        new float[]{ 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1 }, new int[]{ 0, 1, 2, 0, 2, 3 }, -1);
            }
            if (element.faces.containsKey("east")) {
                // right
                thisUv = element.faces.get("east").uv;

                faces[Facing.EAST.index] = new Mesh(new float[]{ tx, fy, tz, tx, fy, fz, tx, ty, fz, tx, ty, tz },
                        new float[]{ thisUv[0], thisUv[1], thisUv[2], thisUv[1], thisUv[2], thisUv[3], thisUv[0],
                                thisUv[3] },
                        new float[]{ 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, }, new int[]{ 0, 1, 2, 0, 2, 3 }, -1);
            }

            // left
            if (element.faces.containsKey("west")) {
                thisUv = element.faces.get("west").uv;

                faces[Facing.WEST.index] = new Mesh(new float[]{ fx, fy, fz, fx, fy, tz, fx, ty, tz, fx, ty, fz },
                        new float[]{ thisUv[0], thisUv[1], thisUv[2], thisUv[1], thisUv[2], thisUv[3], thisUv[0],
                                thisUv[3] },
                        new float[]{ 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1 }, new int[]{ 0, 1, 2, 0, 2, 3 }, -1);
            }

            // bottom
            if (element.faces.containsKey("down")) {
                thisUv = element.faces.get("down").uv;

                faces[Facing.DOWN.index] = new Mesh(new float[]{ fx, fy, fz, tx, fy, fz, tx, fy, tz, fx, fy, tz },
                        new float[]{ thisUv[0], thisUv[1], thisUv[2], thisUv[1], thisUv[2], thisUv[3], thisUv[0],
                                thisUv[3] },
                        new float[]{ 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, }, new int[]{ 0, 1, 2, 0, 2, 3 }, -1);
            }

            // top
            if (element.faces.containsKey("up")) {
                thisUv = element.faces.get("up").uv;

                faces[Facing.UP.index] = new Mesh(new float[]{ tx, ty, tz, tx, ty, fz, fx, ty, fz, fx, ty, tz },
                        new float[]{ thisUv[0], thisUv[1], thisUv[2], thisUv[1], thisUv[2], thisUv[3], thisUv[0],
                                thisUv[3] },
                        new float[]{ 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, }, new int[]{ 0, 1, 2, 0, 2, 3 }, -1);
            }

            if (element.rotation != null) {
                var trans = new Matrix4f().translate(-element.rotation.origin[0], -element.rotation.origin[1],
                        -element.rotation.origin[2]);
                var rot = new Matrix4f();
                var repo = new Matrix4f().translate(element.rotation.origin[0], element.rotation.origin[1],
                        element.rotation.origin[2]);
                switch (element.rotation.axis) {
                    case "posY":
                        rot.rotate((float) Math.toRadians(element.rotation.angle), 0, 1, 0);
                        break;
                    case "posX":
                        rot.rotate((float) Math.toRadians(element.rotation.angle), 1, 0, 0);
                        break;
                    case "posZ":
                        rot.rotate((float) Math.toRadians(element.rotation.angle), 0, 0, 1);
                        break;
                    default:
                        break;
                }

                for (var i = 0; i < 8; ++i) {
                    Mesh f = faces[i];
                    if (f == null)
                        continue;
                    float[] vs = f.getVertices();
                    float[] ns = f.getNormals();
                    for (var j = 0; j < vs.length; j += 3) {
                        var vec = new Vector3f(vs[j + 0], vs[j + 1], vs[j + 2]);
                        trans.transformPosition(vec);
                        rot.transformPosition(vec);
                        repo.transformPosition(vec);
                        vs[j + 0] = vec.x;
                        vs[j + 1] = vec.y;
                        vs[j + 2] = vec.z;
                        vec.set(ns[j + 0], ns[j + 1], ns[j + 2]);
                        rot.transformPosition(vec);
                        ns[j + 0] = vec.x;
                        ns[j + 1] = vec.y;
                        ns[j + 2] = vec.z;
                    }
                }
            }
        }
        Mesh whole;
        {
            float[] vertices = new float[model.elements.length * 24 * 3];
            float[] uv = new float[model.elements.length * 24 * 2];
            float[] normals = new float[model.elements.length * 24 * 3];
            int vp = 0, uvp = 0, np = 0;

            for (var c : cubes) {
                for (Mesh mesh : c.getFaces()) {
                    if (mesh == null) continue;
                    System.arraycopy(mesh.getVertices(), 0, vertices, vp, mesh.getVertices().length);
                    vp += mesh.getVertices().length;
                    System.arraycopy(mesh.getUv(), 0, uv, uvp, mesh.getUv().length);
                    uvp += mesh.getUv().length;
                    System.arraycopy(mesh.getNormals(), 0, normals, np, mesh.getNormals().length);
                    np += mesh.getNormals().length;
                }
            }
            whole = new Mesh(vertices, uv, normals, null, GL11.GL_TRIANGLES);
        }
        return new BakedBlockMinecraft(cubes.toArray(new BakedBlockMinecraft.Cube[0]), full, transparency, whole, model.ambientocclusion);
    }

    private Transparency resolveTransparency(ByteBuffer buffer) {
        Transparency transparency = Transparency.OPAQUE;
        for (int i = 3; i < buffer.limit(); i += 4) {
            int alpha = buffer.get(i) & 0xFF;
            if (alpha > 24 && alpha < 255) {
                return Transparency.TRANSLUCENT;
            }
            if (alpha <= 24)
                transparency = TRANSPARENT;
        }
        return transparency;
    }

    /**
     * First step, load the model file to raw model
     *
     * @param path
     * @return
     * @throws IOException
     */
    Model loadModel(ResourceManager manager, String path) throws IOException {
        Resource load = manager.load(path);
        if (load == null) {
            Engine.getLogger().warn("Cannot find resource at " + path);
            return null;
        }
        Model model = new Gson().fromJson(new InputStreamReader(load.open()), Model.class);

        if (model.parent != null) {
            Model parent = loadModel(manager, "minecraft/models/" + model.parent + ".json");
            if (parent == null)
                throw new IllegalArgumentException("Missing parent");
            if (model.elements == null)
                model.elements = parent.elements;
            if (model.ambientocclusion == null)
                model.ambientocclusion = parent.ambientocclusion;
            if (model.display == null)
                model.display = parent.display;

            if (parent.textures != null)
                model.textures.putAll(parent.textures);
        }
        model.ambientocclusion = model.ambientocclusion == null ? false : model.ambientocclusion;
        return model;
    }

    @Data
    public static class BakedBlockMinecraft extends ResolvedBlockModel {
        private final boolean ambientOcclusion;

        public BakedBlockMinecraft(Cube[] cubes, boolean isFull, Transparency transparency, Mesh wholeBlock, boolean ambientOcclusion) {
            super(cubes, isFull, transparency, wholeBlock);
            this.ambientOcclusion = ambientOcclusion;
        }

        @Data
        public static class Cube extends ResolvedBlockModel.Cube {
            private final boolean shade;

            public Cube(Mesh[] faces, boolean shade) {
                super(faces);
                this.shade = shade;
            }
        }
    }

    static class Model {
        String parent;
        Boolean ambientocclusion;
        Display display;
        Map<String, String> textures;
        Element[] elements;

        @Override
        public String toString() {
            return "Model{" + "parent='" + parent + '\'' + ", ambientocclusion=" + ambientocclusion + ", display="
                    + display + ", textures=" + textures + ", elements=" + Arrays.toString(elements) + '}';
        }

        static class Transform {
            float[] rotation;
            float[] translation;
            float[] scale;
        }

        static class Element {
            int[] from;
            int[] to;
            boolean shade;
            Rotation rotation;
            Map<String, Face> faces;

            static class Face {
                float[] uv;
                String texture;
                boolean cullface;
                int rotation;
                int tintindex;
            }

            static class Rotation {
                int[] origin;
                String axis;
                int angle;
                boolean rescale;
            }
        }

        static class Display {
            Transform thirdperson_righthand;
            Transform thirdperson_lefthand;
            Transform firstperson_righthand;
            Transform firstperson_lefthand;
            Transform gui;
            Transform head;
            Transform ground;
            Transform fixed;
        }
    }
}
