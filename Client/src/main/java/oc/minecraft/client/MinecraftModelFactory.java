package oc.minecraft.client;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import de.matthiasmann.twl.utils.PNGDecoder;
import lombok.Data;
import oc.yamc.Engine;
import oc.yamc.client.gl.GLTexture2D;
import oc.yamc.client.model.BlockModelFactory;
import oc.yamc.client.model.Mesh;
import oc.yamc.client.model.ResolvedBlockModel;
import oc.yamc.client.model.ResolvedBlockModel.Transparency;
import oc.yamc.client.model.TextureAtlas;
import oc.yamc.registry.Registry;
import oc.yamc.resource.Resource;
import oc.yamc.resource.ResourceManager;
import oc.yamc.util.Facing;
import oc.yamc.world.Block;
import org.joml.AABBf;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.lwjgl.opengl.GL11;

import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.util.*;

import static oc.yamc.client.model.ResolvedBlockModel.Transparency.TRANSPARENT;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL30.glGenerateMipmap;

public final class MinecraftModelFactory implements BlockModelFactory {
    private int dimension = 256;
    private ResourceManager manager;

    Result process(String[] paths) throws IOException {
        TextureAtlas atlas;
        int size = paths.length;
        Model[] models = new Model[size];
        // Mesh[] meshes = new Mesh[size];
        ResolvedBlockModel[] resolvedBlockModels = new ResolvedBlockModel[size];
        Map<Model, Transparency> transparencyMap = Maps.newHashMap();

        for (int i = 0; i < size; i++) {
            String path = paths[i];
            Model model = loadModel(path);
            if (model == null) {
                Engine.getLogger().warn("The cannot load model from {}!", path);
            }
            models[i] = model;
        }
        GLTexture2D alloc = GLTexture2D.alloc();
        atlas = resolveUV(models, transparencyMap, alloc);
        for (int i = 0; i < size; i++) {
            Model model = models[i];
            ResolvedBlockModel resolvedBlockModel = bakeBlock(transparencyMap, model);
            if (resolvedBlockModel == null) {
                Engine.getLogger().warn("The cannot bake block model faces from {}!", paths[i]);
            }
            resolvedBlockModels[i] = resolvedBlockModel;
        }
        return new Result(resolvedBlockModels, atlas, alloc);
    }

    @Override
    public Result process(ResourceManager resourceManager, Registry<Block> registry) throws IOException {
        this.manager = resourceManager;

        Collection<Block> values = registry.getValues();
        String[] paths = new String[values.size() - 1];
        for (Block value : values) {
            if (value.getLocalName().equals("air"))
                continue;
            int id = registry.getId(value) - 1;
            String[] split = value.getUniqueName().split("\\.");
            ArrayList<String> ls = Lists.newArrayList(split);
            ls.add(1, "models");
            paths[id] = "/" + String.join("/", ls) + ".json";
        }
        return process(paths);
    }

    ResolvedBlockModel bakeBlock(Map<Model, Transparency> transparencyMap, Model model) {
        if (model == null)
            return null;
        List<BakedBlockMinecraft.Cube> cubes = new ArrayList<>(6);
        final int X = 0, Y = 1, Z = 2;

        AABBf box = null;
        int volume = 0;
        boolean hasFull = false;
        for (Model.Element element : model.elements) {
            AABBf aabb = new AABBf(element.from[0], element.from[1], element.from[2], element.to[0], element.to[1],
                    element.to[2]);
            if (aabb.minZ == 0 && aabb.minX == 0 && aabb.minY == 0 && aabb.maxZ == 16 && aabb.maxX == 16
                    && aabb.maxY == 16) {
                hasFull = true;
                break;
            }
            volume += (aabb.maxX - aabb.minX) * (aabb.maxY - aabb.minX) * (aabb.maxZ - aabb.minZ);
            if (box == null) {
                box = aabb;
            } else {
                box.union(aabb);
            }
        }

        boolean full = hasFull || (volume == 4096 && box.minZ == 0 && box.minX == 0 && box.minY == 0 && box.maxZ == 16
                && box.maxX == 16 && box.maxY == 16);

        for (Model.Element element : model.elements) {
            float[] thisUv;
            float fx = element.from[X] / 16F, fy = element.from[Y] / 16F, fz = element.from[Z] / 16F;
            float tx = element.to[X] / 16F, ty = element.to[Y] / 16F, tz = element.to[Z] / 16F;
            Mesh[] faces = new Mesh[6];
            cubes.add(new BakedBlockMinecraft.Cube(faces, element.shade));

            // north
            if (element.faces.containsKey("north")) {
                thisUv = element.faces.get("north").uv;

                faces[Facing.NORTH.index] = new Mesh(new float[]{ fx, fy, tz, tx, fy, tz, tx, ty, tz, fx, ty, tz },
                        new float[]{ thisUv[0], thisUv[1], thisUv[2], thisUv[1], thisUv[2], thisUv[3], thisUv[0], thisUv[3] },
                        new float[]{ 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1 }, new int[]{ 0, 1, 2, 0, 2, 3 }, -1);
            }

            // south
            if (element.faces.containsKey("south")) {
                thisUv = element.faces.get("south").uv;

                faces[Facing.SOUTH.index] = new Mesh(new float[]{ tx, fy, fz, fx, fy, fz, fx, ty, fz, tx, ty, fz },
                        new float[]{ thisUv[0], thisUv[1], thisUv[2], thisUv[1], thisUv[2], thisUv[3], thisUv[0], thisUv[3] },
                        new float[]{ 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1 }, new int[]{ 0, 1, 2, 0, 2, 3 }, -1);
            }
            if (element.faces.containsKey("east")) {
                // right
                thisUv = element.faces.get("east").uv;

                faces[Facing.EAST.index] = new Mesh(new float[]{ tx, fy, tz, tx, fy, fz, tx, ty, fz, tx, ty, tz },
                        new float[]{ thisUv[0], thisUv[1], thisUv[2], thisUv[1], thisUv[2], thisUv[3], thisUv[0], thisUv[3] },
                        new float[]{ 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, }, new int[]{ 0, 1, 2, 0, 2, 3 }, -1);
            }

            // left
            if (element.faces.containsKey("west")) {
                thisUv = element.faces.get("west").uv;

                faces[Facing.WEST.index] = new Mesh(new float[]{ fx, fy, fz, fx, fy, tz, fx, ty, tz, fx, ty, fz },
                        new float[]{ thisUv[0], thisUv[1], thisUv[2], thisUv[1], thisUv[2], thisUv[3], thisUv[0], thisUv[3] },
                        new float[]{ 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1 }, new int[]{ 0, 1, 2, 0, 2, 3 }, -1);
            }

            // bottom
            if (element.faces.containsKey("down")) {
                thisUv = element.faces.get("down").uv;

                faces[Facing.DOWN.index] = new Mesh(new float[]{ fx, fy, fz, tx, fy, fz, tx, fy, tz, fx, fy, tz },
                        new float[]{ thisUv[0], thisUv[1], thisUv[2], thisUv[1], thisUv[2], thisUv[3], thisUv[0], thisUv[3] },
                        new float[]{ 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, }, new int[]{ 0, 1, 2, 0, 2, 3 }, -1);
            }

            // top
            if (element.faces.containsKey("up")) {
                thisUv = element.faces.get("up").uv;

                faces[Facing.UP.index] = new Mesh(new float[]{ tx, ty, tz, tx, ty, fz, fx, ty, fz, fx, ty, tz },
                        new float[]{ thisUv[0], thisUv[1], thisUv[2], thisUv[1], thisUv[2], thisUv[3], thisUv[0],
                                thisUv[3] },
                        new float[]{ 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, }, new int[]{ 0, 1, 2, 0, 2, 3 }, -1);
            }

            if (element.rotation != null) {
                var trans = new Matrix4f().translate(-element.rotation.origin[0], -element.rotation.origin[1],
                        -element.rotation.origin[2]);
                var rot = new Matrix4f();
                var repo = new Matrix4f().translate(element.rotation.origin[0], element.rotation.origin[1],
                        element.rotation.origin[2]);
                switch (element.rotation.axis) {
                    case "posY":
                        rot.rotate((float) Math.toRadians(element.rotation.angle), 0, 1, 0);
                        break;
                    case "posX":
                        rot.rotate((float) Math.toRadians(element.rotation.angle), 1, 0, 0);
                        break;
                    case "posZ":
                        rot.rotate((float) Math.toRadians(element.rotation.angle), 0, 0, 1);
                        break;
                    default:
                        break;
                }

                for (var i = 0; i < 8; ++i) {
                    Mesh f = faces[i];
                    if (f == null)
                        continue;
                    float[] vs = f.getVertices();
                    float[] ns = f.getNormals();
                    for (var j = 0; j < vs.length; j += 3) {
                        var vec = new Vector3f(vs[j + 0], vs[j + 1], vs[j + 2]);
                        trans.transformPosition(vec);
                        rot.transformPosition(vec);
                        repo.transformPosition(vec);
                        vs[j + 0] = vec.x;
                        vs[j + 1] = vec.y;
                        vs[j + 2] = vec.z;
                        vec.set(ns[j + 0], ns[j + 1], ns[j + 2]);
                        rot.transformPosition(vec);
                        ns[j + 0] = vec.x;
                        ns[j + 1] = vec.y;
                        ns[j + 2] = vec.z;
                    }
                }
            }
        }
        Mesh whole;
        {
            float[] vertices = new float[model.elements.length * 24 * 3];
            float[] uv = new float[model.elements.length * 24 * 2];
            float[] normals = new float[model.elements.length * 24 * 3];
            int vp = 0, uvp = 0, np = 0;

            for (var c : cubes) {
                for (Mesh mesh : c.getFaces()) {
                    if (mesh == null) continue;
                    System.arraycopy(mesh.getVertices(), 0, vertices, vp, mesh.getVertices().length);
                    vp += mesh.getVertices().length;
                    System.arraycopy(mesh.getUv(), 0, uv, uvp, mesh.getUv().length);
                    uvp += mesh.getUv().length;
                    System.arraycopy(mesh.getNormals(), 0, normals, np, mesh.getNormals().length);
                    np += mesh.getNormals().length;
                }
            }
            whole = new Mesh(vertices, uv, normals, null, GL11.GL_TRIANGLES);
        }
        return new BakedBlockMinecraft(cubes.toArray(new BakedBlockMinecraft.Cube[0]), full, transparencyMap.get(model), whole, model.ambientocclusion);
    }

    private Transparency resolveTransparency(ByteBuffer buffer) {
        Transparency transparency = Transparency.OPAQUE;
        for (int i = 3; i < buffer.limit(); i += 4) {
            int alpha = buffer.get(i) & 0xFF;
            if (alpha > 24 && alpha < 255) {
                return Transparency.TRANSLUCENT;
            }
            if (alpha <= 24)
                transparency = TRANSPARENT;
        }
        return transparency;
    }

    /**
     * second step, resolve the texture dependency, build texture and map uv
     *
     * @param models          The models
     * @param transparencyMap
     * @return The texture map
     * @throws Exception
     */
    TextureAtlas resolveUV(Model[] models, Map<Model, Transparency> transparencyMap, GLTexture2D glTexture) throws IOException {
        Map<String, TexturePart> required = new HashMap<>();
        List<TexturePart> parts = Lists.newArrayList();
        for (Model model : models) {
            if (model == null)
                continue;
            for (String variant : model.textures.keySet()) {
                String path = model.textures.get(variant);
                while (path.startsWith("#")) {
                    String next = model.textures.get(path.substring(1));
                    if (next == null) {
                        Engine.getLogger().warn("Missing texture for " + variant);
                        path = null;
                        break;
                    }
                    path = next;
                }

                if (path == null) {
                    Engine.getLogger().warn("Missing texture for " + model);
                    continue;
                }
                model.textures.put(variant, path);
                if (!required.containsKey(path)) {
                    Resource resource = manager.load("minecraft/textures/" + path + ".png");
                    PNGDecoder decoder = new PNGDecoder(resource.open());
                    ByteBuffer buf = ByteBuffer.allocateDirect(4 * decoder.getWidth() * decoder.getHeight());
                    decoder.decode(buf, decoder.getWidth() * 4, PNGDecoder.Format.RGBA);
                    buf.flip();
                    TexturePart part = new TexturePart(decoder.getWidth(), decoder.getHeight(), buf, resolveTransparency(buf));
                    required.put(path, part);
                    parts.add(part);
                }
            }
        }

        int dimension = stitch(parts);
        glBindTexture(GL_TEXTURE_2D, glTexture.id);
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

        nglTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, dimension, dimension, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
        for (TexturePart part : parts) {
            glTexSubImage2D(GL_TEXTURE_2D, 0, part.offsetX, dimension - part.offsetY - part.height, part.width,
                    part.height, GL_RGBA, GL_UNSIGNED_BYTE, part.buffer);
        }
        glGenerateMipmap(GL_TEXTURE_2D);

        for (Model m : models) {
            if (m == null)
                continue;
            Transparency transparency = Transparency.OPAQUE;
            for (Model.Element e : m.elements) {
                for (var face : e.faces.values()) {
                    if (face == null)
                        continue;
                    String path = m.textures.get(face.texture.substring(1));

                    TexturePart p = required.get(path);

                    if (p.transparency == Transparency.TRANSLUCENT) {
                        transparency = Transparency.TRANSLUCENT;
                    } else if (p.transparency == Transparency.TRANSPARENT && transparency == Transparency.OPAQUE) {
                        transparency = Transparency.TRANSPARENT;
                    }

                    if (face.uv == null)
                        face.uv = new float[]{ 0, 0, 16, 16 };
                    face.uv[0] = (face.uv[0] + p.offsetX) / dimension;
                    face.uv[1] = 1 - (face.uv[1] + p.offsetY) / dimension;
                    face.uv[2] = (face.uv[2] + p.offsetX) / dimension;
                    face.uv[3] = 1 - (face.uv[3] + p.offsetY) / dimension;
                }
            }
            transparencyMap.put(m, transparency);
        }
        return null;
    }

    /**
     * First step, load the model file to raw model
     *
     * @param path
     * @return
     * @throws IOException
     */
    Model loadModel(String path) throws IOException {
        Resource load = manager.load(path);
        if (load == null) {
            Engine.getLogger().warn("Cannot find resource at " + path);
            return null;
        }
        Model model = new Gson().fromJson(new InputStreamReader(load.open()), Model.class);

        if (model.parent != null) {
            Model parent = loadModel("minecraft/models/" + model.parent + ".json");
            if (parent == null)
                throw new IllegalArgumentException("Missing parent");
            if (model.elements == null)
                model.elements = parent.elements;
            if (model.ambientocclusion == null)
                model.ambientocclusion = parent.ambientocclusion;
            if (model.display == null)
                model.display = parent.display;

            if (parent.textures != null)
                model.textures.putAll(parent.textures);
        }
        model.ambientocclusion = model.ambientocclusion == null ? false : model.ambientocclusion;
        return model;
    }

    @Data
    public static class BakedBlockMinecraft extends ResolvedBlockModel {
        private final boolean ambientOcclusion;

        public BakedBlockMinecraft(Cube[] cubes, boolean isFull, Transparency transparency, Mesh wholeBlock, boolean ambientOcclusion) {
            super(cubes, isFull, transparency, wholeBlock);
            this.ambientOcclusion = ambientOcclusion;
        }

        @Data
        public static class Cube extends ResolvedBlockModel.Cube {
            private final boolean shade;

            public Cube(Mesh[] faces, boolean shade) {
                super(faces);
                this.shade = shade;
            }
        }
    }

    /**
     * stitch the textures
     *
     * @param parts
     * @return
     */
    int stitch(List<TexturePart> parts) {
        parts.sort(Comparator.<TexturePart>comparingInt(a -> a.height).reversed());
        PriorityQueue<FreeSpace> queue = new PriorityQueue<>(Comparator.comparingInt(a -> a.height * a.width));
        List<FreeSpace> unaccepted = new ArrayList<>();
        int dimension = this.dimension;
        queue.add(new FreeSpace(0, 0, dimension, dimension));

        for (TexturePart part : parts) {
            boolean accepted = false;
            while (!accepted) {
                while (!queue.isEmpty()) {
                    FreeSpace free = queue.poll();
                    if (free == null) {
                        break;
                    }
                    if (free.accept(queue, part)) {
                        accepted = true;
                        break;
                    }
                    unaccepted.add(free);
                }
                unaccepted.addAll(queue);
                queue.clear();

                for (FreeSpace a : unaccepted) {
                    if (!a.valid)
                        continue;
                    for (int i = 0; i < unaccepted.size(); i++) {
                        FreeSpace b = unaccepted.get(i);
                        if (a == b || !b.valid || !a.merge(b))
                            continue;
                        i = 0;
                    }
                }
                for (FreeSpace space : unaccepted)
                    if (space.valid)
                        queue.add(space);

                unaccepted.clear();
                if (!accepted) {
                    queue.add(new FreeSpace(dimension, 0, dimension, dimension));
                    queue.add(new FreeSpace(0, dimension, dimension + dimension, dimension));
                    dimension = dimension * 2;
                }
            }
        }
        return dimension;
    }

    static class TexturePart {
        int width;
        int height;
        int offsetX;
        int offsetY;
        Transparency transparency;
        ByteBuffer buffer;

        TexturePart(int width, int height, ByteBuffer buffer, Transparency transparency) {
            this.width = width;
            this.height = height;
            this.buffer = buffer;
            this.transparency = transparency;
        }

        @Override
        public String toString() {
            return "TexturePart{" + "width=" + width + ", height=" + height + ", offsetX=" + offsetX + ", offsetY="
                    + offsetY + '}';
        }
    }

    static class Model {
        String parent;
        Boolean ambientocclusion;
        Display display;
        Map<String, String> textures;
        Element[] elements;

        @Override
        public String toString() {
            return "Model{" + "parent='" + parent + '\'' + ", ambientocclusion=" + ambientocclusion + ", display="
                    + display + ", textures=" + textures + ", elements=" + Arrays.toString(elements) + '}';
        }

        static class Transform {
            float[] rotation;
            float[] translation;
            float[] scale;
        }

        static class Element {
            int[] from;
            int[] to;
            boolean shade;
            Rotation rotation;
            Map<String, Element.Face> faces;

            static class Face {
                float[] uv;
                String texture;
                boolean cullface;
                int rotation;
                int tintindex;
            }

            static class Rotation {
                int[] origin;
                String axis;
                int angle;
                boolean rescale;
            }
        }

        static class Display {
            Transform thirdperson_righthand;
            Transform thirdperson_lefthand;
            Transform firstperson_righthand;
            Transform firstperson_lefthand;
            Transform gui;
            Transform head;
            Transform ground;
            Transform fixed;
        }
    }

    class FreeSpace {
        int x, y, width, height;
        boolean valid = true;

        FreeSpace(int x, int y, int width, int height) {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }

        boolean merge(FreeSpace other) {
            if (x + width == other.x && other.height == this.height) {
                this.width += other.width;
                other.valid = false;
                return true;
            }
            if (x + height == other.y && other.width == this.width) {
                this.height += other.height;
                other.valid = false;
                return true;
            }
            return false;
        }

        boolean accept(PriorityQueue<FreeSpace> others, TexturePart part) {
            if (part.width <= width && part.height <= height) {
                int remainedWidth = width - part.width;
                int remainedHeight = height - part.height;
                part.offsetX = x;
                part.offsetY = y;
                if (remainedHeight != 0 && remainedWidth != 0) {
                    others.add(new FreeSpace(x + part.width, y, remainedWidth, part.height));
                    others.add(new FreeSpace(x, y + part.height, width + remainedWidth, remainedHeight));
                } else if (remainedWidth != 0)
                    others.add(new FreeSpace(x + part.width, y, remainedWidth, part.height));
                else
                    others.add(new FreeSpace(x, y + part.height, part.width, remainedHeight));
                return true;
            }
            return false;
        }

        @Override
        public String toString() {
            return "FreeSpace{" + "posX=" + x + ", posY=" + y + ", width=" + width + ", height=" + height + '}';
        }
    }
}
