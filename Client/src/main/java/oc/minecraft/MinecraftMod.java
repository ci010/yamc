package oc.minecraft;

import javafx.beans.binding.Bindings;
import oc.minecraft.client.MinecraftModelWorker;
import oc.yamc.GameContext;
import oc.yamc.client.Main;
import oc.yamc.client.game.GameReadyEvent;
import oc.yamc.client.game.ResourceSetupEvent;
import oc.yamc.client.scene.Canvas2D;
import oc.yamc.client.scene.Scene;
import oc.yamc.client.ui.UIText;
import oc.yamc.client.ui.UIVerticalLayout;
import oc.yamc.entity.Entity;
import oc.yamc.event.Listener;
import oc.yamc.game.RegisterEvent;
import oc.yamc.item.Item;
import oc.yamc.item.ItemBuilder;
import oc.yamc.item.ItemType;
import oc.yamc.math.Position;
import oc.yamc.mod.ModIdentifier;
import oc.yamc.registry.Registry;
import oc.yamc.util.RayCast;
import oc.yamc.world.Block;
import oc.yamc.world.BlockBuilder;
import oc.yamc.world.World;

public class MinecraftMod {
    public static ModIdentifier identifier = ModIdentifier.of("mojang", "minecraft", "0.0.1");


    @Listener
    public void registerEvent(RegisterEvent event) {
        Block stone = BlockBuilder.create("stone").build(),
                grass = BlockBuilder.create("grass_block").build(),
                // brick = BlockBuilder.alloc("brick").build(),
                bookshelf = BlockBuilder.create("bookshelf").build(),
                glass = BlockBuilder.create("glass").build();

        event.getRegistry().register(BlockBuilder.create("air").setNoCollision().build());
        event.getRegistry().register(stone);
        event.getRegistry().register(grass);
        // event.getRegistry().register(brick);
        event.getRegistry().register(bookshelf);
        event.getRegistry().register(glass);

        event.getRegistry().register(createPlace(stone, "stone"));
        event.getRegistry().register(createPlace(grass, "grass"));
        event.getRegistry().register(createPlace(glass, "glass"));
        // event.getRegistry().register(createPlace(stone, "bricks"));
        event.getRegistry().register(createPlace(grass, "bookshelf"));
    }

    @Listener
    public void setupResources(ResourceSetupEvent event) {
        event.addBlockModelWorker(new MinecraftModelWorker());
//        try {
//            byte[] cache = event.getResourceManager().load("yamc/fonts/arial.ttf").cache();
//            context = new STBTTFontContext(ByteBuffer.allocateDirect(cache.length).put(cache).flip());
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

    @Listener
    public void onGameReady(GameReadyEvent event) {
        System.out.println("READY");

        GameContext context = event.getContext();
        Registry<Item> itemRegistry = context.getItemRegistry();
        Registry<Block> blockRegistry = context.getBlockRegistry();

        Item stone = itemRegistry.getValue("minecraft.item.glass_placer");

        Entity entity = Main.getGame().getPlayer().getMountingEntity();
        entity.getPosition().set(1, 16, -3.5F);
        Main.getGame().getScene().getCamera().getLookAt().set(0, 0, 1);
        entity.getBehavior(Entity.TwoHands.class).setMainHand(stone);
        Main.getGame().getWorld().setBlock(new Position(1, 0, 0), blockRegistry.getValue(1));

        Scene scene = event.getScene();
        Canvas2D canvas2D = scene.getCanvas2D();

//        AABBd box = AABBs.translate(player.getBoundingBox(), player.getPosition(), new AABBd());
//        World world = Main.getGame().getPlayer().getWorld();
//        BlockPrototype.Hit hit = world.first(player.getPosition(), player.getRotation(), 5);
        canvas2D.getUiRoot().addChild(
                new UIVerticalLayout().addChild(
                        new UIText(Bindings.createStringBinding(() -> {
                            Entity player = Main.getGame().getPlayer().getMountingEntity();
                            return String.format("position: %f, %f, %f", player.getPosition().x,
                                    player.getPosition().y, player.getPosition().z);
                        }, canvas2D.getTick()))).addChild(
                        new UIText(Bindings.createStringBinding(() -> {
                            Entity player = Main.getGame().getPlayer().getMountingEntity();
                            return String.format("lookAt: %f, %f, %f", player.getRotation().x,
                                    player.getRotation().y, player.getRotation().z);
                        }, canvas2D.getTick())))
        );

    }


    private Item createPlace(Block object, String name) {
        class PlaceBlock implements ItemType.UseBlockBehavior {
            private Block object;

            private PlaceBlock(Block object) {
                this.object = object;
            }

            @Override
            public void onUseBlockStart(World world, Entity entity, Item item, RayCast.Hit hit) {
                Position side = hit.face.side(hit.position);
                world.setBlock(side, object);
            }
        }
        return ItemBuilder.create(name + "_placer").setUseBlockBehavior(new PlaceBlock(object))
                .build();
    }
}
