package oc.yamc.client.model.pipeline;

import de.matthiasmann.twl.utils.PNGDecoder;
import oc.yamc.resource.ResourceManagerSingleThread;
import oc.yamc.resource.ResourceSourceBuiltin;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.ByteBuffer;

class MinecraftPipelineTest {

    @Test
    void testImage() throws IOException {
        PNGDecoder pngDecoder = new PNGDecoder(MinecraftPipelineTest.class.getResourceAsStream("/assets/minecraft/textures/block/glass.png"));
        System.out.println(pngDecoder.getWidth());
        System.out.println(pngDecoder.getHeight());
        ByteBuffer allocate = ByteBuffer.allocate(4 * pngDecoder.getWidth() * pngDecoder.getHeight());
        pngDecoder.decode(allocate, pngDecoder.getWidth() * 4, PNGDecoder.Format.RGBA);
        byte[] array = allocate.array();
        class Img {
            byte[] arr;
            int width;

            Img(byte[] arr, int width) {
                this.arr = arr;
                this.width = width;
            }

            int getRed(int x, int y) {
                return arr[width * 4 * y + x * 4] & 0xFF;
            }

            int getGreen(int x, int y) {
                return arr[width * 4 * y + x * 4 + 1] & 0xFF;
            }

            int getBlue(int x, int y) {
                return arr[width * 4 * y + x * 4 + 2] & 0xFF;
            }

            int getAlpha(int x, int y) {
                return arr[width * 4 * y + x * 4 + 3] & 0xFF;
            }
        }
        Img w = new Img(array, pngDecoder.getWidth());
        System.out.println(w.getRed(0, 15));
        System.out.println(w.getGreen(15, 15));
        System.out.println(w.getBlue(15, 15));

        int v = 254 & 0xFF;
        byte b = (byte) v;
        System.out.println(b == w.arr[w.width * 4 * 15 + 15 * 4 + 2]);
        System.out.println(w.arr[w.width * 4 * 15 + 15 * 4 + 2]);
        System.out.println(b);
//        System.out.println(Arrays.toString(array));
    }

    @Test
    void test() {
        Vector3f v = new Vector3f(0.8f, 0f, 8f), v2 = new Vector3f(15.2F, 16, 8);
        var trans = new Matrix4f().translate(-8, -8, -8);
        var rot = new Matrix4f().rotate((float) Math.toRadians(45), 0, 1, 0);
        var repos = new Matrix4f().translate(8, 8, 8);
        trans.transformPosition(v);
        rot.transformPosition(v);
        repos.transformPosition(v);
        trans.transformPosition(v2);
        rot.transformPosition(v2);
        repos.transformPosition(v2);

        Matrix4f a = new Matrix4f().setOrtho(-1, 1, -1, 1, -1, 1)
                .mul(new Matrix4f().setLookAt(new Vector3f(0, 0, 0), new Vector3f(1, 1, 1), new Vector3f(0, 1, 0)));
        System.out.println(a);
        Matrix4f b = new Matrix4f().setOrtho(0, 2, -1, 1, -1, 1)
                .mul(new Matrix4f().setLookAt(new Vector3f(1, 0, 0), new Vector3f(1, 1, 1), new Vector3f(0, 1, 0)));

        System.out.println(b);
        System.out.println(a.equals(b));

        // Matrix4f v = a; //new Matrix4f().setLookAt(new Vector3f(0, 0, 0), new
        // Vector3f(1, 1, 1), new Vector3f(0, 1, 0));
        // // v = new Matrix4f().setTranslation(10, 4, -2);
        // Matrix4f vi = v.invert(new Matrix4f());

        // Vector3f pos = new Vector3f(0, 1, 10);
        // System.out.println(pos);
        // System.out.println(vi.transformPosition(v.transformPosition(pos)));
    }

    @Test
    void create() {
        ResourceManagerSingleThread resourceManager = new ResourceManagerSingleThread();
        resourceManager.addResourceSource(new ResourceSourceBuiltin());
        // Pipeline pipeline = MinecraftPipeline.alloc(resourceManager);
        // pipeline.push("ModelPaths", Lists.newArrayList(new ResourcePath("",
        // "/minecraft/models/block/stone.json")));
    }
}