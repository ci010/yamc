package oc.yamc.world;

import com.google.common.collect.ImmutableMap;

import oc.yamc.math.Position;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public interface BlockCursor extends BlockAccess {
    BlockCursor move(int x, int y, int z);

    BlockCursor at(Position pos);

    <T extends Comparable<T>> T getProperty(BlockPrototype.Property<T> property);

    ImmutableMap<BlockPrototype.Property<?>, Comparable<?>> getProperties();

//    <T extends Comparable<T>, V extends T> BlockCursor setProperty(BlockPrototype.Property<T> property, V value);
//
//    <T extends Comparable<T>> BlockCursor setToNextProperty(BlockPrototype.Property<T> property);
}