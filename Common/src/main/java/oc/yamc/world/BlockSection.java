package oc.yamc.world;

import org.joml.AABBf;

import java.util.Comparator;
import java.util.function.Consumer;
import java.util.function.Predicate;

public interface BlockSection extends Iterable<BlockAccess> {
    World getWorld();

    BlockSection filter(Predicate<BlockAccess> predicate);

    default BlockSection where(Predicate<BlockAccess> predicate) {
        return filter(predicate);
    }

    BlockSection sorted(Comparator<BlockAccess> comparator);

    BlockSection each(Consumer<BlockAccess> action);

    void commit(boolean async);

    AABBf getBoundingBox();
}
