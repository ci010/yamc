package oc.yamc.world;

import com.google.common.collect.ImmutableList;
import oc.yamc.entity.Entity;
import oc.yamc.math.Position;

import java.util.List;
import java.util.Optional;

public abstract class BlockPrototype {
    // all these behaviors are missing arguments
    // fill those arguments later

    public static PlaceBehavior DEFAULT_PLACE = (world, entity, block) -> {
    };
    public static ActiveBehavior DEFAULT_ACTIVE = (world, entity, pos, block) -> {
    };

    public abstract List<Block> getAllStates();

    public interface TickBehavior {
        void tick(Block object);
    }

    public interface PlaceBehavior {
        default boolean canPlace(World world, Entity entity, Block block) {
            return true;
        }

        void onPlaced(World world, Entity entity, Block block);
    }

    public interface ActiveBehavior { // right click entity
        default boolean shouldActivated(World world, Entity entity, Position position, Block block) {
            return true;
        }

        void onActivated(World world, Entity entity, Position pos, Block block);
    }

    public interface TouchBehavior { // left click entity
        boolean onTouch(Block block);

        void onTouched(Block block);
    }

    public interface DestroyBehavior {

    }

    public interface Property<T extends Comparable<T>> {
        String getName();

        ImmutableList<T> getValues();

        /**
         * The class of the values of this property
         */
        Class<T> getValueClass();

        Optional<T> parseValue(String value);

        /**
         * Get the name for the given value.
         */
        String getName(T value);
    }

}
