package oc.yamc.world;

import oc.yamc.RuntimeObject;
import oc.yamc.entity.Entity;
import oc.yamc.game.Game;
import oc.yamc.math.Position;
import oc.yamc.util.Owner;
import oc.yamc.util.RayCast;
import org.joml.Vector3f;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Set;

/**
 * World instance, should spawn by {@link oc.yamc.game.Game}
 */
@Owner(Game.class)
public interface World extends RuntimeObject {
    List<Entity> getEntities();

    RayCast.Hit raycast(Vector3f from, Vector3f dir, float distance);

    RayCast.Hit raycast(Vector3f from, Vector3f dir, float distance, Set<Block> ignore);

    @Nonnull
    Chunk getChunk(@Nonnull Position position);

    @Nonnull
    Block getBlock(@Nonnull Position pos);

    @Nonnull
    Block setBlock(@Nonnull Position pos, Block block);

    interface Config {

    }
}
