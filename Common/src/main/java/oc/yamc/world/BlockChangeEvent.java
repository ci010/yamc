package oc.yamc.world;

import oc.yamc.event.Event;
import oc.yamc.math.Position;

public class BlockChangeEvent implements Event {
    public final Position pos;
    public final int blockId;

    public BlockChangeEvent(Position pos, int blockId) {
        this.pos = pos;
        this.blockId = blockId;
    }
}
