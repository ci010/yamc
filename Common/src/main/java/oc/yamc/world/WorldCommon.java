package oc.yamc.world;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import oc.yamc.GameContext;
import oc.yamc.entity.Entity;
import oc.yamc.entity.EntityBase;
import oc.yamc.event.Event;
import oc.yamc.math.AABBs;
import oc.yamc.math.Position;
import oc.yamc.player.Player;
import oc.yamc.player.PlayerImpl;
import oc.yamc.util.RayCast;
import org.joml.AABBd;
import org.joml.Vector3f;
import org.joml.Vector3i;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public class WorldCommon implements World {
    private GameContext context;

    private PhysicsSystem physicsSystem = new PhysicsSystem(); // prepare for split

    private Chunk.Store chunkStore;
    private List<Player> players = new ArrayList<>();
    private List<Entity> entityList = new ArrayList<>();

    private com.artemis.World batch;

    public WorldCommon(GameContext context, Chunk.Store chunkStore, com.artemis.World batch) {
        this.context = context;
        this.chunkStore = chunkStore;
        this.batch = batch;
    }

    public void spawnEntity(Entity entity) {
        Position pos = Position.of(entity.getPosition());
        Chunk chunk = chunkStore.getChunk(pos);
        chunk.getEntities().add(entity);
        entityList.add(entity);
    }

    public Player playerJoin(Player.Profile data) {
//        com.artemis.Entity e = batch.createEntity(new ArchetypeBuilder()
//                .add(ComponentTransform.class)
//                .add(ComponentVelocity.class)
//                .build(this.batch));
//
//        ComponentTransform transform = e.getComponent(ComponentTransform.class);
//        transform.posX = 0;
//        transform.posY = 2;
//        transform.posZ = 0;
//
//        transform.rotX = 0;
//        transform.rotY = 0;
//        transform.rotZ = 1;

        EntityBase entity = new EntityBase(entityList.size(),
                new Vector3f(0, 2, 0),
                new Vector3f(0, 0, 1),
                new Vector3f(),
                data.getBoundingBox(), ImmutableMap.<String, Object>builder()
                .put(Entity.TwoHands.class.getName(), new EntityBase.TwoHandImpl()).build());
        spawnEntity(entity);
        PlayerImpl player = new PlayerImpl(data, this, entity);
        player.enter(this.chunkStore);
        players.add(player);
        return player;
    }

    @Override
    public List<Entity> getEntities() {
        return entityList;
    }

    @Override
    public RayCast.Hit raycast(Vector3f from, Vector3f dir, float distance) {
        return raycast(from, dir, distance, Sets.newHashSet(context.getBlockRegistry().getValue(0)));
    }

    @Override
    public RayCast.Hit raycast(Vector3f from, Vector3f dir, float distance, Set<Block> ignore) {
        return RayCast.first(this, from, dir, distance, ignore::contains);
    }

    @Nonnull
    @Override
    public Chunk getChunk(@Nonnull Position position) {
        return chunkStore.getChunk(position);
    }

    public void tick() {
//        physicsSystem.tick(this);

        for (Entity entity : this.getEntities()) {
            Vector3f position = entity.getPosition();
            Vector3f motion = entity.getMotion();
            Position oldPosition = Position.of(position);
            position.add(motion);
            Position newPosition = Position.of(position);

            if (!Position.inSameChunk(oldPosition, newPosition)) {
                Chunk oldChunk = chunkStore.getChunk(oldPosition), newChunk = chunkStore.getChunk(newPosition);
                oldChunk.getEntities().remove(entity);
                newChunk.getEntities().add(entity);
                Player mountedPlayer = entity.getMountedPlayer();
                if (mountedPlayer != null) {
                    ((PlayerImpl) mountedPlayer).update(newChunk);
                }
            }
        }

        chunkStore.getChunks().forEach(this::tickChunk);
        for (Entity entity : entityList)
            entity.tick(); // state machine update
    }

    private void tickChunk(Chunk chunk) {
        Collection<Block> blocks = chunk.getRuntimeBlock();
        if (blocks.size() != 0) {
            // for (Block object : blocks) {
            //     BlockPrototype.TickBehavior behavior = object.getBehavior(BlockPrototype.TickBehavior.class);
            //     if (behavior != null) {
            //         behavior.tick(object);
            //     }
            // }
        }
    }

    @Nonnull
    public Block getBlock(@Nonnull Position pos) {
        return chunkStore.getChunk(pos).getBlock(pos);
    }

    @Nonnull
    @Override
    public Block setBlock(@Nonnull Position pos, Block block) {
        return chunkStore.getChunk(pos).setBlock(pos, block);
    }

    static class PhysicsSystem {
        public void tick(World world) {
            List<Entity> entityList = world.getEntities();
            for (Entity entity : entityList) {
                Vector3f motion = entity.getMotion();
                Vector3f direction = new Vector3f(motion);
                if (motion.x == 0 && motion.y == 0 && motion.z == 0)
                    continue;
                Vector3f position = entity.getPosition();
                AABBd box = entity.getBoundingBox();

                Position localPos = new Position(((int) Math.floor(position.x)), ((int) Math.floor(position.y)),
                        ((int) Math.floor(position.z)));

                // int directionX = motion.posX == -0 ? 0 : Float.compare(motion.posX, 0),
                // directionY = motion.posY == -0 ? 0 : Float.compare(motion.posY, 0),
                // directionZ = motion.posZ == -0 ? 0 : Float.compare(motion.posZ, 0);

                AABBd entityBox = AABBs.translate(box, position.add(direction, new Vector3f()), new AABBd());
                List<Position>[] around = AABBs.around(entityBox, motion);
                for (List<Position> ls : around) {
                    ls.add(localPos);
                }
                List<Position> faceX = around[0], faceY = around[1], faceZ = around[2];

                double xFix = Integer.MAX_VALUE, yFix = Integer.MAX_VALUE, zFix = Integer.MAX_VALUE;
                if (faceX.size() != 0) {
                    for (Position pos : faceX) {
                        Block block = world.getBlock(pos);
                        AABBd[] blockBoxes = block.getBoundingBoxes();
                        if (blockBoxes.length != 0)
                            for (AABBd blockBoxLocal : blockBoxes) {
                                AABBd blockBox = AABBs.translate(blockBoxLocal,
                                        new Vector3f(pos.getX(), pos.getY(), pos.getZ()), new AABBd());
                                if (blockBox.testAABB(entityBox)) {
                                    xFix = Math.min(xFix, Math.min(Math.abs(blockBox.maxX - entityBox.minX),
                                            Math.abs(blockBox.minX - entityBox.maxX)));
                                }
                            }
                    }
                }
                if (faceY.size() != 0) {
                    for (Position pos : faceY) {
                        Block block = world.getBlock(pos);
                        AABBd[] blockBoxes = block.getBoundingBoxes();
                        if (blockBoxes.length != 0)
                            for (AABBd blockBox : blockBoxes) {
                                AABBd translated = AABBs.translate(blockBox,
                                        new Vector3f(pos.getX(), pos.getY(), pos.getZ()), new AABBd());
                                if (translated.testAABB(entityBox)) {
                                    yFix = Math.min(yFix, Math.min(Math.abs(translated.maxY - entityBox.minY),
                                            Math.abs(translated.minY - entityBox.maxY)));
                                }
                            }
                    }
                }
                if (faceZ.size() != 0) {
                    for (Position pos : faceZ) {
                        Block block = world.getBlock(pos);
                        AABBd[] blockBoxes = block.getBoundingBoxes();
                        if (blockBoxes.length != 0)
                            for (AABBd blockBox : blockBoxes) {
                                AABBd translated = AABBs.translate(blockBox,
                                        new Vector3f(pos.getX(), pos.getY(), pos.getZ()), new AABBd());
                                if (translated.testAABB(entityBox)) {
                                    zFix = Math.min(zFix, Math.min(Math.abs(translated.maxZ - entityBox.minZ),
                                            Math.abs(translated.minZ - entityBox.maxZ)));
                                }
                            }
                    }
                }
                if (Integer.MAX_VALUE != xFix)
                    motion.x = 0;
                if (Integer.MAX_VALUE != yFix)
                    motion.y = 0;
                if (Integer.MAX_VALUE != zFix) {
                    motion.z = 0;
                }

                // if (motion.posY > 0) motion.posY -= 0.01f;
                // else if (motion.posY < 0) motion.posY += 0.01f;
                // if (Math.abs(motion.posY) <= 0.01f) motion.posY = 0; // physics update
            }
        }
    }

    @Nullable
    @Override
    public <T> T getComponent(@Nonnull String name) {
        return null;
    }

    @Nullable
    @Override
    public <T> T getComponent(@Nonnull Class<T> type) {
        return null;
    }

    @Nullable
    @Override
    public <T> T getBehavior(Class<T> type) {
        return null;
    }

    public static class ChunkUnload implements Event {
        public final Vector3i pos;

        public ChunkUnload(Vector3i pos) {
            this.pos = pos;
        }
    }
}
