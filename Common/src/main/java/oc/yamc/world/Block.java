package oc.yamc.world;

import com.google.common.collect.ImmutableMap;
import org.joml.AABBd;
import oc.yamc.game.Game;
import oc.yamc.registry.RegistryEntry;
import oc.yamc.util.Owner;

import javax.annotation.Nonnull;


@Owner({World.class, Game.class})
public interface Block extends RegistryEntry<Block>, BlockPrototype.PlaceBehavior, BlockPrototype.ActiveBehavior, BlockPrototype.DestroyBehavior, BlockPrototype.TouchBehavior {
    // think about blockstate and tileentity...

    AABBd MAX_BOUNDING_BOX = new AABBd(0, 0, 0, 1, 1, 1);

    ImmutableMap<BlockPrototype.Property<?>, Comparable<?>> getProperties();

    <T extends Comparable<T>> T getProperty(BlockPrototype.Property<T> property);

    <T> T getBehavior(@Nonnull Class<T> type);
//    <T extends Comparable<T>, V extends T> Block withProperty(BlockPrototype.Property<T> property, V value);
//
//    <T extends Comparable<T>> Block cycleProperty(BlockPrototype.Property<T> property);

    AABBd[] getBoundingBoxes();
}
