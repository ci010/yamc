package oc.yamc.world;

import oc.yamc.RuntimeObject;
import oc.yamc.entity.Entity;
import oc.yamc.math.Position;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.List;

public interface Chunk extends RuntimeObject {
    /**
     * Get block in a specific path
     *
     * @param x posX-coordinate of the block related to getChunkXZ coordinate system
     * @param y posY-coordinate of the block related to getChunkXZ coordinate system
     * @param z posZ-coordinate of the block related to getChunkXZ coordinate system
     * @return the block in the specified path
     */
    default Block getBlock(int x, int y, int z) {
        return getBlock(new Position(x, y, z));
    }

    BlockCursor getBlockRef(int x, int y, int z);

    Collection<Block> getRuntimeBlock();

    @Nonnull
    List<Entity> getEntities();

    Block getBlock(Position pos);

    /**
     * Set block in a specific path
     *
     * @param x posX-coordinate of the block related to getChunkXZ coordinate system
     * @param y posY-coordinate of the block related to getChunkXZ coordinate system
     * @param z posZ-coordinate of the block related to getChunkXZ coordinate system
     */
    default Block setBlock(int x, int y, int z, Block destBlock) {
        return setBlock(new Position(x, y, z), destBlock);
    }

    Block setBlock(Position pos, Block destBlock);

    interface Store {
        Collection<Chunk> getChunks();

        @Nonnull
        Chunk getChunk(@Nonnull Position pos);

        /**
         * Touch the getChunkXZ at the the position, ensure it loaded
         */
        void touchChunk(@Nonnull Position pos);

        /**
         * Dispose the getChunkXZ at the position
         */
        void discardChunk(@Nonnull Position pos);
    }
}
