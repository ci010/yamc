package oc.yamc.world;

import oc.yamc.math.Position;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public interface BlockAccess extends Block {
    Position getPosition();

    World getWorld();

    Chunk getChunk();

    @Nullable
    <T> T getComponent(@Nonnull String name);

    @Nullable
    <T> T getComponent(@Nonnull Class<T> type);
}
