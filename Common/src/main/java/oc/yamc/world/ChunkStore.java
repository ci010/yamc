package oc.yamc.world;

import io.netty.util.collection.LongObjectHashMap;
import io.netty.util.collection.LongObjectMap;
import oc.yamc.GameContext;
import oc.yamc.math.Position;
import org.joml.SimplexNoise;

import javax.annotation.Nonnull;
import java.util.Collection;

public class ChunkStore implements Chunk.Store {
    // should do the io operation to load getChunkXZ
    private LongObjectMap<Chunk> chunks = new LongObjectHashMap<>();
    private GameContext gameContext;

    public ChunkStore(GameContext gameContext) {
        this.gameContext = gameContext;
    }

    @Override
    public Collection<Chunk> getChunks() {
        return chunks.values();
    }

    private float height = 16;

    @Nonnull
    @Override
    public Chunk getChunk(@Nonnull Position pos) {
        long cp = pos.getChunkXZ();
        Chunk chunk = this.chunks.get(cp);
        if (chunk != null)
            return chunk;
        ChunkColumn c = new ChunkColumn(gameContext);
        c.data = decorateChunk(pos.getChunkX() << 4, pos.getChunkZ() << 4);
        this.chunks.put(cp, c);
        gameContext.post(new ChunkColumn.LoadEvent(pos.getChunkX(), pos.getChunkZ(), c.data));
        return c;
    }

    @Override
    public void discardChunk(@Nonnull Position pos) {
        long cp = pos.getChunkXZ();
        Chunk remove = this.chunks.remove(cp);
        if (remove != null) {
            gameContext.post(new ChunkColumn.UnloadEvent(pos.getChunkX(), pos.getChunkZ()));
        }
    }

    @Override
    public void touchChunk(@Nonnull Position pos) {
        long cp = pos.getChunkXZ();
        Chunk chunk = this.chunks.get(cp);
        if (chunk == null) {
            ChunkColumn c = new ChunkColumn(gameContext);
            c.data = decorateChunk(pos.getChunkX() << 4, pos.getChunkZ() << 4);
            this.chunks.put(cp, c);
            gameContext.post(new ChunkColumn.LoadEvent(pos.getChunkX(), pos.getChunkZ(), c.data));
        }
    }

    private int[][] decorateChunk(int cx, int cz) {
        int[][] data = new int[16][16 * 16 * 16];
        for (int i = 0; i < 16; i++) {
            for (int j = 0; j < 16; j++) {
                double noise = (SimplexNoise.noise((i + cx) / 32F, (j + cz) / 32F) + 1) / 2D;
                int h = (int) (height * noise) + 2;
                for (int k = 0; k < h; k++) {
                    data[k / 16][i << 8 | (k & 0xF) << 4 | j] = 2;
                }
            }
        }
        return data;
    }
}
