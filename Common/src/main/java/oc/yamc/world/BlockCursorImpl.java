package oc.yamc.world;

import com.google.common.collect.ImmutableMap;
import oc.yamc.entity.Entity;
import oc.yamc.math.Position;
import oc.yamc.mod.ModContainer;
import oc.yamc.registry.Registry;
import oc.yamc.registry.RegistryEntry;
import org.joml.AABBd;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

class BlockCursorImpl implements BlockCursor {
    private int x, y, z;
    private BlockShared block;
    private ChunkColumn chunk;
    private WorldCommon world;

    BlockCursorImpl(BlockShared block, Position pos) {
        this.block = block;
        this.x = pos.getX();
        this.y = pos.getY();
        this.z = pos.getZ();
    }

//    @Override
//    public <T extends Comparable<T>, V extends T> BlockCursor setProperty(BlockPrototype.Property<T> property, V value) {
//        this.world.setBlock(this.position, this.block.withProperty(property, value));
//        return this;
//    }
//
//    @Override
//    public <T extends Comparable<T>> BlockCursor setToNextProperty(BlockPrototype.Property<T> property) {
//        this.world.setBlock(this.position, this.block.cycleProperty(property));
//        return this;
//    }

    public void commit() {

    }

    @Override
    public ImmutableMap<BlockPrototype.Property<?>, Comparable<?>> getProperties() {
        return block.getProperties();
    }

    @Override
    public Position getPosition() {
        return new Position(x, y, z);
    }

    @Override
    public World getWorld() {
        return world;
    }

    @Override
    public Chunk getChunk() {
        return chunk;
    }

    @Override
    public BlockCursor move(int x, int y, int z) {
        if (x == 0 && y == 0 && z == 0) return this;

        commit();

        this.x += x;
        this.y += y;
        this.z += z;

        return this;
    }

    @Override
    public BlockCursor at(Position pos) {
        if (pos.getX() == this.x && pos.getY() == this.y && pos.getZ() == z) return this;
        commit();
        this.x = pos.getX();
        this.y = pos.getY();
        this.z = pos.getZ();
        return null;
    }

    @Override
    public <T extends Comparable<T>> T getProperty(BlockPrototype.Property<T> property) {
        return block.getProperty(property);
    }

    @Override
    public AABBd[] getBoundingBoxes() {
        return block.getBoundingBoxes();
    }

    @Override
    public Class<Block> getRegistryType() {
        return block.getRegistryType();
    }

    @Override
    public Block localName(String name) {
        return block.localName(name);
    }

    @Override
    public Block metadata(String name, String data) {
        return null;
    }

    @Override
    public String getLocalName() {
        return block.getLocalName();
    }

    @Override
    public ModContainer getOwner() {
        return block.getOwner();
    }

    @Override
    public Registry<? extends RegistryEntry<Block>> getAssignedRegistry() {
        return block.getAssignedRegistry();
    }

    @Override
    public String getUniqueName() {
        return block.getUniqueName();
    }

    @Override
    @Nullable
    public <T> T getBehavior(@Nonnull Class<T> type) {
        return block.getBehavior(type);
    }

    @Override
    public boolean canPlace(World world, Entity entity, Block block) {
        return this.block.canPlace(world, entity, block);
    }

    @Override
    public void onPlaced(World world, Entity entity, Block block) {
        this.block.onPlaced(world, entity, block);
    }

    @Override
    public boolean shouldActivated(World world, Entity entity, Position position, Block block) {
        return this.block.shouldActivated(world, entity, position, block);
    }

    @Override
    public void onActivated(World world, Entity entity, Position pos, Block block) {
        this.block.onActivated(world, entity, pos, block);
    }

    @Override
    public boolean onTouch(Block block) {
        return this.block.onTouch(block);
    }

    @Override
    public void onTouched(Block block) {
        this.block.onTouched(block);
    }

    @Nullable
    @Override
    public <T> T getComponent(@Nonnull String name) {
        return null;
    }

    @Nullable
    @Override
    public <T> T getComponent(@Nonnull Class<T> type) {
        return null;
    }
}
