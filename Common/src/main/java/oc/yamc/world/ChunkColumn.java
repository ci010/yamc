package oc.yamc.world;

import com.google.common.collect.Maps;
import oc.yamc.GameContext;
import oc.yamc.entity.Entity;
import oc.yamc.event.Event;
import oc.yamc.math.Position;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class ChunkColumn implements Chunk {
    int[][] data = new int[16][16 * 16 * 16];
    private GameContext context;
    private List<Entity> entities = new ArrayList<>();
    private Map<Position, Block> blockObjects = Maps.newHashMap();
    private Map<String, Object> components;

    ChunkColumn(GameContext context) {
        this.context = context;
    }

    @Override
    public BlockCursor getBlockRef(int x, int y, int z) {
        return null;
    }

    @Override
    public Collection<Block> getRuntimeBlock() {
        return blockObjects.values();
    }

    @Nonnull
    @Override
    public List<Entity> getEntities() {
        return entities;
    }

    @Override
    public Block getBlock(Position pos) {
        Block object = blockObjects.get(pos);
        if (object != null) return object;
        if (pos.getY() < 0) {
            return context.getBlockRegistry().getValue(0);
        }

        int heightIndex = pos.getChunkY();
        int posIndex = pos.pack();
        int id = data[heightIndex][posIndex];
        return context.getBlockRegistry().getValue(id);
    }

    @Override
    public Block setBlock(Position pos, Block destBlock) {
        if (pos.getY() < 0) {
            return context.getBlockRegistry().getValue(0);
        }

        int heightIndex = pos.getChunkY();
        int posIndex = pos.pack();

        Block prev = blockObjects.get(pos);
        if (prev != null) {
            blockObjects.remove(pos);
        }

        int id = 0;
        if (destBlock != null) {
            if (destBlock instanceof BlockRuntime) {
                blockObjects.put(pos, destBlock);
            }
            id = context.getBlockRegistry().getId(destBlock);
        }

        int prevId = data[heightIndex][posIndex];

        if (prev == null) {
            prev = context.getBlockRegistry().getValue(prevId);
        }

        if (prevId == id) {
            return prev;
        }

        data[heightIndex][posIndex] = id;

        context.post(new BlockChangeEvent(pos, id));

        return prev;
    }

    @Nullable
    @Override
    @SuppressWarnings("unchecked")
    public <T> T getComponent(@Nonnull String name) {
        return (T) components.get(name);
    }

    @Nullable
    @Override
    @SuppressWarnings("unchecked")
    public <T> T getComponent(@Nonnull Class<T> type) {
        return (T) components.get(type.getName());
    }

    @Nullable
    @Override
    public <T> T getBehavior(Class<T> type) {
        return null;
    }

    public static class LoadEvent implements Event {
        public final int x, z;
        public final int[][] blocks;

        public LoadEvent(int x, int z, int[][] blocks) {
            this.x = x;
            this.z = z;
            this.blocks = blocks;
        }
    }

    public static class UnloadEvent implements Event {
        public final int x, z;

        public UnloadEvent(int x, int z) {
            this.x = x;
            this.z = z;
        }
    }
}
