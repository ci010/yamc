package oc.yamc.world;

import org.joml.AABBf;

import java.util.Comparator;
import java.util.Iterator;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class BlockSectionImpl implements BlockSection {
    private AABBf region;
    private World world;

    @Override
    public World getWorld() {
        return world;
    }

    @Override
    public BlockSection filter(Predicate<BlockAccess> predicate) {
        return null;
    }

    @Override
    public BlockSection sorted(Comparator<BlockAccess> comparator) {
        return null;
    }

    @Override
    public BlockSection each(Consumer<BlockAccess> action) {
        return null;
    }


    @Override
    public void commit(boolean async) {

    }

    @Override
    public AABBf getBoundingBox() {
        return region;
    }

    @Override
    public Iterator<BlockAccess> iterator() {
        return null;
    }
}
