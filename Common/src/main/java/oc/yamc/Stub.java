package oc.yamc;

public final class Stub {
    static Engine engine;

    private Stub() {}

    public static void setEngine(Engine inEngine) {
        Class<?> aClass = StackWalker.getInstance(StackWalker.Option.RETAIN_CLASS_REFERENCE).getCallerClass();
        if (aClass.getPackage().getName().startsWith("oc.yamc")) {

        }
        engine = inEngine;
    }
}
