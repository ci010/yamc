package oc.yamc.batch;

import lombok.Value;

import java.util.List;

@Value
public class EntityContext {
    private TransformComponent transformComponent = new TransformComponent();
    private CollsionComponent collisionSystem = new CollsionComponent();

    private List<Entity> entities;

    public Entity getEntity(int i) {
        return entities.get(i);
    }
}
