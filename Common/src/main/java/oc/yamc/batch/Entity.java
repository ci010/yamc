package oc.yamc.batch;

import com.google.common.collect.ImmutableList;
import lombok.Value;

@Value
public class Entity {
    private final int id;
    private int[] componentsPointer;
    private ImmutableList<String> components;
}
