package oc.yamc.batch;

import lombok.Getter;

public class MassComponent {
    @Getter
    private float[] mass;
}
