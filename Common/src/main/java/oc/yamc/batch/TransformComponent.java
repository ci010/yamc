package oc.yamc.batch;

import lombok.Getter;

public class TransformComponent {
    @Getter
    private float[] position, rotation,
            deltaPosition, deltaRotation;

    @Getter
    private int count;
}
