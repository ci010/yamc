package oc.yamc.batch;

class Utils {
    static float[] ensureSize(float[] src, int size) {
        if (size > src.length) {
            float[] arr = new float[src.length * 2];
            System.arraycopy(src, 0, arr, 0, src.length);
            return arr;
        }
        return src;
    }
}
