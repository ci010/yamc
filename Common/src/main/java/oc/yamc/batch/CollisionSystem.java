package oc.yamc.batch;

import oc.yamc.math.AABBs;
import oc.yamc.math.Position;
import oc.yamc.world.Block;
import oc.yamc.world.World;
import org.joml.AABBd;

public class CollisionSystem {
    public void update(World world, EntityContext batch) {
        TransformComponent transformComponent = batch.getTransformComponent();
        CollsionComponent collsionComponent = batch.getCollisionSystem();

        AABBd[] collision = collsionComponent.getBoundingBoxes();
        float[] poses = transformComponent.getPosition();
        float[] deltaPosition = transformComponent.getDeltaPosition();

        AABBd entityBox = new AABBd(),
                blockBox = new AABBd();

        for (int i = 0; i < collision.length; i++) {
            if (collision[i] == null) return;
            AABBd aabBd = collision[i];

            float mx = deltaPosition[i], my = deltaPosition[i + 1], mz = deltaPosition[i + 2];

            if (mx == 0 && my == 0 && mz == 0)
                continue;
            Entity entity = batch.getEntity(i);
//            AABBs.translate(entity.getBoundingBox(), poses[i], poses[i + 1], poses[i + 2], entityBox);

            int directionX = mx == -0 ? 0 : Float.compare(mx, 0),
                    directionY = my == -0 ? 0 : Float.compare(my, 0),
                    directionZ = mz == -0 ? 0 : Float.compare(mz, 0);
            int minX = (int) Math.floor(entityBox.minX),
                    minY = (int) Math.floor(entityBox.minY),
                    minZ = (int) Math.floor(entityBox.minZ),
                    maxX = (int) Math.floor(entityBox.maxX),
                    maxY = (int) Math.floor(entityBox.maxY),
                    maxZ = (int) Math.floor(entityBox.maxZ);

            double xFix = Integer.MAX_VALUE, yFix = Integer.MAX_VALUE, zFix = Integer.MAX_VALUE;

            if (directionX != 0) {
                int x = (int) Math.floor(directionX == -1 ? minX : directionX == 1 ? maxX : 0 + mx);
                for (int z = minZ; z <= maxZ; z++)
                    for (int y = minY; y <= maxY; y++) {
                        Block block = world.getBlock(new Position(x, y, z));
                        AABBd[] blockBoxes = block.getBoundingBoxes();
                        if (blockBoxes.length != 0)
                            for (AABBd blockBoxLocal : blockBoxes) {
                                AABBs.translate(blockBoxLocal, x, y, z, blockBox);
                                if (blockBox.testAABB(entityBox)) {
                                    xFix = Math.min(xFix,
                                            Math.min(Math.abs(blockBox.maxX - entityBox.minX),
                                                    Math.abs(blockBox.minX - entityBox.maxX)));
                                }
                            }
                    }
            }
            if (directionY != 0) {
                int y = (int) Math.floor(directionY == -1 ? minY : directionY == 1 ? maxY : 0 + my);
                for (int z = minZ; z <= maxZ; z++)
                    for (int x = minX; x <= maxX; x++) {
                        Block block = world.getBlock(new Position(x, y, z));
                        AABBd[] blockBoxes = block.getBoundingBoxes();
                        if (blockBoxes.length != 0)
                            for (AABBd blockBoxLocal : blockBoxes) {
                                AABBs.translate(blockBoxLocal, x, y, z, blockBox);
                                if (blockBox.testAABB(entityBox)) {
                                    yFix = Math.min(yFix,
                                            Math.min(Math.abs(blockBox.maxY - entityBox.minY),
                                                    Math.abs(blockBox.minY - entityBox.maxY)));
                                }
                            }
                    }
            }
            if (directionZ != 0) {
                int z = (int) Math.floor(directionZ == -1 ? minZ : directionZ == 1 ? maxZ : 0 + mz);
                for (int y = minY; y <= maxY; y++)
                    for (int x = minX; x <= maxX; x++) {
                        Block block = world.getBlock(new Position(x, y, z));
                        AABBd[] blockBoxes = block.getBoundingBoxes();
                        if (blockBoxes.length != 0)
                            for (AABBd blockBoxLocal : blockBoxes) {
                                AABBs.translate(blockBoxLocal, x, y, z, blockBox);
                                if (blockBox.testAABB(entityBox)) {
                                    zFix = Math.min(zFix,
                                            Math.min(Math.abs(blockBox.maxZ - entityBox.minZ),
                                                    Math.abs(blockBox.minZ - entityBox.maxZ)));
                                }
                            }
                    }
            }
            deltaPosition[i] += xFix;
            deltaPosition[i + 1] += yFix;
            deltaPosition[i + 2] += zFix;
        }
    }
}
