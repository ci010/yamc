package oc.yamc.batch;

public class MotionSystem {
    public void update(EntityContext batch) {
        TransformComponent transformComponent = batch.getTransformComponent();

        float[] pos = transformComponent.getPosition(),
                rot = transformComponent.getRotation(),
                dpos = transformComponent.getDeltaPosition(),
                drot = transformComponent.getDeltaRotation();
        for (int i = 0; i < transformComponent.getCount(); i++) {
            pos[i] += dpos[i];
            pos[i + 1] += dpos[i + 1];
            pos[i + 2] += dpos[i + 2];
        }

        for (int i = 0; i < transformComponent.getCount(); i++) {
            rot[i] += drot[i];
            rot[i + 1] += drot[i + 1];
            rot[i + 2] += drot[i + 2];
        }
    }
}
