package oc.yamc.util;

public class VarImpl implements Var {
    @Override
    public Int asInt() {
        return null;
    }

    @Override
    public Short asShort() {
        return null;
    }

    @Override
    public Byte asByte() {
        return null;
    }

    @Override
    public Float asFloat() {
        return null;
    }

    @Override
    public Double asDouble() {
        return null;
    }

    @Override
    public Boolean asBoolean() {
        return null;
    }

    @Override
    public <T extends java.lang.Enum<T>> Enum<T> asEnum() {
        return null;
    }

    static class Enum0<T extends java.lang.Enum<T>> implements Enum<T> {
        private T value;

        @Override
        public T get() {
            return value;
        }

        @Override
        public void set(T v) {

            value = v;
        }
    }

    static class Boolean0 implements Boolean {
        private boolean value;

        @Override
        public boolean get() {
            return value;
        }

        @Override
        public void set(boolean v) {

            value = v;
        }
    }

    static class Int0 implements Int {
        private int value;

        @Override
        public int get() {
            return value;
        }

        @Override
        public void set(int v) {

            value = v;
        }
    }

    static class Short0 implements Short {
        private short value;

        @Override
        public short get() {
            return value;
        }

        @Override
        public void set(short v) {

            value = v;
        }
    }

    static class Byte0 implements Byte {
        private byte value;

        @Override
        public byte get() {
            return value;
        }

        @Override
        public void set(byte v) {

            value = v;
        }
    }

//    static class Boolean0 implements Boolean {
//        private long value;
//
//        @Override
//        public long get() {
//            return value;
//        }
//
//        @Override
//        public void set(long v) {
//
//            value = v;
//        }
//    }

    static class Float0 implements Float {
        private float value;

        @Override
        public float get() {
            return value;
        }

        @Override
        public void set(float v) {

            value = v;
        }
    }

    static class Double0 implements Double {
        private double value;

        @Override
        public double get() {
            return value;
        }

        @Override
        public void set(double v) {

            value = v;
        }
    }

    static class Named {

    }
}
