package oc.yamc.util;

import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.function.Supplier;

public interface Call {
    static Runnable run(Call call) {
        return () -> {
            try {
                call.call();
            } catch (Exception e) {
                throw new IllegalStateException(e);
            }
        };
    }

    static <T> Supplier<T> call(Callable<T> call) {
        return () -> {
            try {
                return call.call();
            } catch (Exception e) {
                throw new IllegalStateException(e);
            }
        };
    }

    static <T> Supplier<T> callOrElse(Callable<T> call, T value) {
        return () -> {
            try {
                return call.call();
            } catch (Exception e) {
                return value;
            }
        };
    }

    void call() throws Exception;

    static <T> T await(Future<T> future) {
        try {
            return future.get();
        } catch (InterruptedException | ExecutionException e) {
            throw new IllegalStateException(e);
        }
    }

    static <T> Optional<T> awaits(Future<T> future) {
        try {
            return Optional.ofNullable(future.get());
        } catch (InterruptedException | ExecutionException e) {
            return Optional.empty();
        }
    }
}
