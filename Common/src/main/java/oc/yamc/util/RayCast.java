package oc.yamc.util;

import oc.yamc.math.Position;
import oc.yamc.world.Block;
import oc.yamc.world.World;
import org.joml.AABBd;
import org.joml.Vector2d;
import org.joml.Vector3f;

import java.util.List;
import java.util.function.Predicate;

public class RayCast {
    public static void all(List<Hit> results, World world, Vector3f from, Vector3f dir, float distance, Predicate<Block> ignore) {
        Vector3f rayDir = dir.normalize(new Vector3f()).mul(distance);

        Vector3f dist = rayDir.add(from, new Vector3f());
        List<Position> all = FastVoxelRayCast.ray(from, dist);

        Vector3f local = new Vector3f();
        Vector2d result = new Vector2d();
        for (Position pos : all) {
            Block object = world.getBlock(pos);
            if (ignore.test(object)) continue;
            from.sub(pos.getX(), pos.getY(), pos.getZ(), local);
            AABBd[] boxes = object.getBoundingBoxes();
            for (AABBd box : boxes) {
                boolean hit = box.intersectRay(local.x, local.y, local.z, rayDir.x, rayDir.y, rayDir.z, result);
                if (hit) {
                    Vector3f hitPoint = local.add(rayDir.mul((float) result.x));
                    Facing facing = Facing.NORTH;
                    if (hitPoint.x == 0f) {
                        facing = Facing.WEST;
                    } else if (hitPoint.x == 1f) {
                        facing = Facing.EAST;
                    } else if (hitPoint.y == 0f) {
                        facing = Facing.DOWN;
                    } else if (hitPoint.y == 1f) {
                        facing = Facing.UP;
                    } else if (hitPoint.z == 0f) {
                        facing = Facing.SOUTH;
                    } else if (hitPoint.z == 1f) {
                        facing = Facing.NORTH;
                    }
                    results.add(new Hit(pos, object, hitPoint, facing));
                }
            }
        }
    }

    public static Hit first(World world, Vector3f from, Vector3f dir, float distance, Predicate<Block> ignore) {
        Vector3f rayDir = dir.normalize(new Vector3f()).mul(distance);

        Vector3f dist = rayDir.add(from, new Vector3f());
        List<Position> all = FastVoxelRayCast.ray(from, dist);

        Vector3f local = new Vector3f();
        Vector2d result = new Vector2d();
        for (Position pos : all) {
            Block object = world.getBlock(pos);
            if (ignore.test(object)) continue;
            from.sub(pos.getX(), pos.getY(), pos.getZ(), local);
            AABBd[] boxes = object.getBoundingBoxes();
            for (AABBd box : boxes) {
                boolean hit = box.intersectRay(local.x, local.y, local.z, rayDir.x, rayDir.y, rayDir.z, result);
                if (hit) {
                    Vector3f hitPoint = local.add(rayDir.mul((float) result.x));
                    Facing facing = Facing.NORTH;
                    if (hitPoint.x == 0f) {
                        facing = Facing.WEST;
                    } else if (hitPoint.x == 1f) {
                        facing = Facing.EAST;
                    } else if (hitPoint.y == 0f) {
                        facing = Facing.DOWN;
                    } else if (hitPoint.y == 1f) {
                        facing = Facing.UP;
                    } else if (hitPoint.z == 0f) {
                        facing = Facing.SOUTH;
                    } else if (hitPoint.z == 1f) {
                        facing = Facing.NORTH;
                    }
                    return new Hit(pos, object, hitPoint, facing);
                }
            }
        }
        return null;
    }

    public static class Hit {
        public final Position position;
        public final Block block;
        public final Vector3f hit;
        public final Facing face;

        public Hit(Position position, Block block, Vector3f hit, Facing face) {
            this.position = position;
            this.block = block;
            this.hit = hit;
            this.face = face;
        }
    }
}
