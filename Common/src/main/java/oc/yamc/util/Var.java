package oc.yamc.util;

public interface Var {
    Int asInt();

    Short asShort();

    Byte asByte();

    Float asFloat();

    Double asDouble();

    Boolean asBoolean();

    <T extends java.lang.Enum<T>> Enum<T> asEnum();

    interface Named extends Var {
        String getName();
    }

    interface Enum<T extends java.lang.Enum<T>> {
        T get();

        void set(T v);
    }

    interface Boolean {
        boolean get();

        void set(boolean v);
    }

    interface Int {
        int get();

        void set(int v);

        interface Bounded {
            int max();

            int min();
        }
    }

    interface Short {
        short get();

        void set(short v);

        interface Bounded {
            short max();

            short min();
        }
    }

    interface Byte {
        byte get();

        void set(byte v);

        interface Bounded {
            byte max();

            byte min();
        }
    }

    interface Float {
        float get();

        void set(float v);

        interface Bounded {
            float max();

            float min();
        }
    }

    interface Double {
        double get();

        void set(double v);

        interface Bounded {
            double max();

            double min();
        }
    }
}