package oc.yamc.event;

import oc.yamc.registry.Impl;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * A state machine that maintains multiple event handlers. It used for the case that you want several event handler share same context.
 */
public abstract class EventHandlerModule {
    public static EventHandlerModule wrap(Type type, EventHandler... handlers) {
        return new EventHandlerModule() {
            @Override
            public EventHandler[] get() {
                return handlers;
            }

            @Override
            public Type getType() {
                return type;
            }
        };
    }

    public abstract EventHandler[] get();

    public abstract Type getType();

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.TYPE)
    public @interface RegType {
        /**
         * @return the local name of the type
         */
        String value();
    }

    public static abstract class Type extends Impl<Type> {
        public abstract EventHandlerModule create();
    }
}
