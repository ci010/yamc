package oc.yamc.event;

public interface Event {
	default boolean isCancellable() {
        return false;
	}
}
