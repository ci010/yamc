package oc.yamc.event;

import lombok.NonNull;

public interface EventBus {
    static EventBus create() {
        return new AsmEventBus();
    }

    /**
     * Post an event to the event bus.
     *
     * @param event The event instance.
     * @return True if cancelled, false if not.
     */
    boolean post(@NonNull Event event);

    /**
     * Register an event handler module to the event bus. If the argument extends {@link EventHandlerModule}, it will be directly registered.
     * If the argument not extends {@link EventHandlerModule}, the bus will generate a {@link EventHandlerModule} proxy, and register it.
     *
     * @param eventHandlerModule The event handler module object.
     * @return The event handlers module
     */
    EventHandlerModule register(@NonNull Object eventHandlerModule);

    void unregister(@NonNull Object eventHandlerModule);

    /**
     * The
     *
     * @param eventType
     * @return
     */
    Executor getEventExecutor(@NonNull Class<? extends Event> eventType);

    interface Executor {
        boolean execute(@NonNull Event event);

        Class<? extends Event> getEventType();
    }
}
