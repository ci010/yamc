package oc.yamc.event;

public interface Cancellable extends Event {
    boolean isCancelled();

    void setCancelled(boolean cancelled);

    default boolean isCancellable() {
        return true;
    }
}
