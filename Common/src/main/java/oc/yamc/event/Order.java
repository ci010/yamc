package oc.yamc.event;

public enum Order {
	FIRST,
	EARLY,
	DEFAULT,
	LATE,
	LAST
}
