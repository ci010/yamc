package oc.yamc.event;

import oc.yamc.GameContext;
import oc.yamc.registry.RegistryEntry;

/**
 * EventSource is an event emitter which might emit an event when it performs.
 */
public interface EventSource<T extends Event> extends RegistryEntry<EventSource<? extends Event>> {
    /**
     * Perform the action under the certain context.
     *
     * @param gameContext The game context
     * @return The triggering event
     */
    T construct(GameContext gameContext);

    Class<T> getEventType();
}
