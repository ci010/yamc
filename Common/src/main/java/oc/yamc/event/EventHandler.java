package oc.yamc.event;

import java.util.function.Consumer;

public abstract class EventHandler implements Comparable<EventHandler> {
    private final Class<?> eventType;
    private final boolean receiveCancelled;
    private final Order order;

    public EventHandler(Class<?> eventType, boolean receiveCancelled, Order order) {
        this.eventType = eventType;
        this.receiveCancelled = receiveCancelled;
        this.order = order;
    }

    public EventHandler(Class<?> eventType) {
        this.eventType = eventType;
        this.receiveCancelled = false;
        this.order = Order.DEFAULT;
    }

    public static <T extends Event> EventHandler wrap(Class<T> type, Consumer<T> consumer) {
        return new EventHandler(type) {
            @Override
            @SuppressWarnings("unchecked")
            public void handle(Event event) {
                consumer.accept((T) event);
            }
        };
    }

    public Class<?> getEventType() {
        return eventType;
    }

    public boolean isReceiveCancelled() {
        return receiveCancelled;
    }

    public Order getOrder() {
        return order;
    }

    public abstract void handle(Event event);

    @Override
    public int compareTo(EventHandler o) {
        return getOrder().ordinal() - o.getOrder().ordinal();
    }
}
