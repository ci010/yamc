package oc.yamc.event;

import lombok.NonNull;
import oc.yamc.Engine;
import oc.yamc.util.SafeClassDefiner;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Type;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.function.Function;

import static org.objectweb.asm.Opcodes.*;

public class AsmEventBus implements EventBus {
    private static final Map<Class, Function<Object, EventHandler>[]> cache = new HashMap<>();
    private final Map<Class<?>, ExecutorImpl> eventExecutors = new HashMap<>();
    private final Map<Object, EventHandlerModule> registeredContext = new HashMap<>();
    private int uniqueId = 0;

    @Override
    public boolean post(@NonNull Event event) {
        Objects.requireNonNull(event);

        Executor executor = eventExecutors.get(event.getClass());
        if (executor != null)
            return executor.execute(event);
        return false;
    }

    @Override
    public EventHandlerModule register(@NonNull Object eventHandlerModule) {
        if (registeredContext.containsKey(eventHandlerModule))
            throw new EventException("Listener has been registered.");

        EventHandlerModule context;
        if (eventHandlerModule instanceof EventHandlerModule) {
            context = (EventHandlerModule) eventHandlerModule;
        } else {
            EventHandlerModule.RegType annotation = eventHandlerModule.getClass().getAnnotation(EventHandlerModule.RegType.class);
            String localName;
            if (annotation != null) {
                localName = annotation.value();
            } else {
                localName = eventHandlerModule.getClass().getSimpleName();
            }
            var functions = cache.computeIfAbsent(eventHandlerModule.getClass(), this::generateEventHandlers);
            var type = new EventHandlerModule.Type() {
                @Override
                public EventHandlerModule create() {
                    EventHandler[] handlers = new EventHandler[functions.length];
                    for (int i = 0; i < handlers.length; i++)
                        handlers[i] = functions[i].apply(eventHandlerModule);
                    return EventHandlerModule.wrap(this, handlers);
                }
            }.localName(localName);
            context = type.create();
        }

        EventHandler[] handlers = context.get();
        for (EventHandler executor : handlers) {
            getExecutorByType(executor.getEventType()).add(executor);
        }

        registeredContext.put(eventHandlerModule, context);

        return context;
    }

    @Override
    public void unregister(@NonNull Object eventHandlerModule) {
        Objects.requireNonNull(eventHandlerModule);

        EventHandlerModule context = registeredContext.get(eventHandlerModule);
        EventHandler[] handlers = context.get();
        if (handlers == null)
            return;

        for (EventHandler handler : handlers) {
            Class<?> eventType = handler.getEventType();
            for (var entry : eventExecutors.entrySet()) {
                Class<?> childEventType = entry.getKey();
                if (!eventType.isAssignableFrom(childEventType))
                    continue;

                entry.getValue().orderedExecutors.get(handler.getOrder()).remove(handler);
            }
        }
        registeredContext.remove(eventHandlerModule);
    }

    @Override
    public Executor getEventExecutor(@NonNull Class<? extends Event> eventType) {
        return getExecutorByType(eventType);
    }

    private ExecutorImpl getExecutorByType(Class<?> eventType) {
        if (!Event.class.isAssignableFrom(eventType)) return null;
        ExecutorImpl executor = eventExecutors.get(eventType);
        if (executor != null) return executor;
        Class<?> parent = eventType.getSuperclass();
        executor = new ExecutorImpl(getExecutorByType(parent), (Class<? extends Event>) eventType, new EnumMap<>(Order.class));
        eventExecutors.put(eventType, executor);
        return executor;
    }

    private void exec(EventHandler executor, Event event) {
        try {
            executor.handle(event);
        } catch (Exception e) {
            Engine.getLogger().warn("Failed to handle event.");
            e.printStackTrace();
        }
    }

    /**
     *
     */
    private Function<Object, EventHandler>[] generateEventHandlers(Class clazz) {
        List<Function<Object, EventHandler>> builders = new ArrayList<>();
        for (Method method : clazz.getDeclaredMethods()) {
            Listener anno = method.getAnnotation(Listener.class);
            if (anno == null)
                continue;

            int modifiers = method.getModifiers();

            if (!Modifier.isPublic(modifiers) || Modifier.isStatic(modifiers) || Modifier.isAbstract(modifiers)) {
                Engine.getLogger().warn("Require event bus listened method is public and not static/abstract! " + clazz); // TODO: support static
                continue;
            }

            if (method.getParameterCount() != 1) {
                Engine.getLogger().warn("Require event bus listened method has only one event parameter! " + clazz);
                continue;
            }

            Class<?> eventType = method.getParameterTypes()[0];
            if (!Event.class.isAssignableFrom(eventType)) {
                Engine.getLogger().warn("Require event bus listened method has only one event parameter! " + clazz);
                continue;
            }
            builders.add((lis) -> {
                try {
                    return createEventHandler(method, lis, anno.order(),
                            anno.receiveCancelled(), eventType);
                } catch (ReflectiveOperationException e) {
                    Engine.getLogger().warn(String.format("Failed to register listener %s.%s .",
                            clazz.getSimpleName(), method.getName()), new EventException(e));
                    return null;
                }
            });
        }
        return builders.toArray(new Function[0]);
    }

    private EventHandler createEventHandler(Method method, Object owner, Order order, boolean receiveCancelled,
                                            Class<?> eventType) throws ReflectiveOperationException {
        Class<?> ownerType = owner.getClass();
        String ownerName = ownerType.getTypeName().replace('.', '/');
        String ownerDesc = Type.getDescriptor(ownerType);
        String eventName = eventType.getTypeName().replace('.', '/');
        String handlerDesc = Type.getMethodDescriptor(method);
        String handlerName = method.getName();
        String className = getUniqueName(ownerType.getSimpleName(), handlerName, eventType.getSimpleName());

        ClassWriter cw = new ClassWriter(0);
        FieldVisitor fv;
        MethodVisitor mv;

        cw.visit(V1_8, ACC_PUBLIC + ACC_SUPER, className, null,
                "oc/yamc/event/EventHandler", null);

        cw.visitSource(".dynamic", null);

//        cw.visitInnerClass("oc/yamc/event/EventHandler",
//                "oc/yamc/event/AsmEventBus", "EventExecutor", ACC_PUBLIC + ACC_STATIC + ACC_ABSTRACT);

        {
            fv = cw.visitField(ACC_PRIVATE + ACC_FINAL, "owner", ownerDesc, null, null);
            fv.visitEnd();
        }
        {
            mv = cw.visitMethod(ACC_PUBLIC, "<init>",
                    "(" + ownerDesc + "Ljava/lang/Class;ZLoc/yamc/event/Order;)V",
                    "(" + ownerDesc + "Ljava/lang/Class<*>;ZLoc/yamc/event/Order;)V", null);
            mv.visitCode();
            mv.visitVarInsn(ALOAD, 0);
            mv.visitVarInsn(ALOAD, 2);
            mv.visitVarInsn(ILOAD, 3);
            mv.visitVarInsn(ALOAD, 4);
            mv.visitMethodInsn(INVOKESPECIAL, "oc/yamc/event/EventHandler", "<init>",
                    "(Ljava/lang/Class;ZLoc/yamc/event/Order;)V", false);
            mv.visitVarInsn(ALOAD, 0);
            mv.visitVarInsn(ALOAD, 1);
            mv.visitFieldInsn(PUTFIELD, className, "owner", ownerDesc);
            mv.visitInsn(RETURN);
            mv.visitMaxs(4, 5);
            mv.visitEnd();
        }
//        {
//            mv = cw.visitMethod(ACC_PUBLIC, "getOwner", "()Ljava/lang/Object;", null, null);
//            mv.visitCode();
//            mv.visitVarInsn(ALOAD, 0);
//            mv.visitFieldInsn(GETFIELD, className, "owner", ownerDesc);
//            mv.visitInsn(ARETURN);
//            mv.visitMaxs(1, 1);
//            mv.visitEnd();
//        }
        {
            mv = cw.visitMethod(ACC_PUBLIC, "handle", "(Loc/yamc/event/Event;)V", null, null);
            mv.visitCode();
            mv.visitVarInsn(ALOAD, 0);
            mv.visitFieldInsn(GETFIELD, className, "owner", ownerDesc);
            mv.visitVarInsn(ALOAD, 1);
            mv.visitTypeInsn(CHECKCAST, eventName);
            mv.visitMethodInsn(INVOKEVIRTUAL, ownerName, handlerName, handlerDesc, false);
            mv.visitInsn(RETURN);
            mv.visitMaxs(2, 2);
            mv.visitEnd();
        }
        cw.visitEnd();

        Class<?> executorType = SafeClassDefiner.INSTANCE.defineClass(ownerType.getClassLoader(), className,
                cw.toByteArray());
        Constructor<?> executorConstructor = executorType.getConstructors()[0];
        return (EventHandler) executorConstructor.newInstance(owner, eventType, receiveCancelled, order);
    }

    /**
     * Generate a unique name for the class, used by asm generate handlers
     */
    private String getUniqueName(String ownerName, String handlerName, String eventName) {
        return "AsmEventBus_" + this.hashCode() + "_" + ownerName + "_" + handlerName + "_" + eventName + "_" + (uniqueId++);
    }

    private class ExecutorImpl implements EventBus.Executor {
        final Class<? extends Event> eventType;
        final ExecutorImpl parent;
        final Map<Order, Collection<EventHandler>> orderedExecutors;

        ExecutorImpl(ExecutorImpl parent, Class<? extends Event> eventType, Map<Order, Collection<EventHandler>> orderedExecutors) {
            this.parent = parent;
            this.orderedExecutors = orderedExecutors;
            this.eventType = eventType;
        }

        void add(EventHandler handler) {
            orderedExecutors.computeIfAbsent(handler.getOrder(), k -> new LinkedHashSet<>()).add(handler);
        }

        @Override
        public boolean execute(@NonNull Event event) {
            if (parent != null) parent.execute(event);

            if (orderedExecutors == null || orderedExecutors.size() == 0)
                return false;
            if (event.isCancellable()) {
                Cancellable cancellable = (Cancellable) event;

                for (Order order : Order.values()) {
                    Collection<EventHandler> executors = orderedExecutors.get(order);
                    if (executors == null)
                        continue;
                    for (EventHandler handler : executors)
                        if (!cancellable.isCancelled() || handler.isReceiveCancelled())
                            exec(handler, event);
                }
                return cancellable.isCancelled();
            } else {
                for (Order order : Order.values()) {
                    Collection<EventHandler> executors = orderedExecutors.get(order);
                    if (executors == null)
                        continue;
                    for (EventHandler handler : executors)
                        exec(handler, event);
                }
                return false;
            }
        }

        @Override
        public Class<? extends Event> getEventType() {
            return eventType;
        }
    }

}
