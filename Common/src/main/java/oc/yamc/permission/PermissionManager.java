package oc.yamc.permission;

public interface PermissionManager {
	
	boolean hasPermission(Permissable permissable, String permission);

	void setPermission(Permissable permissable, String permission, boolean value);
}