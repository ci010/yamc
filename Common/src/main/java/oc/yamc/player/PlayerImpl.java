package oc.yamc.player;

import oc.yamc.entity.Entity;
import oc.yamc.event.Cancellable;
import oc.yamc.event.Event;
import oc.yamc.math.Position;
import oc.yamc.world.Chunk;
import oc.yamc.world.World;

import javax.annotation.Nonnull;

public class PlayerImpl implements Player {
    private Profile profile;
    private World world;
    private Entity mounting;

    public PlayerImpl(Profile data, World world, Entity mounting) {
        this.profile = data;
        this.world = world;
        this.mounting = mounting;
    }

    public void enter(Chunk.Store chunkStore) {
        int radius = this.profile.trackingChunkRadius;
        Entity entity = this.getMountingEntity();
        Position pos = Position.of(entity.getPosition());
        for (int i = -radius; i <= radius; i++) {
            if (i <= 0) {
                int zOff = i + radius;
                for (int j = -zOff; j <= zOff; j++)
                    chunkStore.touchChunk(pos.add(i * 16, 0, j * 16));
            } else {
                int zOff = radius - i;
                for (int j = -zOff; j <= zOff; j++)
                    chunkStore.touchChunk(pos.add(i * 16, 0, j * 16));
            }
        }
    }

    public void update(Chunk chunk) {

    }

    @Nonnull
    @Override
    public Entity getMountingEntity() {
        return mounting;
    }

    @Nonnull
    @Override
    public Entity mountEntity(Entity entity) {
        Entity old = mounting;
        if (entity == null) {
            // create phantom entity if is god, else create default player
        }
        mounting = entity;
        // fire event;
        return old;
    }

    @Nonnull
    @Override
    public Player.Profile getProfile() {
        return profile;
    }

    @Nonnull
    @Override
    public World getWorld() {
        return world;
    }

    public static class PlayerPlaceBlockEvent implements Event, Cancellable {
        public final PlayerImpl player;
        public final Position position;
        private boolean cancelled;

        public PlayerPlaceBlockEvent(PlayerImpl player, Position position) {
            this.player = player;
            this.position = position;
        }

        @Override
        public boolean isCancelled() {
            return cancelled;
        }

        @Override
        public void setCancelled(boolean cancelled) {
            this.cancelled = cancelled;
        }
    }
}
