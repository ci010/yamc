package oc.yamc.math;

import org.joml.AABBd;
import org.joml.Vector3f;

import java.util.ArrayList;
import java.util.List;

public class AABBs {
    public static AABBd translate(AABBd aabBd, Vector3f translate) {
        return translate(aabBd, translate, aabBd);
    }

    public static AABBd translate(AABBd aabBd, Vector3f translate, AABBd result) {
        result.minX = aabBd.minX + translate.x;
        result.minY = aabBd.minY + translate.y;
        result.minZ = aabBd.minZ + translate.z;

        result.maxX = aabBd.maxX + translate.x;
        result.maxY = aabBd.maxY + translate.y;
        result.maxZ = aabBd.maxZ + translate.z;
        return result;
    }

    public static AABBd translate(AABBd aabBd, float x, float y, float z, AABBd result) {
        result.minX = aabBd.minX + x;
        result.maxX = aabBd.maxX + x;
        result.minY = aabBd.minY + y;
        result.maxY = aabBd.maxY + y;
        result.minZ = aabBd.minZ + z;
        result.maxZ = aabBd.maxZ + z;
        return result;
    }



    @SuppressWarnings("unchecked")
    public static List<Position>[] around(AABBd aabb, Vector3f movement) {
        List<Position>[] offsets = new List[3];
        for (int i = 0; i < offsets.length; i++) {
            offsets[i] = new ArrayList<>();
        }
//        aabb = translate(aabb, movement, new AABBd());

        int directionX = movement.x == -0 ? 0 : Float.compare(movement.x, 0),
                directionY = movement.y == -0 ? 0 : Float.compare(movement.y, 0),
                directionZ = movement.z == -0 ? 0 : Float.compare(movement.z, 0);
        int minX = (int) Math.floor(aabb.minX),
                minY = (int) Math.floor(aabb.minY),
                minZ = (int) Math.floor(aabb.minZ),
                maxX = (int) Math.floor(aabb.maxX),
                maxY = (int) Math.floor(aabb.maxY),
                maxZ = (int) Math.floor(aabb.maxZ);

        if (directionX != 0) {
            int src = (int) Math.floor(directionX == -1 ? minX : directionX == 1 ? maxX : 0 + movement.x);
            for (int z = minZ; z <= maxZ; z++)
                for (int y = minY; y <= maxY; y++)
                    offsets[0].add(new Position(src, y, z));
        }
        if (directionY != 0) {
            int src = (int) Math.floor(directionY == -1 ? minY : directionY == 1 ? maxY : 0 + movement.y);
            for (int z = minZ; z <= maxZ; z++)
                for (int x = minX; x <= maxX; x++)
                    offsets[1].add(new Position(x, src, z));
        }
        if (directionZ != 0) {
            int src = (int) Math.floor(directionZ == -1 ? minZ : directionZ == 1 ? maxZ : 0 + movement.z);
            for (int y = minY; y <= maxY; y++)
                for (int x = minX; x <= maxX; x++)
                    offsets[2].add(new Position(x, y, src));
        }
        return offsets;
    }
}
