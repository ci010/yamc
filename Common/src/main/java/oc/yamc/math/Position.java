package oc.yamc.math;

import lombok.Data;
import org.joml.Vector3f;

/**
 * World coord position in int
 */
@Data
public final class Position {
    public static final Position ZERO = new Position(0, 0, 0);

    private final int x, y, z;

    public Position(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Position add(int x, int y, int z) {
        return new Position(this.x + x, this.y + y, this.z + z);
    }

    public Position minus(int x, int y, int z) {
        return new Position(this.x - x, this.y - y, this.z - z);
    }

    public static long getChunkXZ(int x, int z) {
        return (long) (x >> 4) & 4294967295L | ((long) (z >> 4) & 4294967295L) << 32;
    }

    public int sqDistanceBetween(Position another) {
        int x = this.x - another.x;
        int y = this.y - another.y;
        int z = this.z - another.z;
        return x * x + y * y + z * z;
    }

    public static Position of(int x, int y, int z) {
        return new Position(x, y, z);
    }

    public static Position of(Vector3f vector3f) {
        return new Position((int) Math.floor(vector3f.x), (int) Math.floor(vector3f.y), (int) Math.floor(vector3f.z));
    }

    public static boolean inSameChunk(Position a, Position b) {
        return ((a.x >> 4) == (b.x >> 4)) && ((a.y >> 4) == (b.y >> 4)) && ((a.z >> 4) == (b.z >> 4));
    }

    public long getChunkXZ() {
        return (long) (x >> 4) & 4294967295L | ((long) (z >> 4) & 4294967295L) << 32;
    }

    public int getChunkX() {
        return x >> 4;
    }

    public int getChunkY() {
        return y >> 4;
    }

    public int getChunkZ() {
        return z >> 4;
    }

    public int pack() {
        return ((x & 0xF) << 8) | ((y & 0xF) << 4) | (z & 0xF);
    }
}
