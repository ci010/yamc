package oc.yamc.math;

import org.joml.Math;
import org.joml.Matrix4f;
import org.joml.Vector3f;

/**
 * Represent a ellipse by the
 * <a href="https://www.mathopenref.com/coordparamellipse.html">Parametric Equation of an Ellipse</a>
 */
public class Ellipse {
    private float width, height;

    private Matrix4f invTransform;

    /**
     * @param position The world position of the ellipse center
     * @param normal   The normal direction of the ellipse
     * @param width    The width of ellipse which shapes x axis
     * @param height   The height of ellipse which shapes y axis
     */
    public Ellipse(Vector3f position, Vector3f normal, float width, float height) {
        this.width = width;
        this.height = height;
        this.invTransform = new Matrix4f().lookAt(position, position.add(normal), new Vector3f(0, 1, 0)).invert();
    }

    /**
     * Get the position of a object rotate along this ellipse.
     *
     * @param period The time period, from 0.0 to 1.0
     * @return The world position
     */
    public Vector3f get(float period) {
        double rad = 2 * Math.PI * period;
        double x = width * Math.cos(rad),
                y = height * Math.sin(rad);
        var local = new Vector3f((float) x, (float) y, 0);
        return invTransform.transformPosition(local);
    }

}
