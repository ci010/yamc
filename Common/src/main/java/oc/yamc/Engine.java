package oc.yamc;

import oc.yamc.event.EventBus;
import oc.yamc.game.Game;
import oc.yamc.mod.ModRepository;
import oc.yamc.mod.ModStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * really, just the {@link Game} starter, nothing else
 */
public interface Engine {
    Logger LOGGER = LoggerFactory.getLogger("Engine");

    static Engine instance() {return Stub.engine;}

    static Logger getLogger() { return LOGGER; }

    static Game game() { return Stub.engine.getCurrentGame(); }

    static Side side() { return Stub.engine.getSide(); }

    static ModRepository modRepository() { return Stub.engine.getModRepository(); }

    static ModStore modStore() { return Stub.engine.getModStore(); }

    static EventBus bus() { return Stub.engine.getEventBus(); }

    Side getSide();

    /**
     * Start a new game, each oc.yamc only support one game at the time?
     *
     * @param option The game option
     */
    Game startGame(Game.Option option);

    ModRepository getModRepository();

    ModStore getModStore();

    Game getCurrentGame();

    EventBus getEventBus();

    enum Side {
        SERVER, CLIENT
    }
}
