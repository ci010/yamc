package oc.yamc;

public interface Tickable {
    void tick();

    interface Partial {
        void tick(double partial);
    }
}
