package oc.yamc.entity;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.systems.IteratingSystem;

public class SystemMotion extends IteratingSystem {
    private ComponentMapper<ComponentTransform> positionMapper;
    private ComponentMapper<ComponentVelocity> velocityMapper;

    /**
     * Creates a new EntityProcessingSystem.
     */
    public SystemMotion() {
        super(Aspect.all(ComponentTransform.class, ComponentVelocity.class));
    }

    @Override
    protected void process(int entityId) {
        ComponentTransform positionComponent = positionMapper.get(entityId);
        ComponentVelocity velocityComponent = velocityMapper.get(entityId);
        positionComponent.posX += velocityComponent.x;
        positionComponent.posY += velocityComponent.y;
        positionComponent.posZ += velocityComponent.z;
    }
}
