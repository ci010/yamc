package oc.yamc.entity;

import oc.yamc.Prototype;
import oc.yamc.registry.RegistryEntry;
import oc.yamc.world.World;

public interface EntityType extends Prototype<Entity, World>, RegistryEntry<EntityType> {
}
