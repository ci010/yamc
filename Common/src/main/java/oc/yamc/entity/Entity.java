package oc.yamc.entity;

import oc.yamc.RuntimeObject;
import oc.yamc.Tickable;
import oc.yamc.item.Item;
import oc.yamc.player.Player;
import oc.yamc.util.Owner;
import oc.yamc.world.World;
import org.joml.AABBd;
import org.joml.Vector3f;

import javax.annotation.Nullable;

@Owner(World.class)
public interface Entity extends RuntimeObject, Tickable {
    int getId();

    Vector3f getPosition();

    Vector3f getRotation();

    Vector3f getMotion();

    AABBd getBoundingBox();

    void destroy();

    @Nullable
    Player getMountedPlayer();

    interface TwoHands {
        Item getMainHand();

        void setMainHand(Item mainHand);

        Item getOffHand();

        void setOffHand(Item offHand);
    }
}
