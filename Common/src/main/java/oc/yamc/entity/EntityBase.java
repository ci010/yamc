package oc.yamc.entity;

import com.google.common.collect.ImmutableMap;
import lombok.Getter;
import lombok.Setter;
import oc.yamc.item.Item;
import oc.yamc.player.Player;
import org.joml.AABBd;
import org.joml.Vector3f;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class EntityBase implements Entity {
    @Getter
    private int id;
    @Getter
    private Vector3f position, rotation, motion;
    @Getter
    private AABBd boundingBox;
    private ImmutableMap<String, Object> behaviors;

    @Getter
    @Setter
    private Player mountedPlayer;

    public EntityBase(int id, Vector3f position, Vector3f rotation, Vector3f motion, AABBd boundingBox, ImmutableMap<String, Object> behaviors) {
        this.id = id;
        this.position = position;
        this.rotation = rotation;
        this.motion = motion;
        this.boundingBox = boundingBox;
        this.behaviors = behaviors;
    }

    @Override
    public void tick() {
    }

    @Override
    public void destroy() {
    }

    @Nullable
    @Override
    public <T> T getComponent(@Nonnull String name) {
        return null;
    }

    @Nullable
    @Override
    public <T> T getComponent(@Nonnull Class<T> type) {
        return null;
    }

    @Nullable
    @Override
    public <T> T getBehavior(Class<T> type) {
        return (T) behaviors.get(type.getName());
    }

    public static class TwoHandImpl implements TwoHands {
        private Item mainHand, offHand;

        public Item getMainHand() {
            return mainHand;
        }

        @Override
        public void setMainHand(Item mainHand) {
            this.mainHand = mainHand;
        }

        @Override
        public Item getOffHand() {
            return offHand;
        }

        @Override
        public void setOffHand(Item offHand) {
            this.offHand = offHand;
        }
    }
}
