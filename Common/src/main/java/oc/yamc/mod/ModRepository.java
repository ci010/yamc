package oc.yamc.mod;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import oc.yamc.Engine;
import oc.yamc.util.Owner;

import java.io.InputStream;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * The repo of the mod, can be maven, ftp or any other form
 */
@Owner(Engine.class)
public interface ModRepository {
    Collection<RemoteModMetadata> fetchIndex();

    InputStream open(ModIdentifier identifier);

    boolean contains(ModIdentifier identifier);

    @Data
    @lombok.Builder
    class RemoteModMetadata {
        @NonNull
        private ModMetadata modMetadata;
        private String sha256;
        private long size;
    }
}
