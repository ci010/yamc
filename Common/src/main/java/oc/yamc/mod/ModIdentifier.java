package oc.yamc.mod;

import lombok.*;

import java.net.URL;

@Data(staticConstructor = "of")
public class ModIdentifier {
    @NonNull
    private final String group, id, version;

    public static ModIdentifier from(@NonNull String s) {
        String[] split = s.split(":");
        if (split.length != 3 || split[0].equals("") || split[1].equals("") || split[2].equals(""))
            throw new IllegalArgumentException("Invalid mod identifier syntax: " + s);
        return new ModIdentifier(split[0], split[1], split[2]);
    }

    @Override
    public String toString() {
        return group + ":" + id + ":" + version;
    }

    public URL toURL() {
        return null; // TODO implement this
    }
}
