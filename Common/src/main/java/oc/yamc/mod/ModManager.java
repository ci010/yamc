package oc.yamc.mod;

import lombok.NonNull;
import oc.yamc.game.Game;
import oc.yamc.util.Owner;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;

/**
 * The in-game mod manager. Not the one who manage the mod sources.
 */
@Owner(Game.class)
public interface ModManager {
    /**
     * Find the mod by modid.
     *
     * @return The mod container, might be null
     */
    @Nullable
    ModContainer findMod(@NonNull String modId);

    /**
     * Find the mod by its type.
     */
    @Nullable
    ModContainer findMod(@NonNull Class<?> clazz);

    /**
     * Is the mod loaded.
     */
    boolean isModLoaded(@NonNull String modId);

    /**
     * Get the owner mod of this class. If we cannot find any mod own this class. We will return {@link ModContainer#UNKNOWN}.
     *
     * @param clazz The class
     * @return The mod owner
     * @see ModContainer#UNKNOWN
     */
    ModContainer findModByClass(@NonNull Class<?> clazz);

    @Nonnull
    Collection<ModContainer> getLoadedMods();
}
