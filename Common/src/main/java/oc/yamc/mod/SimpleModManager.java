package oc.yamc.mod;

import lombok.NonNull;
import oc.yamc.mod.java.ModClassLoader;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;

public class SimpleModManager implements ModManager {
    private Map<String, ModContainer> idToMods;
    private Map<Class, ModContainer> typeToMods;

    public SimpleModManager(Map<String, ModContainer> idToMods, Map<Class, ModContainer> typeToMods) {
        this.idToMods = idToMods;
        this.typeToMods = typeToMods;
    }

    @Override
    public ModContainer findMod(@NonNull String modId) {
        return idToMods.get(Objects.requireNonNull(modId));
    }

    @Override
    public ModContainer findMod(@NonNull Class<?> clazz) {
        return typeToMods.get(Objects.requireNonNull(clazz));
    }

    @Override
    public boolean isModLoaded(@NonNull String modId) {
        return idToMods.containsKey(Objects.requireNonNull(modId));
    }

    @Override
    public ModContainer findModByClass(@NonNull Class<?> clazz) {
        ClassLoader loader = clazz.getClassLoader();
        if (loader instanceof ModClassLoader) {
            String id = ((ModClassLoader) loader).getIdentifier();
            return idToMods.get(id);
        }
        if (clazz.getName().startsWith("oc.minecraft")) {
            return idToMods.get("minecraft");
        }
        return ModContainer.UNKNOWN;
    }

    @Nonnull
    @Override
    public Collection<ModContainer> getLoadedMods() {
        return idToMods.values();
    }
}
