package oc.yamc.mod;

import oc.yamc.resource.ResourceSource;
import oc.yamc.util.Owner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The runtime mod container of the mod.
 */
@Owner(ModManager.class)
public interface ModContainer {
    ModContainer UNKNOWN = new ModContainerBuiltin(ModMetadata.builder().identifier(ModIdentifier.of("", "", "")).build(), LoggerFactory.getLogger("Unknown"), new Object(), null);

    String getModId();

    /**
     * The instance of the class which is annotated by {@link Mod}
     *
     * @return The instance
     * @see Mod
     */
    Object getInstance();

    Logger getLogger();

    ModMetadata getMetadata();

    ResourceSource getResourceSource();
}
