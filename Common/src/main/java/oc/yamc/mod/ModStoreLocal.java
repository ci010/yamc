package oc.yamc.mod;

import com.google.common.collect.Maps;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.OpenOption;
import java.nio.file.StandardOpenOption;
import java.util.Map;
import java.util.Objects;
import javax.annotation.Nonnull;

import javax.annotation.Nullable;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;


public class ModStoreLocal implements ModStore {
    /**
     * Since we have already decide all the mod will be downloaded to local.
     * <p>The loader could confirm that it can only load from disk</p>
     * <p>Otherwise, we will have a much pure api: (InputStream)->ModContainer</p>
     */
    private final Path root;

    private final Map<ModIdentifier, ModMetadata> cachedMetaData = Maps.newHashMap();

    public ModStoreLocal(Path root) {
        this.root = root;
    }

    /**
     * There might be different structure to store mod.
     */
    protected Path resolve(ModIdentifier identifier) {
        return root.resolve(identifier.getGroup()).resolve(identifier.getId() + "-" + identifier.getVersion() + ".jar");
    }

    @Override
    public boolean exists(@Nonnull ModIdentifier identifier) {
        Path source = resolve(Objects.requireNonNull(identifier));
        return Files.exists(source);
    }

    @Override
    public void store(@Nonnull ModIdentifier identifier, InputStream stream) {
        Path resolve = resolve(identifier);
//        try {
//            long size = identifier instanceof ModRepository.RemoteModMetadata ? ((ModRepository.RemoteModMetadata) identifier).getSize() : -1;
//            Transfer transfer = new Transfer(Channels.newChannel(stream), FileChannel.open(resolve, StandardOpenOption.WRITE), size);
//            transfer.call(); // TODO: monitor transfer
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    @Nullable
    @Override
    public ModMetadata metadata(@Nonnull ModIdentifier identifier) throws IOException {
        var metadata = cachedMetaData.get(identifier);
        if (metadata != null) return metadata;
        if (exists(identifier)) return null;
        Path resolve = resolve(identifier);
        try (var stream = new FileInputStream(resolve.toFile())) {
            ModMetadata.fromJsonStream(stream);
        }

        return null;
    }

    @Override
    public Path path(ModIdentifier identifier) {
        return resolve(identifier);
    }
}
