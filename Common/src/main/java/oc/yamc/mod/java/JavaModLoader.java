package oc.yamc.mod.java;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.JsonParser;
import oc.yamc.Engine;
import oc.yamc.mod.*;
import oc.yamc.mod.java.harvester.HarvestedAnnotation;
import oc.yamc.mod.java.harvester.HarvestedInfo;
import oc.yamc.resource.source.ResourceSourceZip;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Path;
import java.util.*;
import java.util.jar.JarFile;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;

/**
 * intermediate loader class
 */
public class JavaModLoader implements ModLoader {
    private Map<String, ModContainerJava> cache = Maps.newHashMap();
    private ModStore store;

    public JavaModLoader(ModStore st) {
        store = st;
    }

    private ModContainer load(ModIdentifier identifier) {
        if (!store.exists(identifier))
            return null;
        Path source = store.path(Objects.requireNonNull(identifier));
        try {
            JarFile jarFile = new JarFile(source.toFile());
            ZipEntry entry = jarFile.getEntry("metadata.json");
            if (entry == null) {
                Engine.getLogger().warn("metadata.json isn't exists. Path: " + source.toAbsolutePath().toString());
                return null;
            }
            ModMetadata metadata;
            try (InputStream stream = jarFile.getInputStream(entry)) {
                metadata = ModMetadata.fromJson(new JsonParser().parse(new InputStreamReader(stream)).getAsJsonObject());
            }

            Logger log = LoggerFactory.getLogger(metadata.getIdentifier().getId());
            ModClassLoader classLoader = new ModClassLoader(metadata.getIdentifier().getId(), log, source,
                    Thread.currentThread().getContextClassLoader());

            metadata.getDependency().stream().map(ModDependencyEntry::create).forEach(d -> {
                if (d.getLoadOrder() == ModDependencyEntry.LoadOrder.REQUIRED) {
                    ModContainerJava dep = cache.get(d.getModId());
                    if (dep == null) {
                        throw new IllegalStateException("");
                    }
                    classLoader.getDependencyClassLoaders().add(dep.getClassLoader());
                }
            });

            HarvestedInfo info = HarvestedInfo.harvest(jarFile);
            Collection<HarvestedAnnotation> modAnnos = info.getHarvestedAnnotations(Mod.class);
            if (modAnnos.isEmpty()) {
                Engine.getLogger().warn(String.format("cannot find the main class for mod %s!", metadata.getIdentifier().getId()));
                jarFile.close();
                classLoader.close();
                return null;
            }

            Class<?> mainClass = Class.forName(
                    modAnnos.toArray(new HarvestedAnnotation[0])[0].getOwnerType().getClassName(), true,
                    classLoader);
            Object instance = mainClass.getDeclaredConstructor().newInstance();

            ModContainerJava container = new ModContainerJava(classLoader, metadata, info, instance,
                    new ResourceSourceZip(jarFile));
            cache.put(identifier.getId(), container);

            return container;
        } catch (IOException e) {
            Engine.getLogger().warn(String.format("cannot load mod %s!", source.toString()), e);
        } catch (ClassNotFoundException e) {
            Engine.getLogger().warn(String.format("cannot find the main class for mod %s!", source.toString()), e);
        } catch (IllegalAccessException | InstantiationException e) {
            Engine.getLogger().warn(String.format("cannot instantiate the main class for mod %s!", source.toString()),
                    e);
        } catch (NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<ModContainer> loadAll(List<ModIdentifier> identifiers) {
        List<ModMetadata> mods = Lists.newArrayList();
        for (ModIdentifier mod : identifiers) {
            try {
                mods.add(store.metadata(mod));
            } catch (IOException e) {
                e.printStackTrace();
                Engine.getLogger().warn("Cannot find mod " + mod + "! Skipped. ");
            }
        }
        Map<ModIdentifier, ModDependencyEntry[]> map = mods.stream()
                .map(m -> new AbstractMap.SimpleImmutableEntry<>(m,
                        m.getDependency().stream().map(ModDependencyEntry::create).toArray(ModDependencyEntry[]::new)))
                .collect(Collectors.toMap(k -> k.getKey().getIdentifier(),
                        AbstractMap.SimpleImmutableEntry::getValue));
        return identifiers.stream().sorted((a, b) -> {
            return 0;
//            ModDependencyEntry[] entriesA = map.get(a);
//            ModDependencyEntry[] entriesB = map.get(b);
//
//            boolean aReqB = dependOn(entriesA, b);
//            boolean bReqA = dependOn(entriesB, a);
//
//            if (aReqB && bReqA) return 0;
//            return 0;
        }).map(this::load).collect(Collectors.toList());
    }
}
