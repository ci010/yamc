package oc.yamc.mod.java;

import oc.yamc.mod.ModContainer;
import oc.yamc.mod.ModMetadata;
import oc.yamc.mod.java.harvester.HarvestedInfo;
import oc.yamc.resource.ResourceSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ModContainerJava implements ModContainer {
    private final Logger logger;
    private final ModClassLoader classLoader;
    private final Object instance;
    private final ModMetadata metadata;
    private final HarvestedInfo harvestedInfo;
    private final ResourceSource resourceSource;

    ModContainerJava(ModClassLoader classLoader, ModMetadata metadata, HarvestedInfo harvestedInfo, Object instance, ResourceSource resourceSource) {
        this.classLoader = classLoader;
        this.metadata = metadata;
        this.harvestedInfo = harvestedInfo;
        this.instance = instance;
        this.resourceSource = resourceSource;
        this.logger = LoggerFactory.getLogger(getModId());
    }

    @Override
    public String getModId() {
        return metadata.getIdentifier().getId();
    }

    @Override
    public Object getInstance() {
        return instance;
    }

    @Override
    public Logger getLogger() {
        return logger;
    }

    @Override
    public ModMetadata getMetadata() {
        return metadata;
    }

    @Override
    public ResourceSource getResourceSource() {
        return resourceSource;
    }

    public ClassLoader getClassLoader() {
        return classLoader;
    }

    public HarvestedInfo getHarvestedInfo() {
        return harvestedInfo;
    }
}
