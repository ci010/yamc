package oc.yamc.mod;

import java.util.List;

public interface ModLoader {
    List<ModContainer> loadAll(List<ModIdentifier> identifiers);
}
