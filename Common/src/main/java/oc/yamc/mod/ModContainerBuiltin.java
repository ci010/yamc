package oc.yamc.mod;

import oc.yamc.resource.ResourceSource;
import org.slf4j.Logger;

public class ModContainerBuiltin implements ModContainer {
    private ModMetadata metadata;
    private Logger logger;
    private Object instance;
    private ResourceSource source;

    public ModContainerBuiltin(ModMetadata metadata, Logger logger, Object instance, ResourceSource source) {
        this.metadata = metadata;
        this.logger = logger;
        this.instance = instance;
        this.source = source;
    }

    @Override
    public String getModId() {
        return metadata.getIdentifier().getId();
    }

    @Override
    public Object getInstance() {
        return instance;
    }

    @Override
    public Logger getLogger() {
        return logger;
    }

    @Override
    public ModMetadata getMetadata() {
        return metadata;
    }

    @Override
    public ResourceSource getResourceSource() {
        return source;
    }
}
