package oc.yamc.mod;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Just a wrapper contains builtin mod.
 */
public class ModLoaderWrapper implements ModLoader {
    private List<ModContainer> builtin = new LinkedList<>();
    private ModLoader loader;

    public ModLoaderWrapper(ModLoader loader) {
        this.loader = loader;
    }

    public ModLoaderWrapper addBuiltin(ModContainer l) {
        builtin.add(l);
        return this;
    }

    @Override
    public List<ModContainer> loadAll(List<ModIdentifier> identifiers) {
        ArrayList<ModContainer> modContainers = Lists.newArrayList(builtin);
        modContainers.addAll(loader.loadAll(identifiers));
        return modContainers;
    }
}