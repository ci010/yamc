package oc.yamc.mod;

import lombok.Value;
import oc.yamc.util.versioning.InvalidVersionSpecificationException;
import oc.yamc.util.versioning.VersionRange;

@Value
public class ModDependencyEntry {

    public enum LoadOrder {
        /**
         * Mod must be installed, and after it load your mod.
         */
        REQUIRED,
        /**
         * Mod needn't be installed, but after it load your mod.
         */
        AFTER,
        /**
         * Mod needn't be installed, but before it load your mod.
         */
        BEFORE
    }

    public static ModDependencyEntry create(String spec) {
        String[] args = spec.split(":", 4);
        if (args.length < 4)
            throw new ModDependencyException("Failed to create dependency entry. Source: " + spec);

        try {
            LoadOrder loadOrder = LoadOrder.valueOf(args[0].toUpperCase());
            String group = args[1], id = args[2];
            VersionRange range = VersionRange.createFromVersionSpec(args[2]);
            return new ModDependencyEntry(loadOrder, group, id, range);
        } catch (InvalidVersionSpecificationException e) {
            throw new ModDependencyException("Failed to create dependency entry, invalid version range. Range: " + args[2], e);
        } catch (IllegalArgumentException e) {
            throw new ModDependencyException("Failed to create dependency entry, illegal load order. Load order: " + args[0], e);
        }
    }

    private final LoadOrder loadOrder;
    private final String group, modId;
    private final VersionRange versionRange;
}
