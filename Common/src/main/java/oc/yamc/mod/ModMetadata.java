package oc.yamc.mod;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;


@Data
@lombok.Builder
public class ModMetadata {
    @NonNull
    private ModIdentifier identifier;
    @NonNull
    @Builder.Default
    private String name = "", description = "", url = "";
    @NonNull
    @Builder.Default
    private List<String> authors = Collections.emptyList();
    @NonNull
    @Builder.Default
    private String logo = ""; // base 64 logo
    @NonNull
    @Builder.Default
    private List<String> dependency = Collections.emptyList();
    @NonNull
    @Builder.Default
    private Map<String, Object> properties = Collections.emptyMap();

    public static ModMetadata fromJsonStream(InputStream stream) {
        return fromJson(new JsonParser().parse(new InputStreamReader(stream)).getAsJsonObject());
    }

    public static ModMetadata fromJson(JsonObject jo) {
        String modid = "", name = "", url = "", description = "", logoFile = "";
        List<String> authors = Collections.emptyList();
        Map<String, Object> properties = Collections.emptyMap();
        String version = "";
        // TODO make default and validate id & version in metadata

        if (jo.has("id")) {
            modid = jo.get("id").getAsString();
        }
        if (jo.has("name")) {
            name = jo.get("name").getAsString();
        }
        if (jo.has("version")) {
            version = jo.get("version").getAsString();
        }
        if (jo.has("description")) {
            description = jo.get("description").getAsString();
        }
        if (jo.has("url")) {
            url = jo.get("url").getAsString();
        }
        if (jo.has("logo")) {
            logoFile = jo.get("logo").getAsString();
        }
        if (jo.has("authors")) {
            authors = new ArrayList<>();
            for (JsonElement je : jo.getAsJsonArray("authors")) {
                if (je.isJsonPrimitive()) {
                    authors.add(je.getAsString());
                }
            }
            authors = ImmutableList.copyOf(authors);
        }
        if (jo.has("properties")) {
            properties = new HashMap<>();
            JsonObject jProperties = jo.getAsJsonObject("properties");
            for (Map.Entry<String, JsonElement> entry : jProperties.entrySet()) {
                JsonElement value0 = entry.getValue();
                if (value0.isJsonPrimitive()) {
                    JsonPrimitive value = value0.getAsJsonPrimitive();
                    if (value.isString())
                        properties.put(entry.getKey(), value.getAsString());
                    else if (value.isNumber())
                        properties.put(entry.getKey(), value.getAsNumber());
                    else if (value.isBoolean())
                        properties.put(entry.getKey(), value.getAsBoolean());
                }
            }
            properties = ImmutableMap.copyOf(properties);
        }
        return ModMetadata.builder()
                .identifier(ModIdentifier.of("", modid, version))
                .name(name)
                .description(description).url(url)
                .authors(authors)
                .properties(properties).build();
    }
}
