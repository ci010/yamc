package oc.yamc.game;

import oc.yamc.event.Event;
import oc.yamc.registry.RegistryManager;

public class RegisterEvent implements Event {
    private RegistryManager.Mutable registry;

    public RegisterEvent(RegistryManager.Mutable registry) {
        this.registry = registry;
    }

    public RegistryManager.Mutable getRegistry() {
        return registry;
    }
}
