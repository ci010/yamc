package oc.yamc.game;

import oc.yamc.GameContext;
import oc.yamc.event.Event;

public class GameReadyEvent implements Event {
    private GameContext context;

    public GameReadyEvent(GameContext context) {
        this.context = context;
    }

    public GameContext getContext() {
        return context;
    }
}
