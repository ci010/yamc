package oc.yamc.game;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import oc.yamc.event.EventBus;
import oc.yamc.math.FixStepTicker;
import oc.yamc.mod.ModRepository;
import oc.yamc.mod.ModStore;
import oc.yamc.world.ChunkStore;
import oc.yamc.world.World;
import oc.yamc.world.WorldCommon;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Each world host in an independent thread.
 */
public class GameServerFullAsync extends GameBase {
    protected Map<String, World> worlds;
    protected List<FixStepTicker> internalTicker;
    protected List<Thread> worldThreads;
    protected boolean terminated = false;

    public GameServerFullAsync(Option option, ModRepository repository, ModStore store, EventBus bus) {
        super(option, repository, store, bus);
        worlds = Maps.newTreeMap();
        internalTicker = Lists.newArrayList();
        worldThreads = Lists.newArrayList();
    }

    @Override
    public World spawnWorld(World.Config config) {
        if (config == null) {
            WorldCommon w = new WorldCommon(context, new ChunkStore(context), null);
            this.worlds.put("default", w);
            FixStepTicker ticker = new FixStepTicker(w::tick, 20);
            this.internalTicker.add(ticker);
            Thread thread = new Thread(ticker);
            thread.setName("World Thread: default");
            this.worldThreads.add(thread);
            return w;
        }
        return null;
    }


    public Collection<World> getWorlds() {
        return worlds.values();
    }

    @Nullable
    public World getWorld(String name) {
        return worlds.get(name);
    }

    @Override
    public void run() {
        super.run();
        for (Thread thread : this.worldThreads) {
            thread.start();
        }
    }

    @Override
    public boolean isTerminated() {
        return terminated;
    }

    @Override
    public void terminate() {
        for (FixStepTicker ticker : internalTicker) {
            ticker.stop();
        }
        // TODO: unload mod/resource here
        terminated = true;
    }
}