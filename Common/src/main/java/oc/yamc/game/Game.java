package oc.yamc.game;

import lombok.Getter;
import lombok.Setter;
import oc.yamc.GameContext;
import oc.yamc.Prototype;
import oc.yamc.RuntimeObject;
import oc.yamc.mod.ModIdentifier;
import oc.yamc.mod.ModManager;
import oc.yamc.resource.ResourcePath;
import oc.yamc.world.World;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * The game shares the same set of mod and resources pack manifest.
 * <p>
 * The game manages the complex life cycle of mods, configs, worlds.
 * </p>
 * <p>
 * I used to decide to construct all the dependency (mod, config) before game
 * instance creation.
 * </p>
 * <p>
 * But, its complexity makes me feel that it should manage all these by itself
 * (after it create)
 * </p>
 *
 * <p>
 * Each world should hold a separated thread
 */
public interface Game extends RuntimeObject, Prototype<RuntimeObject, Game>, Runnable {
    GameContext getContext();

    World spawnWorld(World.Config config);

    ModManager getModManager();

    boolean isTerminated();

    void terminate();

    interface Server extends Game {
        Collection<World> getWorlds();

        @Nullable
        World getWorld(String name);
    }

    class Option {
        @Getter
        @Setter
        private List<ModIdentifier> mods = Collections.emptyList();
        @Getter
        @Setter
        private List<ResourcePath> resources = Collections.emptyList();
    }


}
