package oc.yamc.game;

import lombok.Data;
import oc.yamc.event.Event;
import oc.yamc.event.EventBus;
import oc.yamc.registry.RegistryManager;
import oc.yamc.resource.ResourceManager;

/**
 * The resource setup event. It will be fired before the game start to setup resources
 */
@Data
public class ResourceSetupEvent implements Event {
    private final EventBus bus;
    private final RegistryManager registry;
    private final ResourceManager resourceManager;
}
