package oc.yamc.game;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import oc.yamc.Engine;
import oc.yamc.GameContext;
import oc.yamc.RuntimeObject;
import oc.yamc.entity.EntityType;
import oc.yamc.event.EventBus;
import oc.yamc.event.EventSource;
import oc.yamc.item.Item;
import oc.yamc.mod.*;
import oc.yamc.mod.java.JavaModLoader;
import oc.yamc.registry.*;
import oc.yamc.resource.ResourceSource;
import oc.yamc.world.Block;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public abstract class GameBase implements Game {
    protected final Option option;
    protected final EventBus bus;
    protected final ModRepository modRepository;
    protected final ModStore modStore;

    protected GameContext context;
    protected ModManager modManager;
    protected Queue<Runnable> nextTicks = new ConcurrentLinkedQueue<>();


    /**
     * self metadata
     */
    private ModMetadata meta = ModMetadata.builder().identifier(ModIdentifier.of("oc", "yamc", "0.0.1")).build();

    public GameBase(Option option, ModRepository repository, ModStore store, EventBus bus) {
        this.option = option;
        this.modStore = store;
        this.modRepository = repository;
        this.bus = bus;
    }

    @Override
    public ModManager getModManager() {
        return modManager;
    }

    @Override
    public GameContext getContext() {
        return context;
    }

    @Override
    public RuntimeObject createObject(GameContext gameContext, Game context) {
        return null;
    }

    @Nullable
    @Override
    public <T> T getComponent(@Nonnull String name) {
        return null;
    }

    @Nullable
    @Override
    public <T> T getComponent(@Nonnull Class<T> type) {
        return null;
    }

    @Nullable
    @Override
    public <T> T getBehavior(Class<T> type) {
        return null;
    }

    protected Stage createInitStage() { return new Stage(); }

    private void init() {
        var stage = createInitStage();
        stage.constructStage();
        stage.registerStage();
        stage.resourceStage();
        stage.finishStage();
    }

    @Override
    public void run() {
        init();
    }

    protected class Stage {
        /**
         * Construct stage, collect mod and resource according to it option
         */
        protected void constructStage() {
            ImmutableMap.Builder<String, ModContainer> idToMapBuilder = ImmutableMap.builder();
            ImmutableMap.Builder<Class, ModContainer> typeToMapBuilder = ImmutableMap.builder();
            ModLoaderWrapper loader = new ModLoaderWrapper(new JavaModLoader(modStore));
            decorateLoader(loader);
            List<ModContainer> modContainers = loader.loadAll(option.getMods());
            for (ModContainer modContainer : modContainers) {
                idToMapBuilder.put(modContainer.getModId(), modContainer);
                typeToMapBuilder.put(modContainer.getInstance().getClass(), modContainer);
            }
            modManager = new SimpleModManager(idToMapBuilder.build(), typeToMapBuilder.build());
            for (ModContainer mod : modManager.getLoadedMods())
                bus.register(mod.getInstance());
        }

        protected void decorateLoader(ModLoaderWrapper wrapper) {
            wrapper.addBuiltin(new ModContainerBuiltin(meta, Engine.getLogger(), GameBase.this, ResourceSource.empty()));
        }

        /**
         * Register stage, collect all registerable things from mod here.
         */
        protected void registerStage() {
            Registry.Type[] tps = new Registry.Type[]{ Registry.Type.of("action", EventSource.class), Registry.Type.of("block", Block.class),
                    Registry.Type.of("item", Item.class), Registry.Type.of("entity", EntityType.class) };
            Map<Class<?>, Registry<?>> maps = Maps.newHashMap();
            List<MutableRegistry<?>> registries = Lists.newArrayList();
            for (Registry.Type<?> tp : tps) {
                MutableRegistry<?> registry = new MutableRegistry<>(tp.type, tp.name);
                maps.put(tp.type, registry);
                registries.add(registry);
            }
            MutableRegistryManager manager = new MutableRegistryManager(maps);
            bus.post(new RegisterEvent(manager));
            context = new GameContext(manager, bus, nextTicks);
        }

        /**
         * let mod and resource related module load resources.
         */
        protected void resourceStage() {
            bus.post(new ResourceSetupEvent(bus, context.getRegistry(), null));
        }

        /**
         * final stage of the
         */
        protected void finishStage() {
            ImmutableMap.Builder<Class<?>, ImmutableRegistry<?>> builder = ImmutableMap.builder();
            for (Map.Entry<Class<?>, Registry<?>> entry : context.getRegistry().getEntries())
                builder.put(entry.getKey(), ImmutableRegistry.freeze(entry.getValue()));
            context = new GameContext(new FrozenRegistryManager(builder.build()), bus, nextTicks);

            spawnWorld(null);

            bus.post(new GameReadyEvent(context));
        }
    }
}
