package oc.yamc.item;

class ItemStack extends ItemBase implements Item.Stack {
    ItemStack(ItemType.UseBlockBehavior useBlockBehavior, ItemType.HitBlockBehavior hitBlockBehavior, ItemType.UseBehavior useBehavior) {
        super(useBlockBehavior, hitBlockBehavior, useBehavior);
    }

    private int quantity;
    private int maxQuantity;

    @Override
    public boolean isEmpty() {
        return quantity == 0;
    }

    @Override
    public int getQuantity() {
        return quantity;
    }

    @Override
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public int getMaxQuantity() {
        return maxQuantity;
    }

    @Override
    public Stack split(int quantity) {
        var item = new ItemStack(this.useBlockBehavior, this.hitBlockBehavior, this.useBehavior);
        item.setQuantity(quantity);
        this.quantity -= quantity;
        return item;
    }
}
