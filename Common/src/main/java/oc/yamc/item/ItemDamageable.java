package oc.yamc.item;

class ItemDamageable extends ItemBase implements Item.Damageable {
    ItemDamageable(ItemType.UseBlockBehavior useBlockBehavior, ItemType.HitBlockBehavior hitBlockBehavior, ItemType.UseBehavior useBehavior) {
        super(useBlockBehavior, hitBlockBehavior, useBehavior);
    }

    private float damage;

    @Override
    public float getDamage() {
        return damage;
    }

    @Override
    public void setDamage(float damage) {
        this.damage = damage;
    }
}
