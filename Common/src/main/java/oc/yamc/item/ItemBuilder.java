package oc.yamc.item;

import java.util.Objects;

public class ItemBuilder {
    private ItemType.UseBlockBehavior useBlockBehavior = ItemType.DEFAULT_USE_BLOCK;
    private ItemType.HitBlockBehavior hitBlockBehavior = ItemType.DEFAULT_HIT_BLOCK;
    private ItemType.UseBehavior useBehavior = ItemType.DEFAULT_USE;

    private String id;

    private ItemBuilder(String id) {
        this.id = id;
    }

    public static ItemBuilder create(String id) {
        Objects.requireNonNull(id);
        return new ItemBuilder(id);
    }

    public ItemBuilder setUseBlockBehavior(ItemType.UseBlockBehavior useBlockBehavior) {
        Objects.requireNonNull(useBlockBehavior);
        this.useBlockBehavior = useBlockBehavior;
        return this;
    }

    public ItemBuilder setUseBehavior(ItemType.UseBehavior useBehavior) {
        Objects.requireNonNull(useBehavior);
        this.useBehavior = useBehavior;
        return this;
    }


    public ItemBuilder setHitBlockBehavior(ItemType.HitBlockBehavior hitBlockBehavior) {
        Objects.requireNonNull(hitBlockBehavior);
        this.hitBlockBehavior = hitBlockBehavior;
        return this;
    }

    public Item build() {
        return new ItemBase(useBlockBehavior, hitBlockBehavior, useBehavior).localName(id);
    }
}