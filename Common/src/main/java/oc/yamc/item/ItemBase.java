package oc.yamc.item;

import oc.yamc.entity.Entity;
import oc.yamc.player.Player;
import oc.yamc.registry.Impl;
import oc.yamc.util.RayCast;
import oc.yamc.world.World;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

class ItemBase extends Impl<Item> implements Item {
    protected ItemType.UseBlockBehavior useBlockBehavior;
    protected ItemType.HitBlockBehavior hitBlockBehavior;
    protected ItemType.UseBehavior useBehavior;

    ItemBase(ItemType.UseBlockBehavior useBlockBehavior, ItemType.HitBlockBehavior hitBlockBehavior, ItemType.UseBehavior useBehavior) {
        this.useBlockBehavior = useBlockBehavior;
        this.hitBlockBehavior = hitBlockBehavior;
        this.useBehavior = useBehavior;
    }

    @Override
    public void onUseStart(World world, Entity entity, Item item) {
        useBehavior.onUseStart(world, entity, item);
    }

    @Override
    public boolean onUsing(World world, Player player, Item item, int tickElapsed) {
        return useBehavior.onUsing(world, player, item, tickElapsed);
    }

    @Override
    public void onUseStop(World world, Player player, Item item, int tickElapsed) {
        useBehavior.onUseStop(world, player, item, tickElapsed);
    }

    @Override
    public void onHit(World world, Player player, Item item, RayCast.Hit hit) {
        hitBlockBehavior.onHit(world, player, item, hit);
    }


    @Override
    public void onUseBlockStart(World world, Entity entity, Item item, RayCast.Hit hit) {
        useBlockBehavior.onUseBlockStart(world, entity, item, hit);
    }

    @Override
    public boolean onUsingBlock(Player player, Item item, RayCast.Hit hit, int tickElapsed) {
        return useBlockBehavior.onUsingBlock(player, item, hit, tickElapsed);
    }

    @Override
    public void onUseBlockStop(Player player, Item item, RayCast.Hit hit, int tickElapsed) {
        useBlockBehavior.onUseBlockStop(player, item, hit, tickElapsed);
    }

    @Nullable
    @Override
    public <T> T getComponent(@Nonnull String name) {
        return null;
    }

    @Nullable
    @Override
    public <T> T getComponent(@Nonnull Class<T> type) {
        return null;
    }

    @Nullable
    @Override
    public <T> T getBehavior(Class<T> type) {
        return null;
    }


}
