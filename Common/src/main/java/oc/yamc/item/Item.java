package oc.yamc.item;

import oc.yamc.RuntimeObject;
import oc.yamc.registry.RegistryEntry;
import oc.yamc.util.Owner;
import oc.yamc.world.World;

@Owner(World.class)
public interface Item extends RuntimeObject, RegistryEntry<Item>,
        ItemType.UseBlockBehavior, ItemType.HitBlockBehavior, ItemType.UseBehavior {

    // block://0/1/0?direction=1&bcd=2

    interface Stack extends Item {
        boolean isEmpty();

        int getQuantity();

        void setQuantity(int quantity);

        int getMaxQuantity();

        Stack split(int quantity);
    }

    interface Damageable extends Item {
        float getDamage();

        void setDamage(float damage);

        // public abstract void addEffect(Effect effect);
        // public abstract List<Effect> getEffects();
    }
}
