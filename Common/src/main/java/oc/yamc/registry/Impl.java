package oc.yamc.registry;

import com.google.common.reflect.TypeToken;
import oc.yamc.Engine;
import oc.yamc.mod.ModContainer;
import oc.yamc.util.Call;

public abstract class Impl<T extends RegistryEntry<T>> implements RegistryEntry<T> {
    private static final StackWalker WALKER = StackWalker.getInstance(StackWalker.Option.RETAIN_CLASS_REFERENCE);
    private final TypeToken<T> token = new TypeToken<>(getClass()) { };
    private String registeredName;
    private ModContainer modContainer = Engine.game().getModManager().findModByClass(WALKER.walk((s) -> s
            .filter(v -> !v.getClassName().startsWith("oc.yamc"))
//            .peek(System.out::println)
            .findFirst()).map(v -> Call.callOrElse(() -> Class.forName(v.getClassName()), null).get())
            .orElse((Class) Impl.class));
    private Registry<? extends RegistryEntry<T>> registry;

    @SuppressWarnings("unchecked")
    @Override
    public final Class<T> getRegistryType() {
        return (Class<T>) token.getRawType();
    }

    @Override
    public String toString() {
        return token + "{" +
                "path='" + registeredName + '\'' +
                '}';
    }

    @Override
    public final String getLocalName() {
        return registeredName;
    }

    @SuppressWarnings("unchecked")
    @Override
    public final T localName(String name) {
        if (this.registeredName != null) throw new Error("Duplicated register " + name);
        this.registeredName = name;
        return (T) this;
    }

    @SuppressWarnings("unchecked")
    @Override
    public T metadata(String name, String data) {
        if (this.modContainer != null) throw new IllegalStateException("The registry entry is already frozen.");
        return (T) this;
    }

    void setup(Registry<? extends RegistryEntry<T>> registry) {
        this.registry = registry;
    }

    @Override
    public ModContainer getOwner() {
        return modContainer;
    }

    @Override
    public Registry<? extends RegistryEntry<T>> getAssignedRegistry() {
        if (registry == null)
            throw new IllegalStateException("This method can only be called after the register stage of the game!");
        return registry;
    }
}
