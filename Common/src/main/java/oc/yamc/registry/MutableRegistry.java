package oc.yamc.registry;

import com.google.common.collect.HashBiMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import io.netty.util.collection.IntObjectHashMap;
import io.netty.util.collection.IntObjectMap;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public class MutableRegistry<T extends RegistryEntry<T>> implements Registry<T> {
    private final Class<T> registryType;
    private final String name;

    private final Map<String, T> nameToObject = Maps.newHashMap();
    private IntObjectMap<T> idToObject = new IntObjectHashMap<>();
    private Map<String, Integer> nameToId = HashBiMap.create();
    private Map<String, Collection<T>> modidToRegistry = Maps.newHashMap();
    private Map<Class<?>, Extension<T, ?>> typeToExtension = Maps.newHashMap();

    private AtomicInteger id = new AtomicInteger();

    MutableRegistry(Class<T> registryType) {
        this.registryType = registryType;
        this.name = registryType.getSimpleName().toLowerCase();
    }

    public MutableRegistry(Class<T> registryType, String name) {
        this.registryType = registryType;
        this.name = name;
    }

    @Override
    public T register(T obj) {
        Objects.requireNonNull(obj);
        ((Impl) obj).setup(this);
        String regId = obj.getUniqueName();
        int next = id.getAndIncrement();
        nameToObject.put(regId, obj);
        idToObject.put(next, obj);
        nameToId.put(regId, next);
        modidToRegistry.computeIfAbsent(obj.getOwner().getModId(), (k) -> Lists.newArrayList()).add(obj);
        return obj;
    }

    @Override
    public Class<T> getRegistryEntryType() {
        return registryType;
    }

    @Override
    public String getRegistryName() {
        return name;
    }

    @Override
    public T getValue(String registryName) {
        Objects.requireNonNull(registryName);
        return nameToObject.get(registryName);
    }

    @Override
    public String getKey(T value) {
        Objects.requireNonNull(value);
        return value.getUniqueName();
    }

    @Override
    public boolean containsKey(String uniqueName) {
        Objects.requireNonNull(uniqueName);
        return nameToObject.containsKey(uniqueName);
    }

    @Override
    public Set<String> getKeys() {
        return nameToObject.keySet();
    }

    @Override
    public Collection<T> getValues() {
        return nameToObject.values();
    }

    @Override
    public int getId(T obj) {
        Objects.requireNonNull(obj);
        String regId = obj.getUniqueName();
        return nameToId.get(regId);
    }

    @Override
    public int getId(String key) {
        Objects.requireNonNull(key);
        return nameToId.get(key);
    }

    @Override
    public String getKey(int id) {
        return idToObject.get(id).getLocalName();
    }

    @Override
    public T getValue(int id) {
        return idToObject.get(id);
    }

    @Override
    public Collection<Map.Entry<String, T>> getEntries() {
        return nameToObject.entrySet();
    }

    @Override
    @SuppressWarnings("unchecked")
    public <E> Extension<T, E> getExtension(Class<E> type) {
        return (Extension<T, E>) typeToExtension.computeIfAbsent(type, (k) -> new ExtensionImpl<>());
    }

    @Override
    public Collection<Map.Entry<Class<?>, Extension<T, ?>>> getExtensions() {
        return typeToExtension.entrySet();
    }

    @Override
    public Collection<T> getEntries(String modid) {
        return modidToRegistry.get(modid);
    }

    private class ExtensionImpl<E> implements Extension<T, E> {
        private IntObjectMap<E> idToElement = new IntObjectHashMap<>();

        @Override
        public void register(T element, E extensionElement) {
            if (!containsValue(element)) throw new IllegalArgumentException();
            idToElement.put(getId(element), extensionElement);
        }

        @Override
        public E getValue(T element) {
            if (!containsValue(element)) return null;
            return idToElement.get(getId(element));
        }

        @Override
        public E getValue(int id) {
            return idToElement.get(id);
        }
    }
}
