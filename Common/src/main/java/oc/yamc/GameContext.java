package oc.yamc;

import lombok.NonNull;
import oc.yamc.event.Event;
import oc.yamc.event.EventBus;
import oc.yamc.event.EventHandlerModule;
import oc.yamc.event.EventSource;
import oc.yamc.game.Game;
import oc.yamc.item.Item;
import oc.yamc.registry.Registry;
import oc.yamc.registry.RegistryManager;
import oc.yamc.util.Owner;
import oc.yamc.world.Block;

import java.util.Queue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ForkJoinPool;
import java.util.function.Supplier;

@Owner(Game.class)
public class GameContext implements EventBus {
    private EventBus bus;
    private RegistryManager manager;
    private ExecutorService executorService;
    private Queue<Runnable> nextTicks;

    public GameContext(RegistryManager manager, EventBus bus, Queue<Runnable> nextTicks) {
        this.manager = manager;
        this.bus = bus;
        this.nextTicks = nextTicks;
        this.executorService = ForkJoinPool.commonPool();
        // this.nextTick = nextTick;
    }

    public RegistryManager getRegistry() {
        return manager;
    }

    public Registry<Block> getBlockRegistry() {
        return manager.getRegistry(Block.class);
    }

    public Registry<Item> getItemRegistry() {
        return manager.getRegistry(Item.class);
    }

    @Override
    public boolean post(@NonNull Event event) {
        return bus.post(event);
    }

    @Override
    public EventHandlerModule register(@NonNull Object eventHandlerModule) {
        return bus.register(eventHandlerModule);
    }

    @Override
    public void unregister(@NonNull Object eventHandlerModule) {
        bus.unregister(eventHandlerModule);
    }

    @Override
    public Executor getEventExecutor(@NonNull Class<? extends Event> eventType) {
        return bus.getEventExecutor(eventType);
    }

    public Event trigger(EventSource eventSource) { return eventSource.construct(this); }

    public void nextTick(Runnable runnable) {
        nextTicks.offer(runnable);
    }

    public ExecutorService getExecutorService() {
        return executorService;
    }

    public <T> CompletableFuture<T> async(Supplier<T> action) {
        return CompletableFuture.supplyAsync(action, executorService);
    }

    public CompletableFuture<Void> async(Runnable action) {
        return CompletableFuture.runAsync(action, executorService);
    }
}
