package oc.yamc.resource;

import oc.yamc.resource.source.ResourceSourceEmpty;

import java.io.IOException;
import java.io.InputStream;

public interface ResourceSource {
    static ResourceSource empty() {
        return ResourceSourceEmpty.empty();
    }

    boolean has(String path);

    InputStream open(String path) throws IOException;

    ResourceSourceInfo info() throws IOException;

    String type();
}
