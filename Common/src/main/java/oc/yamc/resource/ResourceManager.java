package oc.yamc.resource;

import java.io.IOException;

public interface ResourceManager {
    void clearCache();

    void clearAll();

    void invalidate(String path);

    void addResourceSource(ResourceSource source);

    Resource load(String path) throws IOException;
}
