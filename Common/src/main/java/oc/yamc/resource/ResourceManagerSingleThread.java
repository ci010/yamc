package oc.yamc.resource;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The one manager for the game. Not really considering concurrency now.
 * <p>
 * Not considering the putData dependency here (like reload resource toggling re-bake model and others...)
 * <p>
 * I suggest that we should handle putData dependency in other places (not here)
 */
public class ResourceManagerSingleThread implements ResourceManager {
    private Map<String, Resource> cache = new HashMap<>();
    private List<ResourceSource> sources = new ArrayList<>();

    private Resource putCache(Resource res) {
        this.cache.put(res.path(), res);
        return res;
    }

    @Override
    public void clearCache() {
        this.cache.clear();
    }

    @Override
    public void clearAll() {
        this.cache.clear();
        this.sources.clear();
    }

    @Override
    public void invalidate(String path) {
        cache.remove(path);
    }

    /**
     * Add resource source to the manager
     *
     * @param source
     */
    @Override
    public void addResourceSource(ResourceSource source) {
        this.sources.add(source);
    }

    @Override
    public Resource load(String path) throws IOException {
        Resource cached = this.cache.get(path);
        if (cached != null) return cached;

        for (ResourceSource src : sources) {
            if (!src.has(path)) continue;
            Resource loaded = new ResourceBase(path) {
                @Override
                public InputStream open() throws IOException {
                    return src.open(path);
                }
            };
            return this.putCache(loaded);
        }

        return null;
    }
}
