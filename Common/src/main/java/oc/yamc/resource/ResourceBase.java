package oc.yamc.resource;



import java.io.IOException;
import java.io.InputStream;

public abstract class ResourceBase implements Resource {
    private String location;
    private byte[] cache;

    public ResourceBase(String location) {
        this.location = location;
    }

    public String path() {
        return location;
    }

    @Override
    public byte[] cache() throws IOException {
        if (cache != null) return cache;
        InputStream open = this.open();
        if (open != null) {
            cache = open.readAllBytes();
            open.close();
            return cache;
        }
        return null;
    }

    @Override
    public void invalidate() {
        this.cache = null;
    }
}
