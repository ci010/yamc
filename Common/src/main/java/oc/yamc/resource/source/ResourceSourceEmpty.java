package oc.yamc.resource.source;

import oc.yamc.resource.ResourceSource;
import oc.yamc.resource.ResourceSourceInfo;

import java.io.IOException;
import java.io.InputStream;

public class ResourceSourceEmpty implements ResourceSource {
    public static ResourceSourceEmpty empty() {
        return Lazy.inst;
    }

    private ResourceSourceEmpty() {
    }

    private static class Lazy {
        private static ResourceSourceEmpty inst = new ResourceSourceEmpty();
    }

    @Override
    public boolean has(String path) {
        return false;
    }

    @Override
    public InputStream open(String path) throws IOException {
        return null;
    }

    @Override
    public ResourceSourceInfo info() throws IOException {
        return null;
    }

    @Override
    public String type() {
        return "none";
    }
}
