package oc.yamc.resource;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Support multi-thread access for the resource cache, hopefully this work...
 */
public class ResourceManagerConcurrent implements ResourceManager {
    private Map<String, Resource> cache = new ConcurrentHashMap<>();
    private List<ResourceSource> sources = new ArrayList<>();

    @Override
    public void clearCache() {
        this.cache.clear();
    }

    @Override
    public void clearAll() {
        this.cache.clear();
        this.sources.clear();
    }

    @Override
    public void invalidate(String path) {
        cache.remove(path);
    }

    /**
     * Add resource source to the manager
     *
     * @param source
     */
    @Override
    public void addResourceSource(ResourceSource source) {
        this.sources.add(source);
    }

    @Override
    public Resource load(String path) throws IOException {
        cache.computeIfPresent(path, (k, v) -> {
            if (v instanceof ResourceResolving && ((ResourceResolving) v).resolved != null)
                return new ResourceResolved(v.path(), ((ResourceResolving) v).resolved);
            return v;
        });
        return cache.computeIfAbsent(path, (k) -> new ResourceResolving(path, this.sources));
    }

    private static class ResourceResolved extends ResourceBase {
        private ResourceSource source;

        ResourceResolved(String location, ResourceSource source) {
            super(location);
            this.source = source;
        }

        @Override
        public InputStream open() throws IOException {
            return source.open(path());
        }
    }

    private static class ResourceResolving extends ResourceBase {
        private List<ResourceSource> sources;
        private ResourceSource resolved;
        private Lock lock = new ReentrantLock();

        ResourceResolving(String location, List<ResourceSource> sources) {
            super(location);
            this.sources = sources;
        }

        @Override
        public InputStream open() throws IOException {
            try {
                lock.lock();
                String path = path();
                if (resolved != null) {
                    return resolved.open(path);
                }
                for (ResourceSource src : sources) {
                    if (!src.has(path)) continue;
                    this.resolved = src;
                    return resolved.open(path);
                }
                throw new IOException("No such resource @" + path);
            } finally {
                lock.unlock();
            }
        }
    }
}
