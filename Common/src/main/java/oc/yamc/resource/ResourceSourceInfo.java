package oc.yamc.resource;

public interface ResourceSourceInfo {

    String name();

    String description();

    String format();

    String icon();
}
