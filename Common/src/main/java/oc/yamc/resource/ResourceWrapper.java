package oc.yamc.resource;

import java.io.IOException;
import java.io.InputStream;

public class ResourceWrapper implements Resource {
    private Resource resource;

    public ResourceWrapper(Resource resource) { this.resource = resource; }

    @Override
    public String path() {return resource.path();}

    @Override
    public InputStream open() throws IOException {return resource.open();}

    @Override
    public byte[] cache() throws IOException {return resource.cache();}

    @Override
    public void invalidate() {resource.invalidate();}
}
