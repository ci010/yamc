package oc.yamc.event;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class ASMEventBusTest {

    private static AsmEventBus eventBus;

    @BeforeAll
    public static void setup() {
        eventBus = new AsmEventBus();
        eventBus.register(new TestOrderListener());
        eventBus.register(new TestCancellableListener());
        eventBus.register(new TestParentChildListener());

        eventBus.register(new EventHandlerModule() {
            private EventHandler handler = EventHandler.wrap(ExampleEvent.class, (e) -> System.out.println("MM " + e.value));

            @Override
            public EventHandler[] get() {
                return new EventHandler[]{ handler };
            }

            @Override
            public Type getType() {
                return null;
            }
        });
    }

    @Test
    void testOrder() {
        Assertions.assertFalse(eventBus.post(new ExampleEvent("Hello! AsmEventBus!")));
    }

    @Test
    void testCancellable() {
        Assertions.assertTrue(eventBus.post(new ExampleCancellableEvent()));
    }

    @Test
    void testParent() {
        Assertions.assertFalse(eventBus.post(new ExampleChildEvent("Hello! AsmEventBus!")));
    }
}
