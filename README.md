# Yet Another Minecraft
A 3D sandbox game by Java.

- [Yet Another Minecraft](#yet-another-minecraft)
    - [Setup](#setup)
    - [Current Design Situation](#current-design-situation)
        - [What should we identify in game?](#what-should-we-identify-in-game)
        - [About the Registry](#about-the-registry)
            - [About the Register Name](#about-the-register-name)
            - [About Sub Registry Namespace](#about-sub-registry-namespace)
        - [About Block API](#about-block-api)
            - [Current Block Logic Design](#current-block-logic-design)
            - [Accessing Block & Navigate through World](#accessing-block--navigate-through-world)
        - [Item Design](#item-design)
        - [Data Tracking/Syncing (Need to figure out)](#data-trackingsyncing-need-to-figure-out)
        - [Game Startup Progress (Draft)](#game-startup-progress-draft)
        - [Key algorithm/problem](#key-algorithmproblem)
            - [Terrain Generation](#terrain-generation)
            - [Raycasting](#raycasting)
            - [Entity Body Collision Detection (Problematic)](#entity-body-collision-detection-problematic)
            - [Texture Atlas (Build Texture Map)](#texture-atlas-build-texture-map)
            - [Update Baked Chunk](#update-baked-chunk)
            - [Game Loop](#game-loop)
        - [Render Progress Introduction](#render-progress-introduction)
            - [Sky Rendering](#sky-rendering)
            - [Fog rendering](#fog-rendering)
            - [Some Helper Classes to manage the GL stuff](#some-helper-classes-to-manage-the-gl-stuff)
            - [Squeezing Multiple the Chunks into a Single VBO (Just a Thought)](#squeezing-multiple-the-chunks-into-a-single-vbo-just-a-thought)
    - [Some Humble Design Thought](#some-humble-design-thought)

## Setup

You need java 10 to run this project.

You also need to goto https://mcasset.cloud/ to download assets of Minecraft 1.13

To run, you can run oc.yamc.client.Main class to start client.

## Current Design Situation

### What should we identify in game?

Discuss the things we need to identify in game.

| Name             | What does it referring?                                                                          | How to identify?                                                                                                           | Why?                                                                                                                         |
| ---------------- | ------------------------------------------------------------------------------------------------ | -------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------- |
| Action           | The abstract eventSource, like command, but this should include the eventSource like "player move forward" | By string id (player.move.forward)                                                                                         | We need to bind key or command or other input way to these to control game                                                   |
| Game Object Type | The type of ingame object, like block prototype, item prototype, entity type.                    | By semantic string id (block.stone)                                                                                        | Majorly used for (de)serialization on network or disk                                                                        |
| Game Object      | The important things ingame, mostly the object under the world life cycle (entity, block, item)  | By hierarchy structure string. (block requires location, entity requires id) e.g. /\<dimension id>/\<world id>/\<position> | We need to have a universal way to refer an ingame object which provides a easy way to communicate between client and server |
| Resource         | The game resource in disk (or remote)                                                            | By its string dir                                                                                                          | We need to loadOrder resources obviously                                                                                     |

We have to manage these things differently though.

The `Actions` and `GameObjectType` should be managed by individual registries.

The `Resource` should managed by `ResourceManager`. It should provide the basic level of caching (cache bytes).
The resource will transform to other form (Texture, Model, Sound, Animation) and linked with its location.
After the loading stage of a game, all the resource transformed out. The raw bytes cache should be cleared.

The `GameObject` should managed by its parent. We don't need to do anything special to it.
Just process the dir to get dimension, get world, and get block or entity.

### About the Registry

The ideal way to register a thing is only providing its id without any redundant information. The Mod should not consider too much about conflicting with other Mod. 

```java
Registry<BlockObject> registry;
registry.register(block.setRegistryName('air')); // infer the query full dir is <current modid>.block.air
Registry<ItemObject> itemRegistry;
itemRegistry.register(item.setRegisterName('stone')); // infer the query full dir is <current modid>.item.stone

RegistryManager manager;
manager.register(block.setRegistryName('stone'));  // infer the query full dir is <current modid>.block.stone

// when we want to get a registered object
manager.get('yamc.block.stone'); // get the stone block
```

#### About the Register Name

I suggest we all use the snake case (split word with low_dash) for register name. 

The name should not contain any dot/period (.)

#### About Sub Registry Namespace

There might be sub-named blocks and items. For block, it might be produced by combining properties.

### About Block API

The block is the one of the major problems in voxel game...

The old Minecraft use `BlockState` design, which builds flyweight objects to represent blocks.
This design faces various challenges. I won't describe them in detail here.

#### Current Block Logic Design

The flyweight design is the right direction. We still want manage the block by some flyweight object.
In the game, we directly use a immutable flyweight object `Block` to describe the block backed data, 
which is similar to the Minecraft. 

Though, this flyweight object is composited by several interface. 
The user will use [Strategy Pattern](https://en.wikipedia.org/wiki/Strategy_pattern) to build up this flyweight instance.

The `Block` class will contains all the logical functionality, like `onBlockActivated`, or `onBlockHit`. Also, it will contains
the Material information. These are all same with Minecraft.

A `Block` instance also represent a `BlockState` in Minecraft. It will contains the properties of Block. 
(Minecraft currently delegate the `BlockState` methods to the `Block`).

All the `Block` things are all runtime immutable by default.

In pseudo code, it's something like:

```java
// user will not actually build a block.
interface Block extends OnHit, OtherBehaviorInterface /* and other interfaces... */ {
    Material getMaterial();
    Property getProperty(PropertyKey key);

    <T> T getBehavior(Class<T> clz); // get some third party behaviors
    // in the future, the third party behavior integrate might be impl by sponge mixin
}

// user will only implement these interface 
interface OnHit {
    boolean onHit(Block block, Position position, Entity hitter, Hit hitInfo);
}

interface OtherBehaviorInterface {
    // other function here
}
```


#### Accessing Block & Navigate through World

We will also provide an access API for `Block` in `World` at the runtime. 
I'm thinking about the idea of `BlockCursor` which contains the `Block` and `Position`, 
it's also the API implements the accessing of custom data of block, like `TileEntity`. (Notice that we don't actually have some class corresponding to TileEntity)

In pseudo code, it's something like:

```java
interface BlockCursor extends Block, RuntimeObject {
    Position getPosition();
    Object getComponent(String s);

    BlockCursor setProperty(Property p, Value v);
    BlockCursor move(int x, int y, int z);
    BlockCursor moveTo(int x, int y, int z);

    // set block at current position
    BlockCursor setBlock(Block b);
}

class SomeClass {
    void someFunction(World w) {
        BlockCursor cur = w.getCursor(100, 50, 100);
        for (int i = 0; i < 10; ++i) {
            var value = cur.getComponent('some key'); // this is like capability
            cur.setBlock(Blocks.AIR); // set block to air
            // perform some operation on block
            cur.move(1, 0, 0);
        }
    }
}

```

### Item Design 

I'm not really sure about the item yet. It's just like block, the user will never actually create a item/item type class, instead, the user will build up by filling blank (strategy pattern).

The name design is also `Item` is the Minecraft `ItemStack` and the `ItemType` is the Minecraft `Item`.

One thing can be confirmed. The `Item` will be either in stack or non stack. This concept will be split by class (java has no multiple inheritance).

### Data Tracking/Syncing (Need to figure out)

This is a big deal. We have to make an easy way to check dirty and sync data.

### Game Startup Progress (Draft)

1. initialize glfw opengl, window and other hook
2. init engine
    1. initialize player profile, login information from disk (later GUI show to let player login if there is no local cached profile) 
    2. grab game config from disk
    3. create game
        1. pull the resources/mod config from server
        2. construct stage: load mods (asm, classloading)
        3. register stage: register all block/item/entity/eventSource/renderer factory
        4. resource stage: call mods to load resources
        5. finish stage: initialize world
        6. game loop start

### Key algorithm/problem

- physics and collision
    - use joml AABB to deal with bonding
    - maybe a general manager under `World` to manage physics?
- logic/render object state management and update cycle
    - naive idea is that we keep two different form of data in game and renderer thread.
    - the data in logic thread are changed by event (the event could toggle by network/user input)
    - after the logic thread receive event, it should emit the change which only exposed to renderer thread  
    - the data in renderer thread only receives the change provide by logic thread; ideally it won't query logic thread data by it self
- face culling
    - https://0fps.net/2012/06/30/meshing-in-a-minecraft-game/
    - http://www.lighthouse3d.com/tutorials/view-frustum-culling/

#### Terrain Generation

- [Advance Value Noise](http://www.iquilezles.org/www/articles/morenoise/morenoise.htm)
- [Terrain raymarching](http://www.iquilezles.org/www/articles/terrainmarching/terrainmarching.htm)
- [Terrain from noise](https://www.redblobgames.com/maps/terrain-from-noise/)

#### Raycasting

Narrowphase use use `joml` package's `Ray` to deal with picking.

Boardphase we use [A Fast Voxel Traversal Algorithm](http://www.cse.yorku.ca/~amana/research/grid.pdf) with the [implementation](https://stackoverflow.com/questions/12367071/how-do-i-initialize-the-t-variables-in-a-fast-voxel-traversal-algorithm-for-ray).


#### Entity Body Collision Detection (Problematic)

The basic idea is get three "faces" of blocks according to current entity motion vector and entity AABB,
and test the theses blocks AABB with entity AABB.

e.g.

To get these blocks' positions. The entity's motion vector is `(x,y,z)`.

For the `x` axis, we first figure out that the direction of `x`, positive or negative.
Then, pick the correct face from 6 faces of current entity AABB. If `x` is positve, pick the AABB face with `maxX`. Else we pick the `minX` face.

*Please notice that the AABB is represented in two vector: (minX, minY, minZ), (maxX, maxY, maxZ)*

Suppose we pick the face by `x` axis called `X`. The face `X` is defined by 4 vertices. We translate this face by the motion vector `x` axis value. 
Then we get all the block between the translated face area. 



#### Texture Atlas (Build Texture Map)

[Space-optimized texture atlas](http://www.cs.upc.edu/~jmartinez/slides/masterThesisSlides.pdf)

#### Update Baked Chunk 

A block model **A** has N vertices, then it vertex length is N * 3

[x0 y0 z0 x1 y1 z1 ... xn yn zn]

Bake the chunk as combination of block model:

blockAt (0,0,0) (0,0,1) ....

[x0 y0 z0 x1 y1 z1 ... xn yn zn] [x0 y0 z0 x1 y1 z1 ... xn yn zn] ...

Suppose `len(m)` is the vertices count of model `m`. 

We maintains a summed list `L` where `L[x] = L[x - 1] + len(model at index x)`

When a block is changed in chunk, we update the render chunk data by swap the block vertex:

Suppose the block at position `(x, y, z)` changed,

```java
float[] newVertices; // passed by param
int index = (x & 0xF) < 8 | (y & 0xF) < 4 | (z & 0xF);
int leftSum = L[index - 1];
int rightSum = L[index];
int totalLength = L[(0xF) < 8 | (0xF) < 4 | (0xF)];

int newRightSum = leftSum + newVertices.length;
int oldVerticesCount = rightSum - leftSum;

// these are not real gl commands
// shift the right vertices
glCopy(rightSum, newRightSum, totalLength - rightSum);
// upload the new block vertices to the correct position
glUpload(newVertices, leftSum);
```

#### Game Loop

Some reference articles:

- [LWJGL book Fix Step](
https://github.com/lwjglgamedev/lwjglbook/blob/master/chapter02/src/main/java/org/lwjglb/engine/GameEngine.java)
- [What is the point of update independent rendering in a game loop?](
https://gamedev.stackexchange.com/questions/132831/what-is-the-point-of-update-independent-rendering-in-a-game-loop)
- [Dynamic Tick vs Fix Step Tick](https://gamedev.stackexchange.com/questions/56956/how-do-i-make-a-game-tick-method)
- [Introduce Game Loop](http://gameprogrammingpatterns.com/game-loop.html)

Splitting Between Logic and Render

- Logic
    - update independently
    - update world and various world subsystem
        - update world physics between entity
- Renderer
    - update progress depending on the partial progress of logic 
    - maintains block/entity model part transformation
    - maintains batch particles system
    - render update

### Render Progress Introduction

These are some background:

- [How GPU work](http://fragmentbuffer.com/gpu-performance-for-game-artists/)
- [MVP Camera Matrix Transformation](http://www.opengl-tutorial.org/beginners-tutorials/tutorial-3-matrices/)
- [Shadow Mapping](https://learnopengl.com/Advanced-Lighting/Shadows/Shadow-Mapping)
- [Shadow Depth Mapping](https://docs.microsoft.com/en-us/windows/desktop/dxtecharts/common-techniques-to-improve-shadow-depth-maps)
- [Transparent and Cutoff](https://en.wikibooks.org/wiki/GLSL_Programming/GLUT/Transparent_Textures)
- [Blending](https://learnopengl.com/Advanced-OpenGL/Blending)
- [Vertex Attribute Binding](https://stackoverflow.com/questions/34474811/gl-arb-vertex-attrib-binding-and-indexed-drawing)
- [Learning Modern 3D Graphics Programming](https://alfonse.bitbucket.io/oldtut/)
- [Advanced-GLSL](https://learnopengl.com/Advanced-OpenGL/Advanced-GLSL)
- [Shadow Mapping in forward shading](https://github.com/haedri/shadow-mapping/wiki/Part-4-:-multiple-lights,-forward-rendering)

Current render process is naive. Just a collection of `Renderer` objects, which will be called `void render(double partialTick)` each render tick in order.

- [How to update buffer async](https://www.seas.upenn.edu/~pcozzi/OpenGLInsights/OpenGLInsights-AsynchronousBufferTransfers.pdf)
- [Instance Rendering](https://learnopengl.com/Advanced-OpenGL/Instancing)
- [Overall Tutorial](https://lwjglgamedev.gitbooks.io/3d-game-development-with-lwjgl/)
- [BVH](http://fileadmin.cs.lth.se/cs/Education/EDAN30/lectures/S2-bvh.pdf)

#### Sky Rendering

- [THE REAL-TIME VOLUMETRIC CLOUDSCAPES OF HORIZON ZERO DAWN](https://www.guerrilla-games.com/read/the-real-time-volumetric-cloudscapes-of-horizon-zero-dawn) and - [its WIP Implementation](https://github.com/adrianderstroff/realtime-clouds)
- [GPU GEM2](https://developer.nvidia.com/gpugems/GPUGems2/gpugems2_chapter16.html)
- [A really short implementation of procedural sky + clouds renderer](https://github.com/shff/opengl_sky)
- [A atmsphere renderer renders sky colors with Rayleigh and Mie scattering.](https://github.com/wwwtyro/glsl-atmosphere)
- [Stars](http://csc.lsu.edu/~kooima/misc/cs594/final/part2.html)
- [Fourier Opacity Mapping](https://pdfs.semanticscholar.org/2848/88991dc5aee6e7e9d8f0909fe3dae2f1e798.pdf)


#### Fog rendering

- http://www.gamasutra.com/blogs/BartlomiejWronski/20141208/226295/Atmospheric_scattering_and_volumetric_fog_algorithm__part_1.php
- https://bartwronski.files.wordpress.com/2014/08/bwronski_volumetric_fog_siggraph2014.pdf

#### Some Helper Classes to manage the GL stuff

OpenGL managed things by its id. The GL allocated objects, like VAO, VBO, or textures, are all identified by ids. Therefore, we should have some unified wrapper for this. 

Currently, we have wrapper for texture, the `GLTexture` class, which has `bind` function to bind texture, and `destroy` to deallocate this GL resource.

Also, another class called `GLMesh` is using to represent an model data. But, it's not a really good design. I might change it later...
 
#### Squeezing Multiple the Chunks into a Single VBO (Just a Thought)

```
VAO:
    VBO: | chunk 1 vertices data | chunk 2 vertices data | ...
    index: single chunk draw index order
```

We draw it by `glDrawelEmentsBaseVertex(mode, count, type, 0, chunkOffset)`.

We can estimate the how large will this vbo be:

Assume a chunk is fill by 6 faces full blocks.

Each vertex takes 8 floats (3 pos, 2 uv, 3 normal). 
Each face takes 4 vertices.
Each block has 6 faces.
Each chunk has 4096 blocks.

The total floats is `8 * 4 * 6 * 4096 = 786432`, and it takes `786432 * 4 = 3145728` bytes.


## Some Humble Design Thought

When we want to create some object. There always some precondition, re-configuration the object may have. Normally, we pass them as the parameter to the object. But, when the object is complex. This way won't really work.

These are three pattern to create a object:

Whent the object is really simple, like data class. We can use constuctor.

```java
class A {
    public A(String paramA, int paramB) {
        this.a = paramA;
        this.b = paramB;
    }
}
```

If there are some transformation or other calculation which **only** related to this object creation, such as load resources from disk. We can have a static create method to manage that.

```java
class A {
    public static A create(String paramA, int paramB) {
        // perform some transformation to paramA
        // perform some transformation to paramB
        // perform complex transformation
        return new A(transformedA, transformedB, transformed C);
    }
    private A(...) {
    }
}
```

If the object lifecycle is really complex, we usually pass a config to constuctor. Then, a initialization function there to trigger the complex initialization. (Maybe multiple init function). 

This is the case that the object will pass through multiple stages changes. 


```java
class A {
    public A(Config config) {
        this.config = config;
    }

    public start(Context otherContext) throws Exception {
        preInit();
        init();
        postInit();
    }
}
```


